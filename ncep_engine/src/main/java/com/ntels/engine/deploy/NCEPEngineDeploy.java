package com.ntels.engine.deploy;

import com.ntels.engine.repository.CacheServiceVerticle;
import com.ntels.engine.verticle.LogPushVerticle;
import com.ntels.engine.verticle.RuleResultVerticle;
import com.ntels.engine.verticle.WebsocketOutVerticle;
import com.ntels.engine.verticle.input.KafkaInVerticle;
import com.ntels.engine.verticle.input.RESTInVerticle;
import com.ntels.engine.verticle.output.DBOutVerticle;
import com.ntels.engine.verticle.output.EmailOutVerticle;
import com.ntels.engine.verticle.output.RESTOutVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.impl.VertxInternal;
import io.vertx.core.json.JsonObject;

/**
 * NCEPEngineDeploy 클래스
 *
 * N-CEP Engine 서버 시작 Verticle
 *
 */
public class NCEPEngineDeploy extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        String node_id = ((VertxInternal) vertx).getNodeID();

        DeploymentOptions opt = new DeploymentOptions();
        vertx.fileSystem().readFile("ncep-config.json", result -> {
            if (result.failed()) {
                result.cause().printStackTrace();
                return;
            }

            JsonObject config = result.result().toJsonObject();
            config.put("cacheNodeId", node_id);
            opt.setConfig(config);

            vertx.deployVerticle(CacheServiceVerticle.class.getName(), opt, r -> {
                if (r.succeeded()) {
                    vertx.deployVerticle(RESTInVerticle.class.getName(), opt);
                    vertx.deployVerticle(DBOutVerticle.class.getName(), opt);
                    vertx.deployVerticle(EmailOutVerticle.class.getName(), opt);
                    vertx.deployVerticle(RESTOutVerticle.class.getName(), opt);
                    vertx.deployVerticle(LogPushVerticle.class.getName(), opt);
                    vertx.deployVerticle(RuleResultVerticle.class.getName(), opt);
                    vertx.deployVerticle(WebsocketOutVerticle.class.getName(), opt);
                    vertx.deployVerticle(KafkaInVerticle.class.getName(), opt);
                }
            });

        });
    }
}
