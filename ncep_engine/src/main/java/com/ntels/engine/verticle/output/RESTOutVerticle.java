package com.ntels.engine.verticle.output;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

/**
 * Rule 결과를 HTTP REST로 전송 처리하는 Class
 */
public class RESTOutVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer("send.rest", sendREST());
    }

    private Handler<Message<JsonObject>> sendREST() {
        return message -> {
            try {
                logger.debug("sendREST start : " + message.body().encode());
                JsonObject param = message.body();

                JsonObject sendInfo = param.getJsonObject("send_info");
                JsonObject sendContent = new JsonObject(param.getString("send_content"));
                String url = sendInfo.getString("url");
                int port = Integer.parseInt(sendInfo.getString("port"));
                String path = sendInfo.getString("path");
                String method = sendInfo.getString("method");

                HttpMethod hm;
                switch (method) {
                    case "get":
                        hm = HttpMethod.GET;
                        break;
                    case "put":
                        hm = HttpMethod.PUT;
                        break;
                    case "delete":
                        hm = HttpMethod.DELETE;
                        break;
                    default:
                        hm = HttpMethod.POST;
                        break;
                }

                HttpClient client = vertx.createHttpClient();
                HttpClientRequest clientRequest = client.request(hm, port, url, path, r_result -> {
                    r_result.endHandler(end -> {
                        if (logger.isDebugEnabled()) {
                            logger.debug("--------------------------  REST OUT  ---------------------------------");
                            logger.debug("REST URL : " + url);
                            logger.debug("Port : " + port);
                            logger.debug("Path : " + path);
                            logger.debug("Param : " + sendContent.encode());
                            logger.debug("-----------------------------------------------------------------------");
                        }

                        client.close();
                    });
                });

                clientRequest.putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(sendContent.encode().getBytes().length));
                clientRequest.putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
                clientRequest.write(sendContent.encode()).end();

            } catch (Exception e) {
                logger.error("Error : ", e);
                message.fail(100, e.toString());
            }

        };
    }


}
