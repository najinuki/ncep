package com.ntels.engine.verticle;

import com.ntels.engine.router.site.DefaultFilter;
import com.ntels.engine.router.site.nipa.NipaFilter;
import com.ntels.engine.router.site.ProcessFilter;
import com.ntels.engine.router.site.smartcampus.SmartCampusFilter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Rule 결과 처리 Verticle
 */
public class RuleResultVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());
    private ProcessFilter pf;

    @Override
    public void start() throws Exception {
        initFilter();
        vertx.eventBus().consumer("out.rule.result", outResult());
    }

    public void initFilter() {
        String site = config().getString("site");

        if ("SmartCampus".equals(site)) {
            pf = new SmartCampusFilter(config().getJsonObject("ncep.db"));
        } else if ("NIPA".equals(site)) {
            pf = new NipaFilter(config().getJsonObject("ncep.db"));
        } else {
            pf = new DefaultFilter();
        }
    }

    private Handler<Message<JsonObject>> outResult() {
        return message -> {
            try {
                JsonObject param = message.body();
                JsonObject data = param.getJsonObject("data");

                String sql = "SELECT rule_id, send_type, send_info, send_content, send_method, last_send_time, " +
                        " rule_name, rule_descp FROM rule WHERE rule_id = ?";
                JsonObject cache = new JsonObject();
                cache.put("cache", "rule");
                cache.put("sql", sql);
                cache.put("args", new JsonArray().add(param.getString("rule_id")));
                vertx.eventBus().send("cache.select", cache ,result -> {
                    if (result.failed()) {
                        result.cause().printStackTrace();
                    } else {
                        JsonArray sendList = (JsonArray) result.result().body();
                        JsonArray line = sendList.getJsonArray(0);

                        String sendType = line.getString(1);
                        String send_method = line.getString(4);
                        String last_send_time = StringUtils.defaultString(line.getString(5));

                        logger.debug(">> last_send_time : " + last_send_time);

                        // send_method(11: 매번, 12: 최초 1회, 13: 시간당 1회, 14: 하루에 1회)
                        if ("12".equals(send_method) && !"".equals(last_send_time)) {
                            return;
                        } else if ("13".equals(send_method) && !"".equals(last_send_time)) {
                            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime ldt = LocalDateTime.parse(last_send_time, format);
                            LocalDateTime now = LocalDateTime.now().minusHours(1);

                            // 현재시간보다 1시간을 뺀 시간과 룰이 가장 마지막에 실행됐던 시간을 비교
                            // last_send_time 이 더 클 경우 룰을 실행하지 않고 바로 리턴
                            if (ldt.isAfter(now)) {
                                return;
                            }
                        } else if ("14".equals(send_method) && !"".equals(last_send_time)) {
                            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                            LocalDateTime ldt = LocalDateTime.parse(last_send_time, format);
                            LocalDateTime now = LocalDateTime.now().minusDays(1);

                            // 현재시간보다 1일을 뺀 시간과 룰이 가장 마지막에 실행됐던 시간을 비교
                            // last_send_time 이 더 클 경우 룰을 실행하지 않고 바로 리턴
                            if (ldt.isAfter(now)) {
                                return;
                            }
                        }

                        String updateSql = "UPDATE Rule SET last_send_time = ? WHERE rule_id = ? ";

                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        JsonObject updateCache = new JsonObject();
                        updateCache.put("cache", "rule");
                        updateCache.put("sql", updateSql);
                        updateCache.put("args", new JsonArray().add(sdf.format(today)).add(String.valueOf(line.getLong(0))));
                        vertx.eventBus().send("cache.update", updateCache);

                        JsonObject sendInfo = new JsonObject();
                        sendInfo.put("rule_id", String.valueOf(line.getLong(0)));
                        sendInfo.put("send_type", line.getString(1));
                        sendInfo.put("send_info", new JsonObject(line.getString(2)));

                        String content = regexParameter(line.getString(3), data);
                        logger.debug("content : " + content);
                        String sendContent = regexFunction(content);

                        if (sendContent.length() == 0) {
                            sendInfo.put("send_content", line.getString(3));
                        } else {
                            sendInfo.put("send_content", sendContent);
                        }
                        sendInfo.put("send_content", sendInfo.getString("send_content").trim());

                        JsonObject logObj = insertRuleHistory(line, sendInfo.getString("send_content"));
                        pf.afterProcess(logObj);

                        if ("REST".equals(sendType)) {
                            vertx.eventBus().send("send.rest", sendInfo);
                        } else if ("E-Mail".equals(sendType)) {
                            vertx.eventBus().send("send.email", sendInfo);
                        } else if ("DB".equals(sendType)) {
                            vertx.eventBus().send("put.db", sendInfo);
                        }

                    }
                });

            } catch (Exception e) {
                logger.error("Error : ", e);
                message.fail(100, e.toString());
            }

        };
    }

    private String regexParameter(String content, JsonObject data) {
        final String regex = "\\{[0-9]*\\}";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(content);

        StringBuffer sendContent = new StringBuffer();
        while (match.find()) {
            String matchNum = match.group().replaceAll("[\\{\\}]", "");
            String matchVal = String.valueOf(data.getValue(matchNum));

            match.appendReplacement(sendContent, matchVal);
        }
        match.appendTail(sendContent);

        return sendContent.toString();
    }

    private String regexFunction(String content) {
        final String regex = "ntels:base64_encode\\(\\S*\\)";
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(content);

        StringBuffer sendContent = new StringBuffer();
        while (match.find()) {
            logger.debug("match.group() : " + match.group());
            String changeStr = match.group().replace("ntels:base64_encode(", "").replace(")", "");
            logger.debug(">>> changeStr : " + changeStr);
            match.appendReplacement(sendContent, Base64.getEncoder().encodeToString(changeStr.getBytes()));
        }
        match.appendTail(sendContent);

        return sendContent.toString();
    }

    private JsonObject insertRuleHistory(JsonArray line, String sendContent) {
        String rule_id = String.valueOf(line.getLong(0));
        String rule_name = line.getString(6);
        String rule_descp = line.getString(7);

        JsonObject logObj = new JsonObject();
        logObj.put("rule_id", rule_id);
        logObj.put("rule_name", rule_name);
        logObj.put("rule_descp", rule_descp);
        logObj.put("send_content", sendContent);

        return logObj;
    }
}
