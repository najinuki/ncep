package com.ntels.engine.verticle.output;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import io.vertx.ext.mail.StartTLSOptions;
import org.apache.log4j.Logger;

/**
 * Rule 결과를 E-Mail 전송 처리하는 Class
 */
public class EmailOutVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer("send.email", sendEmail());
    }

    private Handler<Message<JsonObject>> sendEmail() {
        return message -> {
            try {
                logger.debug("sendEmail start : " + message.body().encode());
                JsonObject param = message.body();

                JsonObject sendInfo = param.getJsonObject("send_info");
                String to = sendInfo.getString("to");
                String title = sendInfo.getString("title");
                String sendContent = param.getString("send_content");

                JsonObject mailInfo = config().getJsonObject("ncep.out.email");

                MailConfig config = new MailConfig();
                config.setHostname(mailInfo.getString("host"));
                config.setPort(mailInfo.getInteger("port"));
                config.setStarttls(StartTLSOptions.REQUIRED);
                config.setUsername(mailInfo.getString("user"));
                config.setPassword(mailInfo.getString("password"));
                MailClient mailClient = MailClient.createNonShared(vertx, config);

                MailMessage mailMessage = new MailMessage();
                mailMessage.setFrom("ncepEngine@gmail.com");
                mailMessage.setTo(to);
                mailMessage.setSubject(title);
                mailMessage.setHtml(sendContent);

                mailClient.sendMail(mailMessage, result -> {
                    if (result.failed()) {
                        logger.error("Error : ", result.cause());
                    }
                });
            } catch (Exception e) {
                logger.error("Error : ", e);
                message.fail(100, e.toString());
            }

        };
    }


}
