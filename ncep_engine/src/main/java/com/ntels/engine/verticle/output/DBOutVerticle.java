package com.ntels.engine.verticle.output;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Rule 결과를 DB Table 에 Insert or Update 처리하는 Class
 */
public class DBOutVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer("put.db", putDB());
    }

    private Handler<Message<JsonObject>> putDB() {
        return message -> {
            try {
                logger.debug("putDB start : " + message.body().encode());
                JsonObject param = message.body();
                JsonObject sendInfo = param.getJsonObject("send_info");

                String url = sendInfo.getString("conn_url");
                String id = sendInfo.getString("conn_id");
                String pw = sendInfo.getString("conn_pw");
                String port = sendInfo.getString("conn_port");
                String db = sendInfo.getString("conn_db");
                String sendContent = param.getString("send_content");
                db = db + "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC";

                try (Connection conn = DriverManager.getConnection("jdbc:mysql://" + url + ":" + port + "/" + db, id, pw)) {
                    try (PreparedStatement st = conn.prepareStatement(sendContent)) {
                        st.executeUpdate();
                    }
                } catch (Exception e) {
                    logger.error("Error : ", e);
                } finally {
                    message.reply("Success");
                }

            } catch (Exception e) {
                logger.error("Error : ", e);
            } finally {
                message.reply("Success");
            }
        };
    }

}
