package com.ntels.engine.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

public class WebsocketOutVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());

    @Override
    public void start () throws Exception {
        vertx.eventBus().consumer("out.websocket", outWebSocket());
    }

    private Handler<Message<JsonObject>> outWebSocket() {
        return message -> {
            try {
                HttpClient client = vertx.createHttpClient();
                JsonObject config = config().getJsonObject("ncep.web.socket");

                client.websocket(config.getInteger("port"), config.getString("url"), "/engineSimulator", w -> {
                    w.writeTextMessage(message.body().encode()).end();
                });

            } catch (Exception e) {
                logger.error("Error : ", e);
                message.fail(100, e.toString());
            }
        };
    }

}
