package com.ntels.engine.verticle.input;

import com.ntels.engine.router.rest.EventRouter;
import com.ntels.engine.router.rest.RuleRouter;
import com.ntels.engine.router.site.DefaultFilter;
import com.ntels.engine.router.site.nipa.NipaFilter;
import com.ntels.engine.router.site.ProcessFilter;
import com.ntels.engine.router.site.smartcampus.SmartCampusFilter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * HTTP REST Input Verticle
 * 최초 REST 요청 시 처리하는 Verticle
 */
public class RESTInVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());
    private ProcessFilter pf;

    @Override
    public void start() throws Exception {
        initFilter();
        vertx.createHttpServer().requestHandler(this::router).listen(7791);
    }

    public void initFilter() {
        String site = config().getString("site");

        if ("SmartCampus".equals(site)) {
            pf = new SmartCampusFilter(config().getJsonObject("ncep.db"));
        } else if ("NIPA".equals(site)) {
            pf = new NipaFilter(config().getJsonObject("ncep.db"));
        } else {
            pf = new DefaultFilter();
        }
    }

    public void router(HttpServerRequest request) {
        EventRouter eventRouter = new EventRouter(vertx);
        RuleRouter ruleRouter = new RuleRouter(vertx);

        Router rootRouter = Router.router(vertx);
        rootRouter.route().handler(BodyHandler.create());

        rootRouter.route("/*").handler(rc -> {
            JsonObject param;
            if (rc.getBody().length() == 0) {
                param = new JsonObject();
            } else {
                param = rc.getBodyAsJson();
            }

            // Request 확인을 위해 5자리 난수 발생
            String req_key = String.format("%05d", new Random().nextInt(100000));
            rc.put("req_key", req_key);

            printRESTLog(rc.request(), param, req_key);
            rc.next();

        });

        rootRouter.route(HttpMethod.POST, "/input/event").handler(eventRouter::addEvent);
        rootRouter.route(HttpMethod.DELETE, "/input/event/:event_id").handler(eventRouter::delEvent);
        rootRouter.route(HttpMethod.PUT, "/input/event/:event_id").handler(eventRouter::modifyEvent);
        rootRouter.route(HttpMethod.PUT, "/input/event/:event_id/init").handler(eventRouter::modifyInitValue);
        rootRouter.route(HttpMethod.GET, "/input/event").handler(eventRouter::getEventList);
        rootRouter.route(HttpMethod.GET, "/rule/:rule_id/input/event").handler(eventRouter::getEventListByRule);
        rootRouter.route(HttpMethod.GET, "/input/event/:event_id").handler(eventRouter::getEventDetail);
        rootRouter.route(HttpMethod.POST, "/input/event/check").handler(eventRouter::eventCheck);

        rootRouter.route(HttpMethod.POST, "/rule").handler(ruleRouter::addRule);
        rootRouter.route(HttpMethod.DELETE, "/rule/:rule_id").handler(ruleRouter::delRule);
        rootRouter.route(HttpMethod.PUT, "/rule/:rule_id").handler(ruleRouter::modifyRule);
        rootRouter.route(HttpMethod.GET, "/rule").handler(ruleRouter::getRuleList);
        rootRouter.route(HttpMethod.GET, "/rule/hist").handler(ruleRouter::getRuleHistList);
        rootRouter.route(HttpMethod.GET, "/rule/:rule_id").handler(ruleRouter::getRuleDetail);
        rootRouter.route(HttpMethod.GET, "/input/event/:event_id/rule").handler(ruleRouter::getRuleListByEvent);
        rootRouter.route(HttpMethod.POST, "/rule/validate").handler(ruleRouter::validateRule);

        rootRouter.route(HttpMethod.POST, "/send/event").handler(rc -> {
            JsonObject param = pf.beforeProcess(rc.getBodyAsJson().put("protocol", "REST"));
            rc.setBody(param.toBuffer());
            eventRouter.sendEvent(rc);
        });

        rootRouter.accept(request);
    }

    private void printRESTLog(HttpServerRequest req, JsonObject obj, String req_key) {
        if (logger.isDebugEnabled()) {
            logger.debug("--------------------------  REST IN  ---------------------------------");
            logger.debug("REQ_KEY : " + req_key);
            logger.debug("REST URL : " + req.absoluteURI());
            logger.debug("Method : " + req.method().name());
            logger.debug("Param : " + obj.encode());
            logger.debug("-----------------------------------------------------------------------");
        }
    }

}
