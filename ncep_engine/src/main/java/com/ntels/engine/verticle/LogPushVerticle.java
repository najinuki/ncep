package com.ntels.engine.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * ElasticSearch or MySQL DB 에 히스토리 데이터 적재하는 Verticle
 */
public class LogPushVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());
    private RedisClient redis;

    @Override
    public void start() throws Exception {
        String version = config().getString("version");

        if ("ELK".equals(version)) {
            try {
                JsonObject config = config().getJsonObject("redis.send");

                // Log 전송을 위한 Redis 셋팅
                RedisOptions redisOptions = new RedisOptions();
                redisOptions.setHost(config.getString("url"));
                redisOptions.setPort(config.getInteger("port"));

                redis = RedisClient.create(vertx, redisOptions);
            } catch (Exception e) {
                logger.error("Redis Error : " + e.getMessage());
                e.printStackTrace();
            }

            vertx.eventBus().consumer("log.event.history.insert", sendEventRedis());
            vertx.eventBus().consumer("log.rule.history.insert", sendRuleRedis());
        } else if ("MySQL".equals(version)) {
            vertx.eventBus().consumer("log.event.history.insert", insertEventHistory());
            vertx.eventBus().consumer("log.rule.history.insert", insertRuleHistory());
        }

    }

    private Handler<Message<JsonObject>> sendEventRedis() {
        return message ->  {
            try {
                JsonObject body = message.body();
                logger.debug("sendEventRedis : " + body.encode());

                String key = "event";
                JsonObject event = body.getJsonObject("event");
                String eventName = body.getString("event_name");
                String eventType = body.getString("event_type");

                JsonObject value = new JsonObject();
                value.put("event_name", eventName);
                value.put("event_type", eventType);
                value.put("event", event);

                redis.lpush(key, value.encode(), null);

            } catch (Exception e) {
                logger.error("Error : ", e);
            } finally {
                message.reply("Success");
            }
        };
    }

    private Handler<Message<JsonObject>> sendRuleRedis() {
        return message ->  {
            try {
                JsonObject body = message.body();
                logger.debug("sendRuleRedis : " + body.encode());

                String key = "rule";
                String rule_id = body.getString("rule_id");
                String rule_name = StringUtils.defaultString(body.getString("rule_name"));
                String rule_descp = StringUtils.defaultString(body.getString("rule_descp"));
                String send_content = StringUtils.defaultString(body.getString("send_content"));

                JsonObject value = new JsonObject();
                value.put("rule_id", rule_id);
                value.put("rule_name", rule_name);
                value.put("rule_descp", rule_descp);
                value.put("send_content", send_content);

                redis.lpush(key, value.encode(), null);

            } catch (Exception e) {
                logger.error("Error : ", e);
            } finally {
                message.reply("Success");
            }
        };
    }

    private Handler<Message<JsonObject>> insertEventHistory() {
        return message ->  {
            try {
                JsonObject body = message.body();
                logger.debug("insertEventHistory : " + body.encode());

                String event_name = StringUtils.defaultString(body.getString("event_name"));
                String event_type = StringUtils.defaultString(body.getString("event_type"));
                String event = StringUtils.defaultString(body.getJsonObject("event").encode());

                JsonObject config = config().getJsonObject("ncep.db");
                String sql = "INSERT INTO event_history (event_name, event_type, event, insert_date, update_date) " +
                        " VALUES (?, ?, ?, NOW(), NOW())";
                JsonArray params = new JsonArray();
                params.add(event_name).add(event_type).add(event);

                try (Connection conn = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("password"))) {
                    try (PreparedStatement st = conn.prepareStatement(sql)) {
                        for (int i = 0; i < params.size(); i++) {
                            st.setString(i+1, params.getString(i));
                        }

                        st.executeUpdate();
                    }
                } catch (Exception e) {
                    logger.error("Error : ", e);
                } finally {
                    message.reply("Success");
                }

            } catch (Exception e) {
                logger.error("Error : ", e);
            } finally {
                message.reply("Success");
            }
        };
    }

    private Handler<Message<JsonObject>> insertRuleHistory() {
        return message ->  {
            try {
                JsonObject body = message.body();
                logger.debug("insertRuleHistory : " + body.encode());

                String rule_id = body.getString("rule_id");
                String rule_name = StringUtils.defaultString(body.getString("rule_name"));
                String rule_descp = StringUtils.defaultString(body.getString("rule_descp"));
                String send_content = StringUtils.defaultString(body.getString("send_content"));

                JsonObject config = config().getJsonObject("ncep.db");
                String sql = "INSERT INTO rule_history (rule_id, rule_name, rule_descp, send_content, " +
                        " insert_date, update_date) VALUES (?, ?, ?, ?, NOW(), NOW())";
                JsonArray params = new JsonArray();
                params.add(rule_id).add(rule_name).add(rule_descp).add(send_content);

                try (Connection conn = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("password"))) {
                    try (PreparedStatement st = conn.prepareStatement(sql)) {
                        for (int i = 0; i < params.size(); i++) {
                            st.setString(i+1, params.getString(i));
                        }

                        st.executeUpdate();
                    }
                } catch (Exception e) {
                    logger.error("Error : ", e);
                } finally {
                    message.reply("Success");
                }

            } catch (Exception e) {
                logger.error("Error : ", e);
            } finally {
                message.reply("Success");
            }
        };
    }

}
