package com.ntels.engine.verticle.input;

import com.ntels.engine.router.kafka.KafkaRouter;
import com.ntels.engine.router.site.DefaultFilter;
import com.ntels.engine.router.site.ProcessFilter;
import com.ntels.engine.router.site.nipa.NipaFilter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Logger;

import java.util.Properties;

/**
 * Apache Kafka Input Verticle
 * 최초 Kafka 요청 시 처리하는 Verticle
 */
public class KafkaInVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());
    private ProcessFilter pf;

    public void initFilter() {
        String site = config().getString("site");

        if ("NIPA".equals(site)) {
            pf = new NipaFilter(config().getJsonObject("ncep.db"));
        } else {
            pf = new DefaultFilter();
        }
    }

    private static Properties createProperties() {
        Properties props = new Properties();
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "nisbcp-kafka");
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.13.20:19922");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        return props;
    }

    @Override
    public void start() throws Exception {
        initFilter();
        KafkaRouter kafkaRouter = new KafkaRouter(vertx);

        System.out.println("Kafka Consumer Set Property");
        Properties props = createProperties();

        // use consumer for interacting with Apache Kafka
        System.out.println("Create the consumer using props.");
        KafkaConsumer<String, String> consumer = KafkaConsumer.create(vertx, props);

        System.out.println("Set Topic 'hist'");
        consumer.subscribe("hist", ar -> {
            if (ar.succeeded()) {
                logger.debug("Success Subscribed");
            } else {
                logger.debug("Could not subscribe " + ar.cause().getMessage());
            }
        });

        System.out.println("Consumer Records Start");
        consumer.handler(record -> {
            System.out.println("Consumer handler start");
//            logger.debug("Key: " + record.key());
//            logger.debug("Value: " + record.value());
//            logger.debug("Partition: " + record.partition());
//            logger.debug("Offset: " + record.offset());

            System.out.println("Processing key=" + record.key() + ",value=" + record.value() +
                    ",partition=" + record.partition() + ",offset=" + record.offset());

            JsonObject jsonObject = new JsonObject(record.value());
            jsonObject.put("protocol", "Kafka");
            JsonObject param = pf.beforeProcess(jsonObject);

            kafkaRouter.sendEvent(param);

        }).exceptionHandler(e -> {
            logger.debug("Kafka Consumer Exception");
            logger.debug("Error: " + e.getMessage());
        });

        //consumer.close(res -> {
        //    if (res.succeeded()) {
        //        logger.debug("Consumer is now closed");
        //    } else {
        //        logger.debug("Close failed");
        //    }
        //});
    }
}
