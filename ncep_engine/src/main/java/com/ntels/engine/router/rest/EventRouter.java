package com.ntels.engine.router.rest;

import com.ntels.engine.common.ErrorCode;
import com.ntels.engine.common.NCEPException;
import com.ntels.engine.rule.RuleService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * REST Event Router 클래스
 */
public class EventRouter extends BaseRouter {

    protected Logger logger = Logger.getLogger(getClass());
    private Vertx vertx;

    public EventRouter(Vertx vertx) {
        this.vertx = vertx;
    }

    public void addEvent(RoutingContext rc) {
        try {
            JsonObject param = rc.getBodyAsJson();
            checkMandatoryValue(param, "event_name", "event_name_order", "event_type", "init_value_yn", "event_query");

            vertx.eventBus().send("cache.input.event.insert", param, result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    Long event_id = (Long) result.result().body();
                    this.responseSuccess(rc, 200, new JsonObject().put("event_id", event_id).encode());
                }
            });
        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void delEvent(RoutingContext rc) {
        try {
            String event_id = StringUtils.defaultString(rc.request().getParam("event_id"));
            if ("".equals(event_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : event_id is Required.");
            }

            String sql = "SELECT count(*) as cnt " +
                         "FROM RuleInputEvent WHERE event_id = ?";
            JsonObject cache = new JsonObject();
            cache.put("cache", "rule_input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(event_id));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    result.cause().printStackTrace();
                } else {
                    JsonArray data = (JsonArray) result.result().body();
                    JsonArray cnt_info = data.getJsonArray(0);
                    int cnt = cnt_info.getInteger(0);

                    if (cnt == 0) {
                        JsonObject param = new JsonObject();
                        param.put("event_id", Long.parseLong(event_id));
                        vertx.eventBus().send("cache.input.event.delete", param , delete_result -> {
                            if (delete_result.failed()) {
                                this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                            } else {
                                boolean ret = (boolean) delete_result.result().body();
                                if (ret == false) {
                                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_NOT_EXIST_ERROR, ErrorCode.DATA_NOT_EXIST_ERROR_MSG));
                                } else {
                                    this.responseSuccess(rc, 200);
                                }
                            }
                        });
                    } else {
                        this.responseFail(rc, new NCEPException(ErrorCode.RULE_DATA_EXIST_ERROR, ErrorCode.RULE_DATA_EXIST_ERROR_MSG));
                    }
                }
            });
        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void modifyEvent(RoutingContext rc) {
        try {
            String event_id = StringUtils.defaultString(rc.request().getParam("event_id"));

            if (event_id.equals("")) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter: event_id is required.");
            }

            JsonObject bodyParam = rc.getBodyAsJson();

            String event_name = StringUtils.defaultString(bodyParam.getString("event_name"));
            String event_name_order = bodyParam.getJsonArray("event_name_order").encode();
            String event_type = StringUtils.defaultString(bodyParam.getString("event_type"));
            String event_query = StringUtils.defaultString(bodyParam.getString("event_query"));

            String update_sql = "UPDATE InputEvent SET event_name = ?, event_name_order = ?, event_type = ?, event_query = ? WHERE event_id = ? ";

            JsonArray update_args = new JsonArray();
            update_args.add(event_name).add(event_name_order).add(event_type).add(event_query).add(event_id);

            JsonObject param = new JsonObject();
            param.put("cache", "input_event");
            param.put("sql", update_sql);
            param.put("args", update_args);

            vertx.eventBus().send("cache.update", param ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    this.responseSuccess(rc, 200);
                }
            });

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void modifyInitValue(RoutingContext rc) {
        try {
            String event_id = StringUtils.defaultString(rc.request().getParam("event_id"));

            if ("".equals(event_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : event_id is Required.");
            }

            JsonObject param = rc.getBodyAsJson();
            JsonArray init_values = param.getJsonArray("init_values");

            String selectSql = "SELECT a.rule_id, c.event_name, c.event_name_order, c.init_value FROM Rule a, " +
                    " \"rule_input_event\".RuleInputEvent as b, \"input_event\".InputEvent as c WHERE a.rule_id = b.rule_id " +
                    " and b.event_id = c.event_id and c.event_id = ? ";
            JsonObject selectCache = new JsonObject();
            selectCache.put("cache", "rule");
            selectCache.put("sql", selectSql);
            selectCache.put("args", new JsonArray().add(event_id));
            vertx.eventBus().send("cache.select", selectCache, result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray ruleEventList = (JsonArray) result.result().body();

                    for (int i = 0; i < ruleEventList.size(); i++) {
                        JsonArray line = ruleEventList.getJsonArray(i);

                        String rule_id = line.getString(0);
                        String eventName = line.getString(1);
                        JsonArray arrEventNameOrder = new JsonArray(line.getString(2));

                        for (int j = 0; j < init_values.size(); j++) {
                            JsonObject initValue = init_values.getJsonObject(j);
                            String insertSql = "select ";

                            for (int k = 0; k < arrEventNameOrder.size(); k++) {
                                String key = arrEventNameOrder.getString(k);

                                if (initValue.getValue(key) instanceof String) {
                                    if (k == arrEventNameOrder.size() - 1) {
                                        insertSql += "'" + initValue.getValue(key) + "' as " + key + " insert into " + eventName + ";";
                                    } else {
                                        insertSql += "'" + initValue.getValue(key) + "' as " + key + ", ";
                                    }
                                } else {
                                    if (k == arrEventNameOrder.size() - 1) {
                                        insertSql += initValue.getValue(key) + " as " + key + " insert into " + eventName + ";";
                                    } else {
                                        insertSql += initValue.getValue(key) + " as " + key + ", ";
                                    }
                                }
                            }

                            logger.debug("insertSql : " + insertSql);
                            RuleService.insertTableData("rule_" + rule_id, insertSql);
                        }
                    }

                    String preInitValue = ruleEventList.getJsonArray(0).getString(3);
                    JsonArray aftInitValues = new JsonArray(preInitValue);
                    for (int i = 0; i < init_values.size(); i++) {
                        aftInitValues.add(init_values.getJsonObject(i));
                    }

                    String updateSql = "UPDATE InputEvent SET init_value = ? WHERE event_id = ? ";

                    JsonObject updateCache = new JsonObject();
                    updateCache.put("cache", "event");
                    updateCache.put("sql", updateSql);
                    updateCache.put("args", new JsonArray().add(aftInitValues.encode()).add(event_id));
                    vertx.eventBus().send("cache.update", updateCache, result2 -> {
                        if (result2.failed()) {
                            this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                        } else {

                            this.responseSuccess(rc, 200, new JsonObject().encode());
                        }
                    });

                }
            });
        System.out.println("out modifyInitValue");

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getEventList(RoutingContext rc) {
        try {
            String event_type = StringUtils.defaultString(rc.request().getParam("event_type"));
            logger.debug("> event_type : " + event_type);

            String sql = "SELECT event_id, event_name, event_descp, event_type, init_value_yn, \n" +
                         "init_value, event_query, insert_date \n" +
                         "FROM InputEvent \n" +
                         "WHERE event_id IN \n" +
                         "( \n" +
                         "SELECT event_id FROM InputEvent \n" +
                         "WHERE event_type = '10' OR event_type = '11' \n" +
                         "UNION ALL \n" +
                         "SELECT DISTINCT event_id FROM InputEvent \n" +
                         "WHERE event_type = '12' AND event_id NOT IN \n" +
                         "(SELECT event_id FROM \"rule_input_event\".RuleInputEvent) \n" +
                         ");";

            // 일부 쿼리가 view에 사용중이지 않아 주석처리
            //String sql = "SELECT event_id, event_name, event_descp, event_type, init_value_yn, init_value, event_query, " +
            //          "insert_date FROM InputEvent ";
            //if (!"".equals(event_type)) {
            //    sql += " WHERE event_type = '" + event_type + "'";
            //}
            //sql += " ORDER BY event_id ASC";

            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray());
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray eventList = (JsonArray) result.result().body();
                    JsonArray resultList = new JsonArray();
                    for (int i = 0; i < eventList.size(); i++) {
                        JsonArray line = eventList.getJsonArray(i);
                        JsonObject event = new JsonObject();
                        event.put("event_id", line.getLong(0));
                        event.put("event_name", line.getString(1));
                        event.put("event_descp", line.getString(2));
                        event.put("event_type", line.getString(3));
                        event.put("init_value_yn", line.getString(4));
                        event.put("init_value", line.getString(5));
                        event.put("event_query", line.getString(6));
                        event.put("insert_date", line.getString(7));

                        resultList.add(event);
                    }

                    this.responseSuccess(rc, 200, resultList.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getEventDetail(RoutingContext rc) {
        try {
            String event_id = StringUtils.defaultString(rc.request().getParam("event_id"));
            if ("".equals(event_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : event_id is Required.");
            }

            String sql = "SELECT event_id, event_name, event_descp, event_type, init_value_yn, init_value, event_query, " +
                    "insert_date FROM InputEvent WHERE event_id = ?";
            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(event_id));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray eventList = (JsonArray) result.result().body();
                    JsonArray line = eventList.getJsonArray(0);
                    JsonObject event = new JsonObject();
                    event.put("event_id", line.getLong(0));
                    event.put("event_name", line.getString(1));
                    event.put("event_descp", line.getString(2));
                    event.put("event_type", line.getString(3));
                    event.put("init_value_yn", line.getString(4));
                    event.put("init_value", line.getString(5));
                    event.put("event_query", line.getString(6));
                    event.put("insert_date", line.getString(7));

                    this.responseSuccess(rc, 200, event.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getEventListByRule(RoutingContext rc) {
        try {
            String rule_id = StringUtils.defaultString(rc.request().getParam("rule_id"));
            if ("".equals(rule_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : rule_id is Required.");
            }

            String sql = "SELECT a.event_id, a.event_name, a.event_descp, a.event_type, a.init_value_yn, a.init_value, " +
                    "a.event_query, a.insert_date, b.rule_id FROM InputEvent a, \"rule_input_event\".RuleInputEvent as b " +
                    "WHERE a.event_id = b.event_id and b.rule_id = ?";
            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(rule_id));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray eventList = (JsonArray) result.result().body();
                    JsonArray resultList = new JsonArray();
                    for (int i = 0; i < eventList.size(); i++) {
                        JsonArray line = eventList.getJsonArray(i);
                        JsonObject event = new JsonObject();
                        event.put("event_id", line.getLong(0));
                        event.put("event_name", line.getString(1));
                        event.put("event_descp", line.getString(2));
                        event.put("event_type", line.getString(3));
                        event.put("init_value_yn", line.getString(4));
                        event.put("init_value", line.getString(5));
                        event.put("event_query", line.getString(6));
                        event.put("insert_date", line.getString(7));
                        event.put("rule_id", line.getLong(8));

                        resultList.add(event);
                    }

                    this.responseSuccess(rc, 200, resultList.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void eventCheck(RoutingContext rc) {
        try {
            JsonObject param = rc.getBodyAsJson();
            checkMandatoryValue(param, "event_name");
            String eventName = StringUtils.defaultString(param.getString("event_name"));

            String sql = "SELECT COUNT(*) FROM InputEvent WHERE event_name = ?";
            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(eventName));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray eventList = (JsonArray) result.result().body();
                    int cnt = eventList.getJsonArray(0).getInteger(0);

                    if (cnt == 0) {
                        this.responseSuccess(rc, 200);
                    } else {
                        this.responseFail(rc, "Event Name is Duplicate");
                    }
                }
            });

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void sendEvent(RoutingContext rc) {
        try {
            JsonObject param = rc.getBodyAsJson();
            checkMandatoryValue(param, "event_name", "event");

            // 들어오는 Event Log 적재
            //vertx.eventBus().send("log.event.history.insert", param);

            String eventName = StringUtils.defaultString(param.getString("event_name"));
            JsonObject event = param.getJsonObject("event");
            String isSimulator = param.getString("isSimulator");

            if ("true".equals(isSimulator)) {
                JsonObject sendObj = new JsonObject();
                sendObj.put("msg", "Send Event Success!!\n" + event.encode());
                sendObj.put("isSimulator", "true");

                vertx.eventBus().send("out.websocket", sendObj);
            }

            // event_name으로 모든 Rule을 조회해서 해당되는 룰 모두에게 sendEvent 전송
            String sql = "SELECT b.rule_id, a.event_name_order FROM InputEvent as a, \"rule_input_event\".RuleInputEvent as b ";
            sql += " WHERE a.event_id = b.event_id and a.event_name = ?";
            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(eventName));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    logger.error("sendEvent error : ", result.cause());
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    try {
                        JsonArray ruleList = (JsonArray) result.result().body();
                        for (int i = 0; i < ruleList.size(); i++) {
                            Long ruleId = ruleList.getJsonArray(i).getLong(0);
                            String eventNameOrder = ruleList.getJsonArray(i).getString(1);

                            RuleService.sendEvent("rule_" + ruleId, eventName, event, new JsonArray(eventNameOrder));
                        }

                        this.responseSuccess(rc, 200);
                    } catch (Exception e) {
                        logger.error("Error : ", e);
                        this.responseFail(rc, "(" + ErrorCode.RULE_EXECUTE_ERROR + ") " + ErrorCode.RULE_EXECUTE_ERROR_MSG);
                    }

                }
            });

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

}
