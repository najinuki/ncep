package com.ntels.engine.router.site;

import io.vertx.core.json.JsonObject;

public class DefaultFilter extends ProcessFilter {

    public JsonObject beforeProcess(JsonObject param) {
        return param;
    }

    public JsonObject afterProcess(JsonObject param) {
        return param;
    }

}
