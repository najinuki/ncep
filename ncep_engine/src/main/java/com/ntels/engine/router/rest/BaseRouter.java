package com.ntels.engine.router.rest;

import com.ntels.engine.common.ErrorCode;
import com.ntels.engine.common.NCEPException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.log4j.Logger;

/**
 * 기본 Router Class
 */
public class BaseRouter {

    private Logger logger = Logger.getLogger(getClass());

    /**
     * 필수값 체크 함수
     * @param param 파라메터
     * @param names 필수값 이름
     * @throws Exception
     */
    protected void checkMandatoryValue(JsonObject param, String... names) throws Exception {
        for(String name : names) {
            if (param.getValue(name) == null) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, name + " is Required.");
            }
        }
    }

    /**
     * REST 응답 실패
     * @param rc RoutingContext
     * @param ne NCEPException
     */
    protected void responseFail(RoutingContext rc, NCEPException ne) {
        JsonObject result = new JsonObject();
        result.put("error_msg", ne.getErrorMsg());

        rc.response().putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(result.encode().getBytes().length));
        rc.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        rc.response().setStatusCode(400);
        rc.response().write(result.encode());
        rc.response().end();

        printRESTLog(rc, 400, result.encode());
    }

    /**
     * REST 응답 실패
     * @param rc RoutingContext
     * @param errorMsg 에러메세지
     */
    protected void responseFail(RoutingContext rc, String errorMsg) {
        JsonObject result = new JsonObject();
        result.put("error_msg", errorMsg);

        rc.response().putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(result.encode().getBytes().length));
        rc.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        rc.response().setStatusCode(400);
        rc.response().write(result.encode());
        rc.response().end();

        printRESTLog(rc, 400, result.encode());
    }

    /**
     * REST 응답 성공
     * @param rc RoutingContext
     * @param statusCode StatusCode
     * @param result 결과 메세지
     */
    protected void responseSuccess(RoutingContext rc, int statusCode, String result) {
        rc.response().putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(result.getBytes().length));
        rc.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        rc.response().setStatusCode(statusCode);
        rc.response().write(result);
        rc.response().end();

        printRESTLog(rc, statusCode, result);
    }

    /**
     * REST 응답 성공
     * @param rc RoutingContext
     * @param statusCode StatusCode
     */
    protected void responseSuccess(RoutingContext rc, int statusCode) {
        rc.response().setStatusCode(statusCode);
        rc.response().end();

        printRESTLog(rc, statusCode, "");
    }

    protected void printRESTLog(RoutingContext rc, int statusCode, String result) {
        if (logger.isDebugEnabled()) {
            logger.debug("---------------------------  REST END  --------------------------------");
            logger.debug("REQ_KEY : " + rc.get("req_key"));
            logger.debug("STATUS_CODE : " + statusCode);
            logger.debug("Response : " + result);
            logger.debug("-----------------------------------------------------------------------");
        }
    }

}
