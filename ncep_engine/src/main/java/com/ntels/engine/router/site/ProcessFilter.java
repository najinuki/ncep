package com.ntels.engine.router.site;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.LinkedList;

/**
 * 전처리 기본 Class
 */
public abstract class ProcessFilter {

    private LinkedList<JsonObject> loglist = new LinkedList<>();
    private Logger logger = Logger.getLogger(getClass());

    public abstract JsonObject beforeProcess(JsonObject param);

    public abstract JsonObject afterProcess(JsonObject param);

    protected void insertEventLog(JsonObject param, JsonObject config, int size) {
        try {
            logger.debug("insertEventLog start");
            loglist.add(param);

            if (loglist.size() > size) {
                logger.debug("insertEventLog size is : " + size);

                String sql = "INSERT INTO event_history (event_name, event_type, event, insert_date, update_date) " +
                        " VALUES (?, ?, ?, NOW(), NOW())";
                Connection conn = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("password"));;
                conn.setAutoCommit(false);
                PreparedStatement ps = conn.prepareStatement(sql);

                for (int i = 0; i < loglist.size(); i++) {
                    JsonObject body = loglist.get(i);

                    String event_name = StringUtils.defaultString(body.getString("event_name"));
                    String event_type = StringUtils.defaultString(body.getString("event_type"));
                    String event = StringUtils.defaultString(body.getJsonObject("event").encode());

                    ps.setString(1, event_name);
                    ps.setString(2, event_type);
                    ps.setString(3, event);
                    ps.addBatch();
                }
                ps.executeBatch();
                conn.commit();

                ps.close();
                conn.close();
                loglist.clear();
            }

        } catch (Exception e) {
            logger.error("Error : ", e);
        }
    }

    protected void insertRuleLog(JsonObject param, JsonObject config) {
        try {
            logger.debug("insertRuleLog start");

            String rule_id = param.getString("rule_id");
            String rule_name = StringUtils.defaultString(param.getString("rule_name"));
            String rule_descp = StringUtils.defaultString(param.getString("rule_descp"));
            String send_content = StringUtils.defaultString(param.getString("send_content"));

            String sql = "INSERT INTO rule_history (rule_id, rule_name, rule_descp, send_content, " +
                    " insert_date, update_date) VALUES (?, ?, ?, ?, NOW(), NOW())";
            JsonArray params = new JsonArray();
            params.add(rule_id).add(rule_name).add(rule_descp).add(send_content);

            try (Connection conn = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("password"))) {
                try (PreparedStatement st = conn.prepareStatement(sql)) {
                    for (int i = 0; i < params.size(); i++) {
                        st.setString(i+1, params.getString(i));
                    }

                    st.executeUpdate();
                }
            } catch (Exception e) {
                logger.error("Error : ", e);
            }

        } catch (Exception e) {
            logger.error("Error : ", e);
        }
    }

}
