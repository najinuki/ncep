package com.ntels.engine.router.site.smartcampus;

import com.ntels.engine.router.site.ProcessFilter;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Base64;

/**
 * Smart Campus 전용 전처리 Class
 */
public class SmartCampusFilter extends ProcessFilter {

    private Logger logger = Logger.getLogger(getClass());
    private JsonObject config;

    public SmartCampusFilter(JsonObject config) {
        this.config = config;
    }

    public JsonObject beforeProcess(JsonObject param) {
        logger.debug("SmartCampusFilter beforeProcess");

        String event_type = StringUtils.defaultString(param.getString("event_type"));
        if ("home".equals(event_type)) {
            String content_value = param.getJsonObject("event").getString("content_value");
            if (content_value != null) {
                param.getJsonObject("event").put("content_value", new String(Base64.getDecoder().decode(content_value)));
            }
        }

        this.insertEventLog(param, config,5);

        return param;
    }

    public JsonObject afterProcess(JsonObject param) {
        logger.debug("SmartCampusFilter afterProcess");

        this.insertRuleLog(param, config);
        return param;
    }

}
