package com.ntels.engine.router.site.nipa;

import com.ntels.engine.router.site.ProcessFilter;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

/**
 * NIPA 전용 전처리 Class
 */
public class NipaFilter extends ProcessFilter {

    private Logger logger = Logger.getLogger(getClass());
    private JsonObject config;

    public NipaFilter(JsonObject config) {
        this.config = config;
    }

    public JsonObject beforeProcess(JsonObject param) {
        //logger.debug("NipaFilter beforeProcess");

        if ("Kafka".equals(param.getString("protocol"))) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("event_name", param.getString("type"));
            jsonObject.put("event_type", "building");
            jsonObject.put("event", param);

            return jsonObject;
        } else {
            return param;
        }

    }

    public JsonObject afterProcess(JsonObject param) {
        //logger.debug("NipaFilter afterProcess");

        this.insertRuleLog(param, config);
        return param;
    }

}
