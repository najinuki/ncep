package com.ntels.engine.router.rest;

import com.ntels.engine.common.ErrorCode;
import com.ntels.engine.common.NCEPException;
import com.ntels.engine.repository.CacheServiceVerticle;
import com.ntels.engine.rule.RuleService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * REST Rule Router 클래스
 */
public class RuleRouter extends BaseRouter {

    protected Logger logger = Logger.getLogger(getClass());
    private Vertx vertx;

    public RuleRouter(Vertx vertx) {
        this.vertx = vertx;
    }

    public void addRule(RoutingContext rc) {
        try {
            JsonObject param = rc.getBodyAsJson();
            checkMandatoryValue(param, "rule_name", "rule_query", "send_type", "send_info", "send_content",
                    "send_method", "event_info");
            String ruleQuery = StringUtils.defaultString(param.getString("rule_query"));

            String eventQuery = "";
            JsonArray eventInfo = param.getJsonArray("event_info");
            for (int i = 0; i < eventInfo.size(); i++) {
                String tmpQuery = eventInfo.getJsonObject(i).getString("event_query");
                eventQuery += tmpQuery + "\r\n";
            }

            long rule_id = CacheServiceVerticle.getMaxRuleId();

            String callbackName = "output_" + rule_id;
            param.put("callback_name", callbackName);

            String appName = "@App:name('rule_" + rule_id + "') \r\n";
            String query = appName + eventQuery + ruleQuery;

            if (ruleQuery.contains("insert into")) {
                int startIdx = ruleQuery.lastIndexOf("insert into");
                query = appName + eventQuery + ruleQuery.substring(0, startIdx) + "insert into output_" + rule_id + ";";
            } else {
                query += "insert into " + callbackName + ";";
            }

            RuleService.createRule(vertx, query, callbackName);

            String rule_query = query;
            initValueCheck(rule_id, eventInfo, result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonObject copyParam = param.copy();
                    copyParam.put("rule_id", rule_id);
                    copyParam.put("rule_query", rule_query);
                    vertx.eventBus().send("cache.rule.insert", copyParam, i_result -> {
                        if (i_result.failed()) {
                            this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                        } else {
                            this.responseSuccess(rc, 200, new JsonObject().put("rule_id", rule_id).encode());
                        }
                    });
                }
            });

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    private void initValueCheck(long rule_id, JsonArray eventInfo, Handler<AsyncResult<Void>> handler) {
        for (int i = 0; i < eventInfo.size(); i++) {
            int cnt = i;
            String event_id = eventInfo.getJsonObject(i).getString("event_id");
            String initValueYn = eventInfo.getJsonObject(i).getString("init_value_yn");

            String sql = "SELECT event_name, init_value, event_name_order FROM InputEvent WHERE event_id = ? ";
            JsonObject cache = new JsonObject();
            cache.put("cache", "input_event");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(event_id));

            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    handler.handle(Future.failedFuture(result.cause()));
                } else {
                    try {
                        // 만약 초기 데이터 유무가 'Y'이면 초기 데이터를 넣는다.
                        if ("Y".equals(initValueYn)) {

                            JsonArray eventList = (JsonArray) result.result().body();
                            JsonArray line = eventList.getJsonArray(0);
                            String eventName = line.getString(0);
                            JsonArray arrInitValue = new JsonArray(line.getString(1));
                            JsonArray arrEventNameOrder = new JsonArray(line.getString(2));

                            for (int j = 0; j < arrInitValue.size(); j++) {
                                JsonObject initValue = arrInitValue.getJsonObject(j);
                                String insertSql = "select ";
                                for (int k = 0; k < arrEventNameOrder.size(); k++) {
                                    String key = arrEventNameOrder.getString(k);

                                    if (initValue.getValue(key) instanceof String) {
                                        if (k == arrEventNameOrder.size() - 1) {
                                            insertSql += "'" + initValue.getValue(key) + "' as " + key + " insert into " + eventName + ";";
                                        } else {
                                            insertSql += "'" + initValue.getValue(key) + "' as " + key + ", ";
                                        }
                                    } else {
                                        if (k == arrEventNameOrder.size() - 1) {
                                            insertSql += initValue.getValue(key) + " as " + key + " insert into " + eventName + ";";
                                        } else {
                                            insertSql += initValue.getValue(key) + " as " + key + ", ";
                                        }
                                    }
                                }

                                logger.debug("insertSql : " + insertSql);
                                RuleService.insertTableData("rule_" + rule_id, insertSql);
                            }
                        }

                        if (cnt == eventInfo.size() - 1) {
                            handler.handle(Future.succeededFuture());
                        }

                    } catch (Exception e) {
                        logger.error("Error : ", e);
                        handler.handle(Future.failedFuture(result.cause()));
                    }
                }
            });
        }
    }

    public void delRule(RoutingContext rc) {
        try {
            String rule_id = StringUtils.defaultString(rc.request().getParam("rule_id"));
            if ("".equals(rule_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : rule_id is Required.");
            }

            RuleService.deleteRule("rule_" + rule_id);

            JsonObject cache = new JsonObject();
            cache.put("rule_id", Long.parseLong(rule_id));
            vertx.eventBus().send("cache.rule.delete", cache, result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    boolean ret = (boolean) result.result().body();
                    if (ret == false) {
                        this.responseFail(rc, new NCEPException(ErrorCode.DATA_NOT_EXIST_ERROR, ErrorCode.DATA_NOT_EXIST_ERROR_MSG));
                    } else {
                        this.responseSuccess(rc, 200);
                    }
                }
            });
        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void modifyRule(RoutingContext rc) {
        try {
            String rule_id = StringUtils.defaultString(rc.request().getParam("rule_id"));

            JsonObject bodyParam = rc.getBodyAsJson();

            String rule_query = StringUtils.defaultString(bodyParam.getString("rule_query"));
            String send_type = StringUtils.defaultString(bodyParam.getString("send_type"));
            String send_info = bodyParam.getJsonObject("send_info").encode();
            String send_content = StringUtils.defaultString(bodyParam.getString("send_content"));
            String send_method = StringUtils.defaultString(bodyParam.getString("send_method"));

            if (rule_id.equals("")) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter: rule_id is Required");
            }

            String updateSql = "UPDATE Rule SET rule_query = ?, " +
                               "send_type = ?, " +
                               "send_info = ?, " +
                               "send_content = ?, " +
                               "send_method = ? " +
                               "where rule_id = ?";

            JsonArray updateArgs = new JsonArray();
            updateArgs.add(rule_query).add(send_type).add(send_info).add(send_content).add(send_method).add(rule_id);

            JsonObject updateCache = new JsonObject();
            updateCache.put("cache", "rule");
            updateCache.put("sql", updateSql);
            updateCache.put("args", updateArgs);

            vertx.eventBus().send("cache.update", updateCache, result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    this.responseSuccess(rc, 200);
                }
            });

        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getRuleList(RoutingContext rc) {
        try {
            String sql = "SELECT rule_id, rule_name, rule_descp, rule_query, callback_name, insert_date FROM Rule ORDER BY rule_id ASC";
            JsonObject cache = new JsonObject();
            cache.put("cache", "rule");
            cache.put("sql", sql);
            cache.put("args", new JsonArray());
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray ruleList = (JsonArray) result.result().body();
                    JsonArray resultList = new JsonArray();
                    for (int i = 0; i < ruleList.size(); i++) {
                        JsonArray line = ruleList.getJsonArray(i);
                        JsonObject rule = new JsonObject();
                        rule.put("rule_id", line.getLong(0));
                        rule.put("rule_name", line.getString(1));
                        rule.put("rule_descp", line.getString(2));
                        rule.put("rule_query", line.getString(3));
                        rule.put("callback_name", line.getString(4));
                        rule.put("insert_date", line.getString(5));

                        resultList.add(rule);
                    }

                    this.responseSuccess(rc, 200, resultList.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getRuleHistList(RoutingContext rc) {
        try {
            vertx.eventBus().send("sql.rule.history.select", new JsonObject() ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray ruleHistList = (JsonArray) result.result().body();
                    logger.debug(">> ruleHistList size : " +ruleHistList.size());

                    JsonArray resultList = new JsonArray();
                    for (int i = 0; i < ruleHistList.size(); i++) {
                        JsonArray line = ruleHistList.getJsonArray(i);

                        logger.debug(">>> line : " + line.encode());

                        JsonObject rule = new JsonObject();
                        rule.put("hist_id", line.getLong(0));
                        rule.put("rule_id", line.getLong(1));
                        rule.put("rule_name", line.getString(2));
                        rule.put("rule_descp", line.getString(3));
                        rule.put("send_content", line.getString(4));
                        rule.put("insert_date", line.getString(5));

                        resultList.add(rule);

                        logger.debug("weljkwrejklwrejkl " + rule.encode());
                    }

                    logger.debug(">>>> hist resultList.encode : " + resultList.encode());

                    this.responseSuccess(rc, 200, resultList.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getRuleDetail(RoutingContext rc) {
        try {
            String rule_id = StringUtils.defaultString(rc.request().getParam("rule_id"));
            if ("".equals(rule_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : rule_id is Required.");
            }

            String sql = "SELECT rule_id, rule_name, rule_descp, rule_query, send_type, send_info, send_content, " +
                    " send_method, last_send_time FROM Rule WHERE rule_id = ?";

            JsonObject cache = new JsonObject();
            cache.put("cache", "rule");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(rule_id));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray ruleList = (JsonArray) result.result().body();
                    JsonObject resultDetail = new JsonObject();
                    JsonArray line = ruleList.getJsonArray(0);

                    resultDetail.put("rule_id", line.getLong(0));
                    resultDetail.put("rule_name", line.getString(1));
                    resultDetail.put("rule_descp", line.getString(2));
                    resultDetail.put("rule_query", line.getString(3));
                    resultDetail.put("send_type", line.getString(4));
                    resultDetail.put("send_info", line.getString(5));
                    resultDetail.put("send_content", line.getString(6));
                    resultDetail.put("send_method", line.getString(7));
                    resultDetail.put("last_send_time", line.getString(8));

                    this.responseSuccess(rc, 200, resultDetail.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void getRuleListByEvent(RoutingContext rc) {
        try {
            String event_id = StringUtils.defaultString(rc.request().getParam("event_id"));
            if ("".equals(event_id)) {
                throw new NCEPException(ErrorCode.MANDATORY_PARAMETER_ERROR, "Parameter : event_id is Required.");
            }

            String sql = "SELECT a.rule_id, a.rule_name, a.rule_query, a.send_type, a.insert_date FROM Rule a, " +
                    " \"rule_input_event\".RuleInputEvent as b WHERE a.rule_id = b.rule_id and b.event_id = ? ";
            JsonObject cache = new JsonObject();
            cache.put("cache", "rule");
            cache.put("sql", sql);
            cache.put("args", new JsonArray().add(event_id));
            vertx.eventBus().send("cache.select", cache ,result -> {
                if (result.failed()) {
                    this.responseFail(rc, new NCEPException(ErrorCode.DATA_PROCESSING_ERROR, ErrorCode.DATA_PROCESSING_ERROR_MSG));
                } else {
                    JsonArray eventList = (JsonArray) result.result().body();
                    JsonArray resultList = new JsonArray();
                    for (int i = 0; i < eventList.size(); i++) {
                        JsonArray line = eventList.getJsonArray(i);
                        JsonObject event = new JsonObject();
                        event.put("rule_id", line.getLong(0));
                        event.put("rule_name", line.getString(1));
                        event.put("rule_query", line.getString(2));
                        event.put("send_type", line.getString(3));
                        event.put("insert_date", line.getString(4));

                        resultList.add(event);
                    }

                    this.responseSuccess(rc, 200, resultList.encode());
                }
            });
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

    public void validateRule(RoutingContext rc) {
        try {
            JsonObject param = rc.getBodyAsJson();
            checkMandatoryValue(param, "event_query", "rule_query");
            String eventQuery = StringUtils.defaultString(param.getString("event_query"));
            String ruleQuery = StringUtils.defaultString(param.getString("rule_query"));

            RuleService.validateRule(eventQuery + "\r\n" + ruleQuery);
            this.responseSuccess(rc, 200);
        } catch (NCEPException ne) {
            logger.error("Error : ", ne);
            this.responseFail(rc, ne);
        } catch (Exception e) {
            logger.error("Error : ", e);
            this.responseFail(rc, "(" + ErrorCode.SERVER_ERROR + ") " + ErrorCode.SERVER_ERROR_MSG);
        }
    }

}
