package com.ntels.engine.repository;

import com.ntels.engine.repository.entity.InputEvent;
import com.ntels.engine.repository.entity.Rule;
import com.ntels.engine.repository.entity.RuleInputEvent;
import com.ntels.engine.repository.store.CacheStoreSessionListenerFactory;
import com.ntels.engine.repository.store.InputEventCacheStore;
import com.ntels.engine.repository.store.RuleCacheStore;
import com.ntels.engine.repository.store.RuleInputEventCacheStore;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.transactions.Transaction;
import org.apache.log4j.Logger;

import javax.cache.configuration.FactoryBuilder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Cache & DB 처리 Verticle
 */
public class CacheServiceVerticle extends AbstractVerticle {

    private Logger logger = Logger.getLogger(getClass());
    private Ignite ignite;
    private static IgniteAtomicLong max_rule_id;

    public static Long getMaxRuleId() {
        return max_rule_id.incrementAndGet();
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("RuleCacheService Start");
        }

        initRuleCache();
        initRuleInputEventCache();
        initInputEventCache();

        vertx.eventBus().consumer("cache.rule.insert", insertRule());
        vertx.eventBus().consumer("cache.rule.delete", deleteRule());
        vertx.eventBus().consumer("cache.input.event.insert", insertEvent());
        vertx.eventBus().consumer("cache.input.event.delete", deleteEvent());
        vertx.eventBus().consumer("cache.select", selectCache());
        vertx.eventBus().consumer("cache.update", updateCache());

        vertx.eventBus().consumer("sql.rule.history.select", selectRuleHistory());

        if (logger.isDebugEnabled()) {
            logger.debug("RuleCacheService End");
        }

        startFuture.complete();
    }

    private void initRuleCache() {
        if (logger.isDebugEnabled()) {
            logger.debug("Rule initCache Start");
        }

        String cacheNodeId = config().getString("cacheNodeId");
        ignite = Ignition.ignite("vertx.ignite.node." + cacheNodeId);

        CacheConfiguration<Long, Rule> cacheCfg = new CacheConfiguration<>("rule");
        cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cacheCfg.setCacheStoreFactory(FactoryBuilder.factoryOf(RuleCacheStore.class));
        cacheCfg.setCacheStoreSessionListenerFactories(new CacheStoreSessionListenerFactory(config().getJsonObject("ncep.db")));
        cacheCfg.setIndexedTypes(Long.class, Rule.class);
        cacheCfg.setReadThrough(true);
        cacheCfg.setWriteThrough(true);

        IgniteCache<Long, Rule> ruleCache = ignite.getOrCreateCache(cacheCfg);
        if (ruleCache.size() == 0) {
            ruleCache.loadCache(null, vertx);
        }

        List<?> list = ruleCache.query(new SqlFieldsQuery("SELECT IFNULL(MAX(rule_id), 0) FROM Rule")).getAll();
        List line = (List) list.get(0);
        Long rule_id = (Long) line.get(0);
        max_rule_id = ignite.atomicLong("rule_id", rule_id, true);

        if (logger.isDebugEnabled()) {
            logger.debug("rule initCache End(max_rule_id : " + rule_id + ")");
        }
    }

    private void initRuleInputEventCache() {
        if (logger.isDebugEnabled()) {
            logger.debug("RuleInputEvent initCache Start");
        }

        String cacheNodeId = config().getString("cacheNodeId");
        ignite = Ignition.ignite("vertx.ignite.node." + cacheNodeId);

        CacheConfiguration<Long, RuleInputEvent> cacheCfg = new CacheConfiguration<>("rule_input_event");
        cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cacheCfg.setCacheStoreFactory(FactoryBuilder.factoryOf(RuleInputEventCacheStore.class));
        cacheCfg.setCacheStoreSessionListenerFactories(new CacheStoreSessionListenerFactory(config().getJsonObject("ncep.db")));
        cacheCfg.setIndexedTypes(Long.class, RuleInputEvent.class);
        cacheCfg.setReadThrough(true);
        cacheCfg.setWriteThrough(true);

        IgniteCache<Long, RuleInputEvent> ruleInputEventCache = ignite.getOrCreateCache(cacheCfg);
        if (ruleInputEventCache.size() == 0) {
            ruleInputEventCache.loadCache(null);
        }

        List<?> list = ruleInputEventCache.query(new SqlFieldsQuery("SELECT IFNULL(MAX(rule_input_event_id), 0) FROM RuleInputEvent")).getAll();
        List line = (List) list.get(0);
        Long rule_input_event_id = (Long) line.get(0);
        ignite.atomicLong("rule_input_event_id", rule_input_event_id, true);

        if (logger.isDebugEnabled()) {
            logger.debug("RuleInputEvent initCache End(max_rule_input_event_id : " + rule_input_event_id + ")");
        }
    }

    private void initInputEventCache() {
        if (logger.isDebugEnabled()) {
            logger.debug("InputEvent initCache Start");
        }

        String cacheNodeId = config().getString("cacheNodeId");
        ignite = Ignition.ignite("vertx.ignite.node." + cacheNodeId);

        CacheConfiguration<Long, InputEvent> cacheCfg = new CacheConfiguration<>("input_event");
        cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cacheCfg.setCacheStoreFactory(FactoryBuilder.factoryOf(InputEventCacheStore.class));
        cacheCfg.setCacheStoreSessionListenerFactories(new CacheStoreSessionListenerFactory(config().getJsonObject("ncep.db")));
        cacheCfg.setIndexedTypes(Long.class, InputEvent.class);
        cacheCfg.setReadThrough(true);
        cacheCfg.setWriteThrough(true);

        IgniteCache<Long, InputEvent> inputEventCache = ignite.getOrCreateCache(cacheCfg);
        if (inputEventCache.size() == 0) {
            inputEventCache.loadCache(null);
        }

        List<?> list = inputEventCache.query(new SqlFieldsQuery("SELECT IFNULL(MAX(event_id), 0) FROM InputEvent")).getAll();
        List line = (List) list.get(0);
        Long event_id = (Long) line.get(0);
        ignite.atomicLong("event_id", event_id, true);

        if (logger.isDebugEnabled()) {
            logger.debug("InputEvent initCache End(max_event_id : " + event_id + ")");
        }
    }

    private Handler<Message<JsonObject>> insertRule() {
        return message -> vertx.executeBlocking(future -> {
            try(Transaction tx = ignite.transactions().txStart()) {
                JsonObject param = message.body();
                JsonArray event_info = param.getJsonArray("event_info");

                // rule 캐시 & rule 테이블 등록
                Rule rule = new Rule();
                rule.setRule_id(param.getLong("rule_id"));
                rule.setRule_name(StringUtils.defaultString(param.getString("rule_name")));
                rule.setRule_descp(StringUtils.defaultString(param.getString("rule_descp")));
                rule.setRule_query(StringUtils.defaultString(param.getString("rule_query")));
                rule.setCallback_name(StringUtils.defaultString(param.getString("callback_name")));
                rule.setSend_type(StringUtils.defaultString(param.getString("send_type")));
                rule.setSend_info(param.getJsonObject("send_info").encode());
                rule.setSend_content(StringUtils.defaultString(param.getString("send_content")));
                rule.setSend_method(StringUtils.defaultString(param.getString("send_method")));

                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                rule.setInsert_date(sdf.format(today));
                rule.setUpdate_date(sdf.format(today));

                ignite.cache("rule").put(rule.getRule_id(), rule);

                // rule_input_event 캐시 & rule_input_event 테이블 등록
                if (event_info != null) {
                    for (int i = 0; i < event_info.size(); i++) {
                        JsonObject event = event_info.getJsonObject(i);
                        Long event_id = Long.parseLong(event.getString("event_id"));
                        long max_rule_input_event_id = ignite.atomicLong("rule_input_event_id", 0, false).incrementAndGet();

                        RuleInputEvent ruleInputEvent = new RuleInputEvent();
                        ruleInputEvent.setRule_input_event_id(max_rule_input_event_id);
                        ruleInputEvent.setRule_id(param.getLong("rule_id"));
                        ruleInputEvent.setEvent_id(event_id);

                        ignite.cache("rule_input_event").put(ruleInputEvent.getRule_input_event_id(), ruleInputEvent);
                    }
                }

                tx.commit();
                future.complete();

            } catch (Exception e) {
                logger.error("Error : ", e);
                future.fail(e);
            }
        }, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(100, result.cause().toString());
            }
        });
    }

    private Handler<Message<JsonObject>> deleteRule() {
        return message -> vertx.executeBlocking(future -> {
            try(Transaction tx = ignite.transactions().txStart()) {
                JsonObject param = message.body();
                Long rule_id = param.getLong("rule_id");

                String sql = "SELECT rule_input_event_id FROM RuleInputEvent WHERE rule_id = ?";
                List<?> list = ignite.cache("rule_input_event").query(new SqlFieldsQuery(sql).setArgs(rule_id)).getAll();
                if (list.size() == 0) {
                    future.fail(new Exception("The rule_id does not exist"));
                }
                for (int i = 0; i < list.size(); i++) {
                    List tmp = (List) list.get(i);
                    Long rule_input_event_id = (Long) tmp.get(0);

                    ignite.cache("rule_input_event").remove(rule_input_event_id);
                }

                boolean result = ignite.cache("rule").remove(rule_id);

                tx.commit();
                future.complete(result);

            } catch (Exception e) {
                logger.error("Error : ", e);
                future.fail(e);
            }
        }, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(100, result.cause().toString());
            }
        });
    }

    private Handler<Message<JsonObject>> insertEvent() {
        return message -> vertx.executeBlocking(future -> {
            try(Transaction tx = ignite.transactions().txStart()) {
                JsonObject param = message.body();
                long event_id = ignite.atomicLong("event_id", 0, false).incrementAndGet();

                // input_event 캐시 & input_event 테이블 등록
                InputEvent inputEvent = new InputEvent();
                inputEvent.setEvent_id(event_id);
                inputEvent.setEvent_name(StringUtils.defaultString(param.getString("event_name")));
                inputEvent.setEvent_descp(StringUtils.defaultString(param.getString("event_descp")));
                inputEvent.setEvent_type(StringUtils.defaultString(param.getString("event_type")));
                inputEvent.setInit_value_yn(StringUtils.defaultString(param.getString("init_value_yn")));

                if (param.getJsonArray("event_name_order") != null) {
                    inputEvent.setEvent_name_order(param.getJsonArray("event_name_order").encode());
                }

                if (param.getJsonArray("init_value") != null) {
                    inputEvent.setInit_value(param.getJsonArray("init_value").encode());
                }

                inputEvent.setEvent_query(StringUtils.defaultString(param.getString("event_query")));

                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                inputEvent.setInsert_date(sdf.format(today));
                inputEvent.setUpdate_date(sdf.format(today));

                ignite.cache("input_event").put(inputEvent.getEvent_id(), inputEvent);

                tx.commit();
                future.complete(event_id);

            } catch (Exception e) {
                logger.error("Error : ", e);
                future.fail(e);
            }
        }, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(100, result.cause().toString());
            }
        });
    }

    private Handler<Message<JsonObject>> deleteEvent() {
        return message -> vertx.executeBlocking(future -> {
            try(Transaction tx = ignite.transactions().txStart()) {
                JsonObject param = message.body();

                boolean result = ignite.cache("input_event").remove(param.getLong("event_id"));

                tx.commit();
                future.complete(result);

            } catch (Exception e) {
                logger.error("Error : ", e);
                future.fail(e);
            }
        }, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(100, result.cause().toString());
            }
        });
    }

    private Handler<Message<JsonObject>> selectCache() {
        return message -> {
            try {
                JsonObject param = message.body();

                String cache = param.getString("cache");
                String sql = param.getString("sql");
                JsonArray args = param.getJsonArray("args");

                Object objArgs[] = new Object[args.size()];
                for (int i = 0; i < args.size(); i++) {
                    objArgs[i] = args.getValue(i);
                }

                List list = ignite.cache(cache).query(new SqlFieldsQuery(sql).setArgs(objArgs)).getAll();
                message.reply(new JsonArray(list));

            } catch (Exception e) {
                logger.error("Error : ", e);
                message.fail(100, e.toString());
            }
        };
    }

    private Handler<Message<JsonObject>> updateCache() {
        return message -> vertx.executeBlocking(future -> {
            try {
                JsonObject param = message.body();

                String cache = param.getString("cache");
                String sql = param.getString("sql");
                JsonArray args = param.getJsonArray("args");

                Object objArgs[] = new Object[args.size()];
                for (int i = 0; i < args.size(); i++) {
                    objArgs[i] = args.getValue(i);
                }

                ignite.cache(cache).query(new SqlFieldsQuery(sql).setArgs(objArgs));
                message.reply("Success");

                future.complete();

            } catch (Exception e) {
                logger.error("Error : ", e);
                future.fail(e);
            }
        }, result -> {
            if (result.succeeded()) {
                message.reply(result.result());
            } else {
                message.fail(100, result.cause().toString());
            }
        });
    }

    private Handler<Message<JsonObject>> selectRuleHistory() {
        return message ->  {
            try {
                JsonArray returnArr = new JsonArray();
                logger.debug(">>> selectRuleHistory ");

                JsonObject config = config().getJsonObject("ncep.db");
                String sql = "SELECT hist_id, rule_id, rule_name, rule_descp, send_content, insert_date, update_date " +
                        " FROM rule_history ORDER BY hist_id desc";

                try (Connection conn = DriverManager.getConnection(config.getString("url"), config.getString("user"), config.getString("password"))) {
                    try (PreparedStatement st = conn.prepareStatement(sql)) {

                        ResultSet rs = st.executeQuery();
                        while (rs.next()) {
                            JsonArray arr = new JsonArray();
                            arr.add(rs.getLong(1));
                            arr.add(rs.getLong(2));
                            arr.add(rs.getString(3));
                            arr.add(rs.getString(4));
                            arr.add(rs.getString(5));
                            arr.add(rs.getString(6));
                            arr.add(rs.getString(7));

                            returnArr.add(arr);
                        }

                    }
                } catch (Exception e) {
                    logger.error("Error : ", e);
                } finally {
                    message.reply(returnArr);
                }

            } catch (Exception e) {
                logger.error("Error : ", e);
            }
        };
    }

}
