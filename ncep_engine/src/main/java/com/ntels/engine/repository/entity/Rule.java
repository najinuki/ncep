package com.ntels.engine.repository.entity;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Apache Ignite Entity Class (Rule)
 */
public class Rule implements Serializable {

    @QuerySqlField(index = true)
    private Long rule_id; // Rule ID

    @QuerySqlField
    private String rule_name; // Rule 명

    @QuerySqlField
    private String rule_descp; // Rule 설명

    @QuerySqlField
    private String rule_query; // Rule Define Query

    @QuerySqlField
    private String callback_name; // Callback Name

    @QuerySqlField
    private String send_type; // 전송 타입

    @QuerySqlField
    private String send_info; // 전송 정보

    @QuerySqlField
    private String send_content; // 전송 내용

    @QuerySqlField
    private String send_method; // 전송 방식

    @QuerySqlField
    private String last_send_time; // 최종 전송 시간

    @QuerySqlField
    private String insert_date; // 생성 일자

    @QuerySqlField
    private String update_date; // 수정 일자

    public Long getRule_id() {
        return rule_id;
    }

    public void setRule_id(Long rule_id) {
        this.rule_id = rule_id;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getRule_descp() {
        return rule_descp;
    }

    public void setRule_descp(String rule_descp) {
        this.rule_descp = rule_descp;
    }

    public String getRule_query() {
        return rule_query;
    }

    public void setRule_query(String rule_query) {
        this.rule_query = rule_query;
    }

    public String getCallback_name() {
        return callback_name;
    }

    public void setCallback_name(String callback_name) {
        this.callback_name = callback_name;
    }

    public String getSend_type() {
        return send_type;
    }

    public void setSend_type(String send_type) {
        this.send_type = send_type;
    }

    public String getSend_info() {
        return send_info;
    }

    public void setSend_info(String send_info) {
        this.send_info = send_info;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }

    public String getSend_method() {
        return send_method;
    }

    public void setSend_method(String send_method) {
        this.send_method = send_method;
    }

    public String getLast_send_time() {
        return last_send_time;
    }

    public void setLast_send_time(String last_send_time) {
        this.last_send_time = last_send_time;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "rule_id=" + rule_id +
                ", rule_name='" + rule_name + '\'' +
                ", rule_descp='" + rule_descp + '\'' +
                ", rule_query='" + rule_query + '\'' +
                ", callback_name='" + callback_name + '\'' +
                ", send_type='" + send_type + '\'' +
                ", send_info='" + send_info + '\'' +
                ", send_content='" + send_content + '\'' +
                ", send_method='" + send_method + '\'' +
                ", last_send_time='" + last_send_time + '\'' +
                ", insert_date='" + insert_date + '\'' +
                ", update_date='" + update_date + '\'' +
                '}';
    }

}
