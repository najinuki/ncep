package com.ntels.engine.repository.store;

import com.ntels.engine.repository.entity.RuleInputEvent;
import org.apache.ignite.cache.store.CacheStoreAdapter;
import org.apache.ignite.cache.store.CacheStoreSession;
import org.apache.ignite.lang.IgniteBiInClosure;
import org.apache.ignite.resources.CacheStoreSessionResource;
import org.apache.log4j.Logger;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Apache Ignite Persistent Store Class(Rule Input)
 */
public class RuleInputEventCacheStore extends CacheStoreAdapter<Long, RuleInputEvent> {

    private Logger logger = Logger.getLogger(this.getClass());

    /** Store session. */
    @CacheStoreSessionResource
    private CacheStoreSession ses;

    @Override public RuleInputEvent load(Long key) {
        Connection conn = ses.attachment();
        String sql = " SELECT rule_input_event_id, rule_id, event_id FROM rule_input_event WHERE rule_input_event_id = ? ";

        try (PreparedStatement st = conn.prepareStatement(sql)) {
            st.setLong(1, key);

            ResultSet rs = st.executeQuery();
            RuleInputEvent ruleInputEvent = new RuleInputEvent();

            if (rs.next()) {
                ruleInputEvent.setRule_input_event_id(rs.getLong(1));
                ruleInputEvent.setRule_id(rs.getLong(2));
                ruleInputEvent.setEvent_id(rs.getLong(3));
            }

            return ruleInputEvent;
        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load object [key=" + key + ']', e);
        }

    }

    @Override public void write(Cache.Entry<? extends Long, ? extends RuleInputEvent> entry) {
        Long key = entry.getKey();
        RuleInputEvent val = entry.getValue();

        try {
            Connection conn = ses.attachment();
            String sql = " INSERT INTO rule_input_event (rule_input_event_id, rule_id, event_id) VALUES (?, ?, ?) ";

            try (PreparedStatement st = conn.prepareStatement(sql)) {
                int idx = 1;

                st.setLong(idx++, key);
                st.setLong(idx++, val.getRule_id());
                st.setLong(idx++, val.getEvent_id());

                st.executeUpdate();
            }
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to write object [key=" + key + ", val=" + val + ']', e);
        }
    }

    @Override public void delete(Object key) {
        Connection conn = ses.attachment();

        try (PreparedStatement st = conn.prepareStatement("DELETE FROM rule_input_event WHERE rule_input_event_id = ?")) {
            st.setLong(1, (Long)key);
            st.executeUpdate();
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to delete object [key=" + key + ']', e);
        }
    }

    @Override public void loadCache(IgniteBiInClosure<Long, RuleInputEvent> clo, Object... args) {
        Connection conn = ses.attachment();
        String sql = " SELECT rule_input_event_id, rule_id, event_id FROM rule_input_event ";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RuleInputEvent ruleInputEvent = new RuleInputEvent();
                ruleInputEvent.setRule_input_event_id(rs.getLong(1));
                ruleInputEvent.setRule_id(rs.getLong(2));
                ruleInputEvent.setEvent_id(rs.getLong(3));

                clo.apply(ruleInputEvent.getRule_input_event_id(), ruleInputEvent);
            }

        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load values from cache store.", e);
        }
    }

}
