package com.ntels.engine.repository.entity;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Apache Ignite Entity Class (Rule InputEvent)
 */
public class RuleInputEvent implements Serializable {

    @QuerySqlField(index = true)
    private Long rule_input_event_id; // Rule InputEvent ID

    @QuerySqlField(index = true)
    private Long rule_id; // Rule ID

    @QuerySqlField(index = true)
    private Long event_id; // Event ID

    public Long getRule_input_event_id() {
        return rule_input_event_id;
    }

    public void setRule_input_event_id(Long rule_input_event_id) {
        this.rule_input_event_id = rule_input_event_id;
    }

    public Long getRule_id() {
        return rule_id;
    }

    public void setRule_id(Long rule_id) {
        this.rule_id = rule_id;
    }

    public Long getEvent_id() {
        return event_id;
    }

    public void setEvent_id(Long event_id) {
        this.event_id = event_id;
    }

    @Override
    public String toString() {
        return "RuleInputEvent{" +
                "rule_input_event_id=" + rule_input_event_id +
                ", rule_id=" + rule_id +
                ", event_id=" + event_id +
                '}';
    }

}
