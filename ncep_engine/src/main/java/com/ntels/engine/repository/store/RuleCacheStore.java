package com.ntels.engine.repository.store;

import com.ntels.engine.repository.entity.Rule;
import com.ntels.engine.rule.RuleService;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.ignite.cache.store.CacheStoreAdapter;
import org.apache.ignite.cache.store.CacheStoreSession;
import org.apache.ignite.lang.IgniteBiInClosure;
import org.apache.ignite.resources.CacheStoreSessionResource;
import org.apache.log4j.Logger;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Apache Ignite Persistent Store Class(Rule)
 */
public class RuleCacheStore extends CacheStoreAdapter<Long, Rule> {

    private Logger logger = Logger.getLogger(this.getClass());

    /** Store session. */
    @CacheStoreSessionResource
    private CacheStoreSession ses;

    @Override public Rule load(Long key) {
        Connection conn = ses.attachment();
        String sql = " SELECT rule_id, rule_name, rule_descp, rule_query, callback_name, " +
                " send_type, send_info, send_content, send_method, DATE_FORMAT(last_send_time, '%Y-%m-%d %H:%i:%s'), " +
                " DATE_FORMAT(insert_date, '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(update_date, '%Y-%m-%d %H:%i:%s') " +
                " FROM rule WHERE rule_id = ? ";

        try (PreparedStatement st = conn.prepareStatement(sql)) {
            st.setLong(1, key);

            ResultSet rs = st.executeQuery();
            Rule rule = new Rule();

            if (rs.next()) {
                rule.setRule_id(rs.getLong(1));
                rule.setRule_name(rs.getString(2));
                rule.setRule_descp(rs.getString(3));
                rule.setRule_query(rs.getString(4));
                rule.setCallback_name(rs.getString(5));
                rule.setSend_type(rs.getString(6));
                rule.setSend_info(rs.getString(7));
                rule.setSend_content(rs.getString(8));
                rule.setSend_method(rs.getString(9));
                rule.setLast_send_time(rs.getString(10));
                rule.setInsert_date(rs.getString(11));
                rule.setUpdate_date(rs.getString(12));
            }

            return rule;
        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load object [key=" + key + ']', e);
        }

    }

    @Override public void write(Cache.Entry<? extends Long, ? extends Rule> entry) {
        Long key = entry.getKey();
        Rule val = entry.getValue();

        try {
            Connection conn = ses.attachment();
            String sql = " INSERT INTO rule (rule_id, rule_name, rule_descp, rule_query, callback_name, " +
                    " send_type, send_info, send_content, send_method, last_send_time, insert_date, update_date) " +
                    " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE update_date = ? ";

            if (val.getRule_name() != null) {
                sql += ", rule_name = ? ";
            }

            if (val.getRule_descp() != null) {
                sql += ", rule_descp = ? ";
            }

            if (val.getRule_query() != null) {
                sql += ", rule_query = ? ";
            }

            if (val.getCallback_name() != null) {
                sql += ", callback_name = ? ";
            }

            if (val.getSend_type() != null) {
                sql += ", send_type = ? ";
            }

            if (val.getSend_info() != null) {
                sql += ", send_info = ? ";
            }

            if (val.getSend_content() != null) {
                sql += ", send_content = ? ";
            }

            if (val.getSend_method() != null) {
                sql += ", send_method = ? ";
            }

            if (val.getLast_send_time() != null) {
                sql += ", last_send_time = ? ";
            }

            try (PreparedStatement st = conn.prepareStatement(sql)) {
                int idx = 1;

                st.setLong(idx++, key);
                st.setString(idx++, val.getRule_name());
                st.setString(idx++, val.getRule_descp());
                st.setString(idx++, val.getRule_query());
                st.setString(idx++, val.getCallback_name());
                st.setString(idx++, val.getSend_type());
                st.setString(idx++, val.getSend_info());
                st.setString(idx++, val.getSend_content());
                st.setString(idx++, val.getSend_method());
                st.setString(idx++, val.getLast_send_time());
                st.setString(idx++, val.getInsert_date());
                st.setString(idx++, val.getUpdate_date());

                st.setString(idx++, val.getUpdate_date());

                if (val.getRule_name() != null) {
                    st.setString(idx++, val.getRule_name());
                }

                if (val.getRule_descp() != null) {
                    st.setString(idx++, val.getRule_descp());
                }

                if (val.getRule_query() != null) {
                    st.setString(idx++, val.getRule_query());
                }

                if (val.getCallback_name() != null) {
                    st.setString(idx++, val.getCallback_name());
                }

                if (val.getSend_type() != null) {
                    st.setString(idx++, val.getSend_type());
                }

                if (val.getSend_info() != null) {
                    st.setString(idx++, val.getSend_info());
                }

                if (val.getSend_content() != null) {
                    st.setString(idx++, val.getSend_content());
                }

                if (val.getSend_method() != null) {
                    st.setString(idx++, val.getSend_method());
                }

                if (val.getLast_send_time() != null) {
                    st.setString(idx++, val.getLast_send_time());
                }

                st.executeUpdate();
            }
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to write object [key=" + key + ", val=" + val + ']', e);
        }
    }

    @Override public void delete(Object key) {
        Connection conn = ses.attachment();
        try (PreparedStatement st = conn.prepareStatement("DELETE FROM rule WHERE rule_id = ?")) {
            st.setLong(1, (Long)key);
            st.executeUpdate();
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to delete object [key=" + key + ']', e);
        }
    }

    @Override public void loadCache(IgniteBiInClosure<Long, Rule> clo, Object... args) {
        Connection conn = ses.attachment();
        String sql = " SELECT rule_id, rule_name, rule_descp, rule_query, callback_name, " +
                " send_type, send_info, send_content, send_method, DATE_FORMAT(last_send_time, '%Y-%m-%d %H:%i:%s'), " +
                " DATE_FORMAT(insert_date, '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(update_date, '%Y-%m-%d %H:%i:%s') FROM rule ";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Rule rule = new Rule();
                rule.setRule_id(rs.getLong(1));
                rule.setRule_name(rs.getString(2));
                rule.setRule_descp(rs.getString(3));
                rule.setRule_query(rs.getString(4));
                rule.setCallback_name(rs.getString(5));
                rule.setSend_type(rs.getString(6));
                rule.setSend_info(rs.getString(7));
                rule.setSend_content(rs.getString(8));
                rule.setSend_method(rs.getString(9));
                rule.setLast_send_time(rs.getString(10));
                rule.setInsert_date(rs.getString(11));
                rule.setUpdate_date(rs.getString(12));

                clo.apply(rule.getRule_id(), rule);

                Vertx vertx = (Vertx) args[0];
                RuleService.createRule(vertx, rule.getRule_query(), rule.getCallback_name());

                initValueCheck(conn, rs.getLong(1));
            }
        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load values from cache store.", e);
        } catch (Exception e) {
            throw new CacheLoaderException("Failed to WSO2 Siddhi create rule.", e);
        }
    }

    private void initValueCheck(Connection conn, Long rule_id) {
        String sql = "SELECT a.event_name, a.event_name_order, a.init_value FROM input_event a, rule_input_event b " +
                " WHERE a.event_id = b.event_id AND a.init_value_yn = 'Y' AND b.rule_id = ? ";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, rule_id);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                String eventName = rs.getString(1);
                String eventNameOrder = rs.getString(2);
                String strInitValue = rs.getString(3);

                JsonArray arrInitValue = new JsonArray(strInitValue);
                JsonArray arrEventNameOrder = new JsonArray(eventNameOrder);

                for (int i = 0; i < arrInitValue.size(); i++) {
                    JsonObject initValue = arrInitValue.getJsonObject(i);
                    String insertSql = "select ";
                    for (int j = 0; j < arrEventNameOrder.size(); j++) {
                        String key = arrEventNameOrder.getString(j);

                        if (initValue.getValue(key) instanceof String) {
                            if (j == arrEventNameOrder.size() - 1) {
                                insertSql += "'" + initValue.getValue(key) + "' as " + key + " insert into " + eventName + ";";
                            } else {
                                insertSql += "'" + initValue.getValue(key) + "' as " + key + ", ";
                            }
                        } else {
                            if (j == arrEventNameOrder.size() - 1) {
                                insertSql += initValue.getValue(key) + " as " + key + " insert into " + eventName + ";";
                            } else {
                                insertSql += initValue.getValue(key) + " as " + key + ", ";
                            }
                        }
                    }

                    logger.debug("insertSql : " + insertSql);
                    RuleService.insertTableData("rule_" + rule_id, insertSql);
                }
            }
        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load values from cache store.", e);
        } catch (Exception e) {
            throw new CacheLoaderException("Failed to WSO2 Siddhi create rule.", e);
        }

    }


}
