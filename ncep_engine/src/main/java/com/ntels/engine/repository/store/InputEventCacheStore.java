package com.ntels.engine.repository.store;

import com.ntels.engine.repository.entity.InputEvent;
import org.apache.ignite.cache.store.CacheStoreAdapter;
import org.apache.ignite.cache.store.CacheStoreSession;
import org.apache.ignite.lang.IgniteBiInClosure;
import org.apache.ignite.resources.CacheStoreSessionResource;
import org.apache.log4j.Logger;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Apache Ignite Persistent Store Class(Input Event)
 */
public class InputEventCacheStore extends CacheStoreAdapter<Long, InputEvent> {

    private Logger logger = Logger.getLogger(this.getClass());

    /** Store session. */
    @CacheStoreSessionResource
    private CacheStoreSession ses;

    @Override public InputEvent load(Long key) {
        Connection conn = ses.attachment();
        String sql = " SELECT event_id, event_name, event_name_order, event_descp, event_type, init_value_yn, " +
                " init_value, event_query, DATE_FORMAT(insert_date, '%Y-%m-%d %H:%i:%s'), " +
                " DATE_FORMAT(update_date, '%Y-%m-%d %H:%i:%s') FROM input_event WHERE event_id = ? ";

        try (PreparedStatement st = conn.prepareStatement(sql)) {
            st.setLong(1, key);

            ResultSet rs = st.executeQuery();
            InputEvent inputEvent = new InputEvent();

            if (rs.next()) {
                inputEvent.setEvent_id(rs.getLong(1));
                inputEvent.setEvent_name(rs.getString(2));
                inputEvent.setEvent_name_order(rs.getString(3));
                inputEvent.setEvent_descp(rs.getString(4));
                inputEvent.setEvent_type(rs.getString(5));
                inputEvent.setInit_value_yn(rs.getString(6));
                inputEvent.setInit_value(rs.getString(7));
                inputEvent.setEvent_query(rs.getString(8));
                inputEvent.setInsert_date(rs.getString(9));
                inputEvent.setUpdate_date(rs.getString(10));
            }

            return inputEvent;
        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load object [key=" + key + ']', e);
        }

    }

    @Override public void write(Cache.Entry<? extends Long, ? extends InputEvent> entry) {
        Long key = entry.getKey();
        InputEvent val = entry.getValue();

        try {
            Connection conn = ses.attachment();
            String sql = " INSERT INTO input_event (event_id, event_name, event_name_order, event_descp, event_type, " +
                    " init_value_yn, init_value, event_query, insert_date, update_date) VALUES (?, ?, ?, ?, ?, ?, " +
                    " ?, ?, ?, ?) ON DUPLICATE KEY UPDATE update_date = ? ";

            if (val.getEvent_name() != null) {
                sql += ", event_name = ? ";
            }

            if (val.getEvent_name_order() != null) {
                sql += ", event_name_order = ? ";
            }

            if (val.getEvent_descp() != null) {
                sql += ", event_descp = ? ";
            }

            if (val.getEvent_type() != null) {
                sql += ", event_type = ? ";
            }

            if (val.getInit_value_yn() != null) {
                sql += ", init_value_yn = ? ";
            }

            if (val.getInit_value() != null) {
                sql += ", init_value = ? ";
            }

            if (val.getEvent_query() != null) {
                sql += ", event_query = ? ";
            }

            try (PreparedStatement st = conn.prepareStatement(sql)) {
                int idx = 1;

                st.setLong(idx++, key);
                st.setString(idx++, val.getEvent_name());
                st.setString(idx++, val.getEvent_name_order());
                st.setString(idx++, val.getEvent_descp());
                st.setString(idx++, val.getEvent_type());
                st.setString(idx++, val.getInit_value_yn());
                st.setString(idx++, val.getInit_value());
                st.setString(idx++, val.getEvent_query());
                st.setString(idx++, val.getInsert_date());
                st.setString(idx++, val.getUpdate_date());

                st.setString(idx++, val.getUpdate_date());

                if (val.getEvent_name() != null) {
                    st.setString(idx++, val.getEvent_name());
                }

                if (val.getEvent_name_order() != null) {
                    st.setString(idx++, val.getEvent_name_order());
                }

                if (val.getEvent_descp() != null) {
                    st.setString(idx++, val.getEvent_descp());
                }

                if (val.getEvent_type() != null) {
                    st.setString(idx++, val.getEvent_type());
                }

                if (val.getInit_value_yn() != null) {
                    st.setString(idx++, val.getInit_value_yn());
                }

                if (val.getInit_value() != null) {
                    st.setString(idx++, val.getInit_value());
                }

                if (val.getEvent_query() != null) {
                    st.setString(idx++, val.getEvent_query());
                }

                st.executeUpdate();
            }
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to write object [key=" + key + ", val=" + val + ']', e);
        }
    }

    @Override public void delete(Object key) {
        Connection conn = ses.attachment();
        try (PreparedStatement st = conn.prepareStatement("DELETE FROM input_event WHERE event_id = ?")) {
            st.setLong(1, (Long)key);
            st.executeUpdate();
        }
        catch (SQLException e) {
            throw new CacheWriterException("Failed to delete object [key=" + key + ']', e);
        }
    }

    @Override public void loadCache(IgniteBiInClosure<Long, InputEvent> clo, Object... args) {
        Connection conn = ses.attachment();
        String sql = " SELECT event_id, event_name, event_name_order, event_descp, event_type, init_value_yn, " +
                " init_value, event_query, DATE_FORMAT(insert_date, '%Y-%m-%d %H:%i:%s'), " +
                " DATE_FORMAT(update_date, '%Y-%m-%d %H:%i:%s') FROM input_event ";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                InputEvent inputEvent = new InputEvent();
                inputEvent.setEvent_id(rs.getLong(1));
                inputEvent.setEvent_name(rs.getString(2));
                inputEvent.setEvent_name_order(rs.getString(3));
                inputEvent.setEvent_descp(rs.getString(4));
                inputEvent.setEvent_type(rs.getString(5));
                inputEvent.setInit_value_yn(rs.getString(6));
                inputEvent.setInit_value(rs.getString(7));
                inputEvent.setEvent_query(rs.getString(8));
                inputEvent.setInsert_date(rs.getString(9));
                inputEvent.setUpdate_date(rs.getString(10));

                clo.apply(inputEvent.getEvent_id(), inputEvent);
            }

        }
        catch (SQLException e) {
            throw new CacheLoaderException("Failed to load values from cache store.", e);
        }
    }

}
