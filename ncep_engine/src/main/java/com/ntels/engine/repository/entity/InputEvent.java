package com.ntels.engine.repository.entity;

import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Apache Ignite Entity Class(Input Event)
 */
public class InputEvent implements Serializable {

    @QuerySqlField(index = true)
    private Long event_id; // Event ID

    @QuerySqlField
    private String event_name; // Event 명

    @QuerySqlField
    private String event_name_order; // Event 변수명 순서

    @QuerySqlField
    private String event_descp; // Event 설명

    @QuerySqlField
    private String event_type; // Event Type(Table or Stream)

    @QuerySqlField
    private String init_value_yn; // 초기 데이터 유무

    @QuerySqlField
    private String init_value; // 초기 데이터 값

    @QuerySqlField
    private String event_query; // Event Define Query

    @QuerySqlField
    private String insert_date; // 생성 일자

    @QuerySqlField
    private String update_date; // 수정 일자

    public Long getEvent_id() {
        return event_id;
    }

    public void setEvent_id(Long event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_name_order() {
        return event_name_order;
    }

    public void setEvent_name_order(String event_name_order) {
        this.event_name_order = event_name_order;
    }

    public String getEvent_descp() {
        return event_descp;
    }

    public void setEvent_descp(String event_descp) {
        this.event_descp = event_descp;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getInit_value_yn() {
        return init_value_yn;
    }

    public void setInit_value_yn(String init_value_yn) {
        this.init_value_yn = init_value_yn;
    }

    public String getInit_value() {
        return init_value;
    }

    public void setInit_value(String init_value) {
        this.init_value = init_value;
    }

    public String getEvent_query() {
        return event_query;
    }

    public void setEvent_query(String event_query) {
        this.event_query = event_query;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    @Override
    public String toString() {
        return "InputEvent{" +
                "event_id=" + event_id +
                ", event_name='" + event_name + '\'' +
                ", event_name_order='" + event_name_order + '\'' +
                ", event_descp='" + event_descp + '\'' +
                ", event_type='" + event_type + '\'' +
                ", init_value_yn='" + init_value_yn + '\'' +
                ", init_value='" + init_value + '\'' +
                ", event_query='" + event_query + '\'' +
                ", insert_date='" + insert_date + '\'' +
                ", update_date='" + update_date + '\'' +
                '}';
    }

}
