package com.ntels.engine.repository.store;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import io.vertx.core.json.JsonObject;
import org.apache.ignite.cache.store.CacheStoreSessionListener;
import org.apache.ignite.cache.store.jdbc.CacheJdbcStoreSessionListener;

import javax.cache.configuration.Factory;

public class CacheStoreSessionListenerFactory implements Factory<CacheStoreSessionListener> {

    private static JsonObject config;

    public CacheStoreSessionListenerFactory(JsonObject config) {
        this.config = config;
    }

    @Override
    public CacheStoreSessionListener create() {
        CacheJdbcStoreSessionListener lsnr = new CacheJdbcStoreSessionListener();
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL(config.getString("url"));
        ds.setUser(config.getString("user"));
        ds.setPassword(config.getString("password"));
        lsnr.setDataSource(ds);

        return lsnr;
    }
}
