package com.ntels.engine.rule;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;
import org.wso2.siddhi.core.event.Event;
import org.wso2.siddhi.core.stream.output.StreamCallback;

/**
 * Rule Detect 후처리 Class
 */
public class StreamCallbackHandler extends StreamCallback {

    private Logger logger = Logger.getLogger(getClass());
    private Vertx vertx;

    public StreamCallbackHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void receive(Event event) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < event.getData().length; i++) {
            sb.append(event.getData(i) + ", ");
        }
        logger.debug("receive : " + sb.toString());

        String rule_id = this.getStreamId().split("_")[1];
        Object obj[] = event.getData();

        JsonObject sendObj = new JsonObject();
        sendObj.put("msg", "Rule " + this.getStreamId() + " received!!!\n" + sb.toString());
        sendObj.put("rule_id", rule_id);
        vertx.eventBus().send("out.websocket", sendObj);

        JsonObject data = new JsonObject();
        for (int i = 0; i < obj.length; i++) {
            data.put(String.valueOf(i), obj[i]);
        }

        JsonObject param = new JsonObject();
        param.put("rule_id", rule_id);
        param.put("data", data);

        vertx.eventBus().send("out.rule.result", param);

    }

    @Override
    public void receive(Event[] events) {
        for (int i = 0; i < events.length; i++) {
            logger.debug("receive(" + i + ") : " + events[i]);
        }
    }

}
