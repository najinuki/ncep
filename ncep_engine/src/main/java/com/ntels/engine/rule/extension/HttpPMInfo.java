package com.ntels.engine.rule.extension;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.wso2.siddhi.annotation.Example;
import org.wso2.siddhi.annotation.Extension;
import org.wso2.siddhi.annotation.Parameter;
import org.wso2.siddhi.annotation.ReturnAttribute;
import org.wso2.siddhi.annotation.util.DataType;
import org.wso2.siddhi.core.config.SiddhiAppContext;
import org.wso2.siddhi.core.exception.SiddhiAppRuntimeException;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.function.FunctionExecutor;
import org.wso2.siddhi.core.util.config.ConfigReader;
import org.wso2.siddhi.query.api.definition.Attribute;
import org.wso2.siddhi.query.api.exception.SiddhiAppValidationException;

import java.util.Map;
import org.apache.log4j.Logger;

@Extension(
        name = "httpPmInfo",
        namespace = "ntels",
        description = "공공데이터를 이용해서 미세먼지 정보를 조회하는 함수",
        parameters = {
                @Parameter(name = "station",
                        description = "미세먼지 측정소 주소",
                        type = {DataType.STRING})
        },
        returnAttributes = @ReturnAttribute(
                description = "JsonArray 형태로 Return",
                type = {DataType.OBJECT}),
        examples = {
                @Example(
                        syntax = "from fooStream\n" +
                                "select ntels:httpPmInfo('강남구') as pm\n" +
                                "insert into output;",
                        description = "현재 시간 기준으로 가장 최신 미세먼지 정보 결과를 리턴한다.")
        }
)
public class HttpPMInfo extends FunctionExecutor {

    private CloseableHttpClient httpclient = null;
    private static Logger logger = Logger.getLogger(HttpPMInfo.class);

    @Override
    protected void init(ExpressionExecutor[] attributeExpressionExecutors, ConfigReader configReader,
                        SiddhiAppContext siddhiAppContext) {
        logger.debug("HttpPMInfo init");

        httpclient = HttpClients.custom().build();

        if (attributeExpressionExecutors.length < 1) {
            throw new SiddhiAppValidationException("필수 파라메터 오류(stationName을 입력해 주세요.) : " + attributeExpressionExecutors.length);
        }
    }

    @Override
    protected Object execute(Object[] data) {
        logger.debug("HttpPMInfo execute-1");
        String pm25 = null;

        try {
            String station = (String) data[0];
            String url = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty";
            String parameter = "?_returnType=json&dataTerm=month&pageNo=1&numOfRows=1&stationName=" + station + "&ver=1.3";
            parameter += "&ServiceKey=%2Bb8ek0q0ptON4Qd%2FUqF5GED%2Fc4u%2BEq%2BoR77zSrjU8FoVfZSxyW%2FfZvQapkr4hsMj8jug2cSUClWWRElLSDkh8g%3D%3D";

            HttpGet httpGet = new HttpGet(url + parameter);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            String result = EntityUtils.toString(response.getEntity(), "UTF-8");
            JsonObject obj = new JsonObject(result);
            JsonArray list = obj.getJsonArray("list");

            logger.debug(">>>>> resultArray : " + list.encode());

            // 가장 최신 데이터 1개에서 PM 2.5값만 리턴
            pm25 = list.getJsonObject(0).getString("pm25Value");

        } catch (Exception e) {
            e.printStackTrace();
            throw new SiddhiAppRuntimeException("Siddhi Runtime Exception : " + e.getMessage());
        }

        return Long.parseLong(pm25);
    }

    @Override
    protected Object execute(Object data) {
        logger.debug("HttpPMInfo execute-2");
        String pm25 = null;

        try {
            String station = (String) data;
            String url = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty";
            String parameter = "?_returnType=json&dataTerm=month&pageNo=1&numOfRows=1&stationName=" + station + "&ver=1.3";
            parameter += "&ServiceKey=%2Bb8ek0q0ptON4Qd%2FUqF5GED%2Fc4u%2BEq%2BoR77zSrjU8FoVfZSxyW%2FfZvQapkr4hsMj8jug2cSUClWWRElLSDkh8g%3D%3D";

            HttpGet httpGet = new HttpGet(url + parameter);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            String result = EntityUtils.toString(response.getEntity(), "UTF-8");
            JsonObject obj = new JsonObject(result);
            JsonArray list = obj.getJsonArray("list");

            logger.debug(">>>>> resultArray : " + list.encode());

            // 가장 최신 데이터 1개에서 PM 2.5값만 리턴
            pm25 = list.getJsonObject(0).getString("pm25Value");

        } catch (Exception e) {
            e.printStackTrace();
            throw new SiddhiAppRuntimeException("Siddhi Runtime Exception : " + e.getMessage());
        }

        return Long.parseLong(pm25);

    }

    @Override
    public Attribute.Type getReturnType() {
        return Attribute.Type.LONG;
    }

    @Override
    public Map<String, Object> currentState() {
        return null;    //No need to maintain a state.
    }

    @Override
    public void restoreState(Map<String, Object> state) {
        //Since there's no need to maintain a state, nothing needs to be done here.
    }
}
