package com.ntels.engine.rule.extension.rule.smartcampus.rule;

import org.apache.log4j.Logger;

/**
 * Smart Campus 용 Extension 함수
 */
public class SmartCampusQuery {
    private static Logger logger = Logger.getLogger(SmartCampusQuery.class);

    public String getQuery(String rule_name, String[] params) {
        String query = "";

        if (rule_name.equals("rule_2") && params.length == 4) {
            query = " SELECT SUM(CASE \n" +
                    " WHEN TIMESTAMPDIFF(MINUTE, AA.update_date, NOW()) > " + params[3] + " AND CC.status = 'IN' THEN 0 \n" +
                    " ELSE 1 END) AS CNT \n" +
                    " FROM device_last_status AA, \n" +
                    " (SELECT c.device_id, c.node_id, c.`group`, b.model_number, \n" +
                    "         a.class AS dev_class, a.class_type AS dev_class_type, \n" +
                    "         d.class AS cmd_class, d.class_type AS cmd_class_type, d.class_id \n" +
                    " FROM device_class a, device_info b, device c, command_class d  \n" +
                    " WHERE  a.device_info_id = b.id AND b.manufacturer = c.manufaturer AND \n" +
                    "   b.model_number = c.model_number AND a.id = d.device_class_id AND d.visible = 1 \n" +
                    "   AND d.mandatory = 1 AND c.`group` = 'motion' AND c.device_id = '" + params[0] + "' AND c.node_id <> '' \n" +
                    "   AND d.class = '" + params[1] + "' AND d.class_type = '" + params[2] + "' ) BB,\n" +
                    "   (SELECT * FROM modes WHERE mode_name = 'Pressence') CC \n" +
                    "   WHERE AA.device_id = BB.device_id AND AA.node_id = BB.node_id AND \n" +
                    "         AA.class_id = BB.class_id AND AA.device_id = CC.device_id\n";
        } else if (rule_name.equals("rule_7") && params.length == 5) {
            query = "SELECT COUNT(*) AS cnt \n" +
                    "FROM (\n" +
                    "SELECT a.node_id, b.class_id, b.class_value, b.update_date, NOW()\n" +
                    "FROM device a LEFT OUTER JOIN (SELECT * FROM device_last_status WHERE class_id = '" + params[0] +"') b \n" +
                    "ON a.device_id = b.device_id AND a.node_id = b.node_id\n" +
                    ", device_info c, \n" +
                    "(SELECT * FROM device_class WHERE class='" + params[1] + "' AND class_type = '" + params[2] + "') d\n" +
                    "WHERE a.manufaturer = c.manufacturer AND a.model_number = c.model_number AND a.profile_version = c.profile_version\n" +
                    "  AND c.id = d.device_info_id\n" +
                    "  AND a.device_id = '" + params[3] + "' AND a.node_id <> ''\n" +
                    ") a\n" +
                    "WHERE a.class_value = '" + params[4] + "'";
        }

        System.out.println("Query For Smartcampuse Service: " + query);

        return query;
    }
}
