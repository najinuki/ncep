package com.ntels.engine.rule.extension.rule.nipa;

import com.ntels.engine.rule.extension.rule.NipaNotiUtil;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * NIPA 용 Extension 함수
 */
public class NipaQuery {
    private static Logger logger = Logger.getLogger(NipaQuery.class);

    public String getQuery(String rule_name, String[] params) {
        String query = "";

        if (rule_name.equals("rule_1")) {
            int nYear;
            int nMonth;

            Calendar calendar = new GregorianCalendar(Locale.KOREA);
            nYear = calendar.get(Calendar.YEAR);
            nMonth = calendar.get(Calendar.MONTH) + 1;

            query = "SELECT EMS_GOALS.BLD_ID AS bld_id, EMS_GOALS.GVALUE AS gvalue, COALESCE(EMS_GOALS.GMAX, 100) AS gmax\n" +
                    "FROM T_BUILDINGS as BLD, T_BLD_EMS_GOALS as EMS_GOALS\n" +
                    "WHERE BLD.BLD_ID = EMS_GOALS.BLD_ID\n" +
                    "AND EMS_GOALS.GYEAR = '" + nYear + "'\n" +
                    "AND EMS_GOALS.GMONTH = '" + nMonth + "'\n" +
                    "AND EMS_GOALS.GKIND = 'elec' \n" +
                    "AND EMS_GOALS.ISSHOW = 'Y'\n" +
                    "AND BLD.NOTI_YN = 'Y'";
        } else if (rule_name.equals("rule_2")) {
            NipaNotiUtil nipaNotiUtil = new NipaNotiUtil();

            LocalDateTime peakDateTime = nipaNotiUtil.getPredDateTime();
            String formattedPeakDateTime = peakDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

            query = "SELECT DISTINCT MST.REAL_PEAK_CPVALUE as real_peak_cpvalue, \n" +
                    "MST.AND_OR as and_or, MST.ML_PEAK_CPVALUE as ml_peak_cpvalue, \n" +
                    "MST.OPTCTL_NAME as optctl_name, \n" +
                    "BLD.ELEC_CONTRACT as elec_contract, COALESCE(ML.PRED_VALUE, 0.0) as pred_value, BLD.BLD_ID as bld_id \n" +
                    "FROM T_OPTCTL_PEAK_MODELS AS MST \n" +
                    "INNER JOIN T_BLD_OPTCTL_LIST AS CTL_LIST\n" +
                    "ON MST.OPTCTL_P_ID = CTL_LIST.MODEL_ID\n" +
                    "INNER JOIN T_BUILDINGS AS BLD\n" +
                    "ON CTL_LIST.BLD_ID = BLD.BLD_ID\n" +
                    "INNER JOIN T_ML_PREDICATE_MIN AS ML\n" +
                    "ON BLD.BLD_ID = ML.BLD_ID\n" +
                    "WHERE CTL_LIST.ISAPPLY = 'Y'\n" +
                    "AND ML.PRED_DATE = '" +  formattedPeakDateTime + "'";
        } else if (rule_name.equals("rule_2_1")) {
            query = "SELECT FACIL_POINT.POINT_ID as point_id, FACIL_POINT.BLD_ID as bld_id, \n" +
                    "FACIL_LIST.facil_name, FACIL_LIST.facil_id, REF_POINT.MEASURE_ID as measure_id \n" +
                    "FROM T_BLD_FACIL_LIST FACIL_LIST \n" +
                    "INNER JOIN T_BLD_FACIL_POINTS FACIL_POINT \n" +
                    "ON FACIL_LIST.FACIL_ID = FACIL_POINT.FACIL_ID \n" +
                    "AND FACIL_LIST.BLD_ID = FACIL_POINT.BLD_ID\n" +
                    "INNER JOIN T_GATEWAYS GW \n" +
                    "ON GW.BLD_ID = facil_point.bld_id \n" +
                    "INNER JOIN T_POINTS POINT \n" +
                    "ON GW.GW_ID = POINT.GW_ID \n" +
                    "AND FACIL_POINT.POINT_ID = POINT.POINT_ID \n" +
                    "INNER JOIN T_REF_POINT_LIST REF_POINT \n" +
                    "ON POINT.POINT_REF_ID = REF_POINT.POINT_REF_ID \n" +
                    "WHERE FACIL_LIST.BLD_ID = '" + params[0] + "' AND FACIL_LIST.PEAK_SHUTOFF_ORDER > '' \n" +
                    "AND REF_POINT.MEASURE_ID IN  ('OSS','OS') \n" +
                    "ORDER BY REF_POINT.MEASURE_ID desc, FACIL_LIST.PEAK_SHUTOFF_ORDER";
        }

        System.out.println("Query For Nipa Service " +  rule_name + ": " + query);

        return query;
    }
}
