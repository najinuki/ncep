package com.ntels.engine.rule.extension;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.wso2.siddhi.annotation.Example;
import org.wso2.siddhi.annotation.Extension;
import org.wso2.siddhi.annotation.Parameter;
import org.wso2.siddhi.annotation.ReturnAttribute;
import org.wso2.siddhi.annotation.util.DataType;
import org.wso2.siddhi.core.config.SiddhiAppContext;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.function.FunctionExecutor;
import org.wso2.siddhi.core.util.config.ConfigReader;
import org.wso2.siddhi.query.api.definition.Attribute;
import org.wso2.siddhi.query.api.exception.SiddhiAppValidationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Extension(
        name = "selectCol",
        namespace = "ntels",
        description = " ",
        parameters = {
                @Parameter(name = "jdbc.url",
                        description = "JDBC Full URL Ex) jdbc:mysql://localhost:3306/test",
                        type = {DataType.STRING}),
                @Parameter(name = "id",
                        description = "DB ID",
                        type = {DataType.STRING}),
                @Parameter(name = "password",
                        description = "DB Password",
                        type = {DataType.STRING}),
                @Parameter(name = "query",
                        description = "SQL Query",
                        type = {DataType.STRING}),
                @Parameter(name = "params",
                        description = "SQL Query Parameter",
                        type = {DataType.STRING})
        },
        returnAttributes = @ReturnAttribute(
                description = "JsonArray 형태로 Return",
                type = {DataType.OBJECT}),
        examples = {
                @Example(
                        syntax = "from fooStream\n" +
                                "select ntels:selectCol('select * from rule') as result\n" +
                                "insert into output;",
                        description = ".")
        }
)
public class SQLQuery extends FunctionExecutor {

    private static Logger logger = Logger.getLogger(SQLQuery.class);

    @Override
    protected void init(ExpressionExecutor[] attributeExpressionExecutors, ConfigReader configReader,
                        SiddhiAppContext siddhiAppContext) {
        logger.debug("SQLQuery init");

        if (attributeExpressionExecutors.length < 4) {
            throw new SiddhiAppValidationException("필수 파라메터 오류 : " + attributeExpressionExecutors.length);
        }
    }

    @Override
    protected Object execute(Object[] data) {
        logger.debug("SQLQuery execute-1");

        String url = (String) data[0];
        String id = (String) data[1];
        String pw = (String) data[2];
        String query = (String) data[3];
        int idx = 0;
        String params[] = new String[data.length - 4 + 1];

        for (int i = 4; i < data.length; i++) {
            params[idx++] = (String) data[i];
        }

        String db_url = url + "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC";

        StringBuffer result = new StringBuffer();
        Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(db_url, id, pw);

            final String regex = "\\?";
            Pattern pattern = Pattern.compile(regex);
            Matcher match = pattern.matcher(query);

            StringBuffer sb = new StringBuffer();
            int i = 0;
            while (match.find()) {
                String changeStr = match.group().replace("?", "'" + params[i++] + "'");
                match.appendReplacement(sb, changeStr);
            }
            match.appendTail(sb);

            logger.debug("sql query >> " + sb.toString());
            PreparedStatement stmt = conn.prepareStatement(sb.toString());
            ResultSet rs = stmt.executeQuery();

            int j = 1;
            while (rs.next()) {
                if (rs.isLast()) {
                    result.append(rs.getObject(j++));
                } else {
                    result.append(rs.getObject(j++)).append(",");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {}
        }

        return result;
    }

    @Override
    protected Object execute(Object data) {
        logger.debug("SQLQuery execute-2");
        return "";
    }

    @Override
    public Attribute.Type getReturnType() {
        return Attribute.Type.OBJECT;
    }

    @Override
    public Map<String, Object> currentState() {
        return null;    //No need to maintain a state.
    }

    @Override
    public void restoreState(Map<String, Object> state) {
        //Since there's no need to maintain a state, nothing needs to be done here.
    }
}
