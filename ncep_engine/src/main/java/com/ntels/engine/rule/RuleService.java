package com.ntels.engine.rule;

import com.ntels.engine.common.ErrorCode;
import com.ntels.engine.common.NCEPException;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;
import org.wso2.siddhi.core.SiddhiAppRuntime;
import org.wso2.siddhi.core.SiddhiManager;
import org.wso2.siddhi.core.event.Event;
import org.wso2.siddhi.core.stream.input.InputHandler;

import java.util.Map;
import java.util.Set;

/**
 * WSO2 Siddhi Core 처리 클래스
 */
public class RuleService {

    private static Logger logger = Logger.getLogger(RuleService.class);
    private static SiddhiManager siddhiManager = new SiddhiManager();

    public RuleService() {


    }

    /**
     * Siddhi Rule 생성
     * @param vertx vertx 인스턴스
     * @param query Siddhi 쿼리문
     * @param callback Callback 함수명
     */
    public static void createRule(Vertx vertx, String query, String callback) {
        SiddhiAppRuntime siddhiAppRuntime = siddhiManager.createSiddhiAppRuntime(query);
        siddhiAppRuntime.start();

        siddhiAppRuntime.addCallback(callback, new StreamCallbackHandler(vertx));
    }

    /**
     * Siddhi Rule 삭제
     * @param appName Rule 이름
     */
    public static void deleteRule(String appName) {
        try {
            SiddhiAppRuntime app = siddhiManager.getSiddhiAppRuntime(appName);
            app.shutdown();
        } catch (Exception e) {
            throw new NCEPException(ErrorCode.DATA_NOT_EXIST_ERROR, ErrorCode.DATA_NOT_EXIST_ERROR_MSG);
        }
    }

    /**
     * Siddhi Rule 검증
     * @param query Siddhi 쿼리문
     */
    public static void validateRule(String query) {
        try {
            siddhiManager.validateSiddhiApp(query);
        } catch (Exception e) {
            throw new NCEPException(ErrorCode.RULE_SYNTAX_ERROR, e.getMessage());
        }
    }

    /**
     * Siddhi Event 전송
     * @param appName Rule 이름
     * @param eventName Event 명
     * @param event Event 내용
     */
    public static void sendEvent(String appName, String eventName, JsonObject event, JsonArray eventNameOrder) {
        try {
            SiddhiAppRuntime app = siddhiManager.getSiddhiAppRuntime(appName);
            InputHandler inputHandler = app.getInputHandler(eventName);

            Object obj[] = new Object[eventNameOrder.size()];

            for (int i = 0; i < eventNameOrder.size(); i++) {
                obj[i] = event.getValue(eventNameOrder.getString(i));
            }

            inputHandler.send(obj);

        } catch (Exception e) {
            throw new NCEPException(ErrorCode.RULE_EXECUTE_ERROR, e.getMessage());
        }
    }

    /**
     * Siddhi Table 데이터 등록
     * @param appName Rule 이름
     */
    public static void insertTableData(String appName, String query) {
        try {
            SiddhiAppRuntime app = siddhiManager.getSiddhiAppRuntime(appName);
            app.query(query);
        } catch (Exception e) {
            throw new NCEPException(ErrorCode.RULE_DATA_INSERT_ERROR, ErrorCode.RULE_DATA_INSERT_ERROR_MSG);
        }
    }

}
