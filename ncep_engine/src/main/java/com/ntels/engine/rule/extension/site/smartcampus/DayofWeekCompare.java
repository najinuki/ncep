package com.ntels.engine.rule.extension.site.smartcampus;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.wso2.siddhi.annotation.Example;
import org.wso2.siddhi.annotation.Extension;
import org.wso2.siddhi.annotation.Parameter;
import org.wso2.siddhi.annotation.ReturnAttribute;
import org.wso2.siddhi.annotation.util.DataType;
import org.wso2.siddhi.core.config.SiddhiAppContext;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.function.FunctionExecutor;
import org.wso2.siddhi.core.util.config.ConfigReader;
import org.wso2.siddhi.query.api.definition.Attribute;
import org.wso2.siddhi.query.api.exception.SiddhiAppValidationException;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Extension(
        name = "dayofWeekComp",
        namespace = "ntels",
        description = "주어진 날짜 기준으로 요일 비교",
        parameters = {
                @Parameter(name = "start.dow",
                        description = "비교시작요일(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY)",
                        type = {DataType.STRING}),
                @Parameter(name = "end.dow",
                        description = "비교종료요일(MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY)",
                        type = {DataType.STRING}),
                @Parameter(name = "zone.id",
                        description = "zoneId(Asia/Seoul or Asia/Jakarta)",
                        type = {DataType.STRING})

        },
        returnAttributes = @ReturnAttribute(
                description = "True or False",
                type = {DataType.STRING}),
        examples = {
                @Example(
                        syntax = "from fooStream\n" +
                                 "select ntels:dayOfWeekComp('2018-10-26 18:00:00') as rslt\n" +
                                 "insert into output;",
                        description = "주어진 날짜 기준으로 요일 비교")
        }
)

public class DayofWeekCompare extends FunctionExecutor {

    private CloseableHttpClient httpclient = null;
    private static Logger logger = Logger.getLogger(DayofWeekCompare.class);

    @Override
    protected void init(ExpressionExecutor[] attributeExpressionExecutors, ConfigReader configReader,
                        SiddhiAppContext siddhiAppContext) {
        logger.debug("dayofWeekCompare init");

        httpclient = HttpClients.custom().build();

        if (attributeExpressionExecutors.length < 1) {
            throw new SiddhiAppValidationException("필수 파라메터 오류(stationName을 입력해 주세요.) : " + attributeExpressionExecutors.length);
        }
    }

    @Override
    protected Object execute(Object[] data) {
        logger.debug("dayofWeekCompare execute-1");

        String startDow = (String) data[0];
        String endDow = (String) data[1];
        String zoneId = (String) data[2];

        LocalDateTime nowLtime = LocalDateTime.now();
        ZonedDateTime utcNowLTime = ZonedDateTime.of(nowLtime, ZoneId.of("UTC"));
        ZonedDateTime chgNowLTime = ZonedDateTime.ofInstant(utcNowLTime.toInstant(), ZoneId.of(zoneId));

        DayOfWeek ddow = chgNowLTime.getDayOfWeek();
        DayOfWeek sdow = DayOfWeek.valueOf(startDow);
        DayOfWeek edow = DayOfWeek.valueOf(endDow);

        System.out.println("dayofweek : " + sdow + ", " + edow + ", " + ddow);

        logger.debug("---------- DayofWeekCompare Info ----------");
        logger.debug("nowLtime : " + nowLtime);
        logger.debug("DayofWeek : " + ddow);
        logger.debug("DayofWeek StartTime: " + sdow);
        logger.debug("DayofWeek EndTime: " + edow);
        logger.debug("----------------------------------------");

        String result;
        if (ddow.getValue() <= edow.getValue() && ddow.getValue() >= sdow.getValue()) {
            result = "True";
        } else {
            result = "False";
        }

        return result;
    }

    @Override
    protected Object execute(Object data) {
        logger.debug("dayofWeekCompare execute-2");

        return "False";

    }

    @Override
    public Attribute.Type getReturnType() {
        return Attribute.Type.STRING;
    }

    @Override
    public Map<String, Object> currentState() {
        return null;    //No need to maintain a state.
    }

    @Override
    public void restoreState(Map<String, Object> state) {
        //Since there's no need to maintain a state, nothing needs to be done here.
    }
}
