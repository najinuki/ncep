package com.ntels.engine.rule.extension.rule;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

import java.sql.*;

public class DBManager {

    private static final String NCEP_LOCAL_URL = "jdbc:mysql://localhost:3306/ncep_service";
    private static final String NCEP_DOCKER_URL = "jdbc:mysql://172.17.0.1:3366/ncep_service";
    private static final String NCEP_SMARTCAMPUS_URL = "jdbc:mysql://52.231.157.107:3366/ncep_service";
    private static final String NCEP_NIPA_URL = "jdbc:mysql://210.219.151.172:3366/ncep_service";
    private static final String NISBCP_PUBLIC_URL = "jdbc:mysql://210.219.151.173:3306/nisbcp";
    private static final String NISBCP_PRIVATE_URL = "jdbc:mysql://192.168.13.20:3306/nisbcp";
    private static final String SERVICEHUB_PUBLIC_URL = "jdbc:mysql://52.231.192.50:3306/servicehub";
    private static final String SERVICEHUB_PRIVATE_URL = "jdbc:mysql://10.0.0.9:3306/servicehub";

    private static final String NCEP_USER = "ncep";
    private static final String NCEP_PASSWD = "ncep123#";
    private static final String NISBCP_USER = "nisbcp";
    private static final String NISBCP_PASSWD = "nisbcp123";
    private static final String SERVICEHUB_USER = "servicehub";
    private static final String SERVICEHUB_PASSWD = "servicehub123#";

    private static Logger logger = Logger.getLogger(DBManager.class);

    public Connection getDBConnection(String dbName, String dbOption) {
        System.out.println("Start getDBConnection");

        Connection conn = null;
        String dbUrl = "";
        String dbUrlOption = "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC&useSSL=false";
        String user = "";
        String passwd = "";

        if (dbName.equals(NCEP_USER)) {
            dbUrl = NCEP_DOCKER_URL;
            user = DBManager.NCEP_USER;
            passwd = DBManager.NCEP_PASSWD;

        } else if (dbName.equals(NISBCP_USER)) {
            user = DBManager.NISBCP_USER;
            passwd = DBManager.NISBCP_PASSWD;

            if (dbOption.equals("public")) {
                dbUrl = NISBCP_PUBLIC_URL;
            } else if (dbOption.equals("private")) {
                dbUrl = NISBCP_PRIVATE_URL;
            }
        } else if (dbName.equals(SERVICEHUB_USER)) {
            user = DBManager.SERVICEHUB_USER;
            passwd = DBManager.SERVICEHUB_PASSWD;

            if (dbOption.equals("public")) {
                dbUrl = SERVICEHUB_PUBLIC_URL;
            } else if (dbOption.equals("private")) {
                dbUrl = SERVICEHUB_PRIVATE_URL;
            }
        }

        try {
            dbUrl = dbUrl + dbUrlOption;

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(dbUrl, user, passwd);
        } catch (Exception e) {
            logger.debug("getDBConnection Exception: " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("End getDBConnection");

        return conn;
    }

    public int getDBResult(Connection conn, String query) {
        System.out.println("Start getDBResult");

        int result = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = conn.prepareStatement(query);
            rs = stmt.executeQuery();

            while (rs.next()) {
                result = rs.getInt(1);
                System.out.println("Check Result : " + result);
            }
        } catch (Exception e) {
            logger.debug("getDBResult Exception: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.debug("ResultSet Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    logger.debug("PreparedStatement Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    logger.debug("Connection Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        System.out.println("End getDBResult");

        return result;
    }

    public JsonObject getDBResultForJsonObject(Connection conn, String query) {
        System.out.println("Start getDBResultForJsonObject");

        JsonObject result = new JsonObject();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = conn.prepareStatement(query);
            rs = stmt.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();

            JsonArray resultArray = new JsonArray();

            while(rs.next()) {
                int numColumns = rsmd.getColumnCount();

                JsonObject retrieveData = new JsonObject();

                for (int i=1; i<=numColumns; i++) {
                    String column_name = rsmd.getColumnName(i);
                    retrieveData.put(column_name, rs.getObject(column_name));
                }
                resultArray.add(retrieveData);
            }

            result.put("data", resultArray);

            System.out.println("Check getDBResultForJsonObject Data: " + result);

        } catch (Exception e) {
            logger.debug("getDBResultForJsonObject Exception: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    logger.debug("ResultSet Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    logger.debug("PreparedStatement Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    logger.debug("Connection Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        System.out.println("End getDBResultForJsonObject");

        return result;
    }

    public boolean insertDB(Connection conn, String query) {
        System.out.println("Start insertDBResult");

        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = conn.prepareStatement(query);
            stmt.executeUpdate();
            result = true;
        } catch (Exception e) {
            logger.debug("insertDBResult Exception: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    logger.debug("PreparedStatement Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    logger.debug("Connection Close SQLException: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        System.out.println("End insertDBResult");

        return result;
    }
}
