package com.ntels.engine.rule.extension;

import com.ntels.engine.rule.extension.rule.DBManager;
import com.ntels.engine.rule.extension.rule.NipaNotiManager;
import com.ntels.engine.rule.extension.rule.NipaNotiUtil;
import com.ntels.engine.rule.extension.rule.nipa.NipaQuery;
import com.ntels.engine.rule.extension.rule.smartcampus.rule.SmartCampusQuery;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;
import org.wso2.siddhi.annotation.Example;
import org.wso2.siddhi.annotation.Extension;
import org.wso2.siddhi.annotation.Parameter;
import org.wso2.siddhi.annotation.ReturnAttribute;
import org.wso2.siddhi.annotation.util.DataType;
import org.wso2.siddhi.core.config.SiddhiAppContext;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.function.FunctionExecutor;
import org.wso2.siddhi.core.util.config.ConfigReader;
import org.wso2.siddhi.query.api.definition.Attribute;
import org.wso2.siddhi.query.api.exception.SiddhiAppValidationException;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Map;

@Extension(
        name = "se",
        namespace = "ntels",
        description = " ",
        parameters = {
                @Parameter(name = "db.name",
                        description = "ncep",
                        type = {DataType.STRING}),
                @Parameter(name = "db.option",
                        description = "private",
                        type = {DataType.STRING}),
                @Parameter(name = "rule.type",
                        description = "smartcampus",
                        type = {DataType.STRING}),
                @Parameter(name = "rule.name",
                        description = "rule_1",
                        type = {DataType.STRING}),
                @Parameter(name = "query.params",
                        description = "SENSOR.NOTIFICATION,NTELS_4,D44157450456,motionalarm",
                        type = {DataType.OBJECT})
        },
        returnAttributes = @ReturnAttribute(
                description = "result",
                type = {DataType.INT}),
        examples = {
                @Example(
                    syntax = "from node_content" +
                             "select ntels:se('ncep','','smartcampus','rule_1','dooralarm,SENSOR.NOTIFICATION,NTELS_1,D44157450456,TRUE') as result" +
                             "insert into output;",
                    description = ".")
        }
)
public class SQLExecution extends FunctionExecutor {

    private static Logger logger = Logger.getLogger(SQLExecution.class);

    private DBManager dbManager = new DBManager();
    private NipaNotiManager nipaNotiManager = new NipaNotiManager();
    private NipaNotiUtil nipaNotiUtil = new NipaNotiUtil();

    private SmartCampusQuery smartcampusQuery = new SmartCampusQuery();
    private NipaQuery nipaQuery = new NipaQuery();

    @Override
    protected void init(ExpressionExecutor[] attributeExpressionExecutors, ConfigReader configReader,
                        SiddhiAppContext siddhiAppContext) {
        logger.debug("SQLExecution Init");

        if (attributeExpressionExecutors.length < 4) {
            throw new SiddhiAppValidationException("필수 파라메터 오류 : " + attributeExpressionExecutors.length);
        }
    }

    @Override
    protected Object execute(Object[] data) {
        logger.debug("SQLExecution Execute-1");

        String dbName = (String) data[0];
        String dbOption = (String) data[1];
        String ruleType = (String) data[2];
        String ruleName = (String) data[3];
        String queryParam = (String) data[4];
        String[] queryParams = queryParam.split(",");

        logger.debug("SQLExecution Check Params");
        logger.debug("dbName: " + dbName);
        logger.debug("dbOption: " + dbOption);
        logger.debug("ruleType: " + ruleType);
        logger.debug("ruleName: " + ruleName);
        logger.debug("queryParam: " + queryParam);
        logger.debug("=========================");


        Connection conn;
        String query;
        int result = 0;
        JsonObject jsonDBResult;

        try {
            conn = dbManager.getDBConnection(dbName, dbOption);

            if (ruleType.equals("smartcampus")) {
                query = smartcampusQuery.getQuery(ruleName, queryParams);

                logger.debug("Smartcampus Query: " + query);

                result = dbManager.getDBResult(conn, query);
            } else if (ruleType.equals("nipa")) {
                query = nipaQuery.getQuery(ruleName, queryParams);

                logger.debug("Nipa Query: " + query);

                jsonDBResult = dbManager.getDBResultForJsonObject(conn, query);

                String ip;

                if (dbOption.equals("private")) {
                    ip = NipaNotiManager.NISBCP_SERVICEHUB_MAIN_PRIVATE_IP;
                } else {
                    ip = NipaNotiManager.NISBCP_SERVICEHUB_MAIN_PUBLIC_IP;
                }

                if (ruleName.equals("rule_1") && jsonDBResult.size() > 0) {

                    LocalDateTime firstDaysofCurrentMonth = LocalDateTime.now().with(TemporalAdjusters.firstDayOfMonth());
                    LocalDateTime lastDaysofCurrentMonth = LocalDateTime.now().with(TemporalAdjusters.lastDayOfMonth());

                    String start_date = firstDaysofCurrentMonth.with(LocalTime.of(0, 0, 0)).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                    String end_date = lastDaysofCurrentMonth.with(LocalTime.of(23, 59, 59)).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

                    String path = "/energy/usages?start_date=" + start_date + "&end_date=" + end_date + "&energy_type=WH";

                    logger.debug("Nipa Rule_1 StartDate: " + start_date);
                    logger.debug("Nipa Rule_1 EndDate: " + end_date);
                    nipaNotiManager.notiGoalElectricPower(ip, path, jsonDBResult, dbOption);
                } else if (ruleName.equals("rule_2") && jsonDBResult.size() > 0) {

                    LocalDateTime bldUsageStartDateTime = nipaNotiUtil.getPredDateTime().minusMinutes(30);
                    LocalDateTime bldUsageEndDateTime = bldUsageStartDateTime.plusMinutes(15);

                    System.out.println("Nipa Rule_2 bldUsageStartDateTime: " + bldUsageStartDateTime);
                    System.out.println("Nipa Rule_2 bldUsageEndDateTime: " + bldUsageEndDateTime);

                    int startDateHour = bldUsageStartDateTime.getHour();
                    int startDateMinute = bldUsageStartDateTime.getMinute();
                    int endDateHour = bldUsageEndDateTime.getHour();
                    int endDateMinute = bldUsageEndDateTime.getMinute();
                    int endDateSecond = 59;

                    if (startDateHour < endDateHour) {
                        endDateHour = startDateHour;
                        endDateMinute = 59;
                    } else {
                        if (startDateMinute < endDateMinute) {
                            endDateMinute = bldUsageEndDateTime.minusMinutes(1).getMinute();
                        }
                    }

                    String startDate = bldUsageStartDateTime.with(LocalTime.of(startDateHour, startDateMinute, 0)).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
                    String endDate = bldUsageEndDateTime.with(LocalTime.of(endDateHour, endDateMinute, endDateSecond)).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

                    String path = "/energy/peak?start_date=" + startDate + "&end_date=" + endDate + "&energy_type=WH";

                    logger.debug("Nipa Rule_2 StartDate: " + startDate);
                    logger.debug("Nipa Rule_2 EndDate: " + endDate);
                    nipaNotiManager.notiPeakElectricPower(ip, path, jsonDBResult, dbOption);
                }
            }

        } catch (Exception e) {
            logger.error("SQLExecution Exception : ", e);
            e.printStackTrace();
        }

        logger.debug("Final Result : " + result);

        return result;
    }

    @Override
    protected Object execute(Object data) {
        logger.debug("SQLQuery Execute-2");
        return "";
    }

    @Override
    public Attribute.Type getReturnType() {
        return Attribute.Type.INT;
    }

    @Override
    public Map<String, Object> currentState() {
        return null;    //No need to maintain a state.
    }

    @Override
    public void restoreState(Map<String, Object> state) {
        //Since there's no need to maintain a state, nothing needs to be done here.
    }
}