package com.ntels.engine.rule.extension.rule;

import com.ntels.engine.rule.extension.rule.nipa.NipaQuery;
import io.vertx.core.Vertx;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NipaNotiManager {

    private static Logger logger = Logger.getLogger(NipaNotiManager.class);
    private DBManager dbManager = new DBManager();

    private static final String authorization = "Bearer 50b7c348-41af-4400-a839-29800622f288";
    public static final String NISBCP_SERVICEHUB_MAIN_PUBLIC_IP = "210.219.151.169";
    public static final String NISBCP_SERVICEHUB_MAIN_PRIVATE_IP = "192.168.13.16";
    private static final int NISBCP_SERVICEHUB_PORT = 10002;

    public void notiGoalElectricPower(String url, String path, JsonObject dbResult, String dbOption) {

        JsonObject apiResultData = new JsonObject();

        logger.debug("notiGoalElectricPower - Start Send REST API: IF_WEB_HUB_020");
        HttpClient client = Vertx.vertx().createHttpClient(new HttpClientOptions().setSsl(true).setTrustAll(true).setVerifyHost(false));
        HttpClientRequest clientRequest = client.request(HttpMethod.GET, NISBCP_SERVICEHUB_PORT, url, path, r_result -> {

            r_result.bodyHandler(buffer -> {
                logger.debug("notiGoalElectricPower - Start Bodyhandler");

                JsonObject bufferResult = buffer.toJsonObject();
                JsonArray dataArray = bufferResult.getJsonArray("data");

                logger.debug("notiGoalElectricPower - Start Check dbResult, calledApi Size&Result");
                logger.debug("dbResult size: " + dbResult.size());
                logger.debug("dbResult size: " + dbResult.toString());
                logger.debug("calledApiSize: " + dataArray.size());
                logger.debug("calledApiResult: " + dataArray.toString());
                logger.debug("notiGoalElectricPower - End Check dbResult, calledApi Size&Result");

                JsonArray energyDataArray = new JsonArray();

                for (int i=0; i<dataArray.size(); i++) {
                    JsonObject data = new JsonObject();

                    String bldId = dataArray.getJsonObject(i).getString("bld_id");
                    double usage = dataArray.getJsonObject(i).getDouble("usage");

                    data.put("bld_id", bldId);
                    data.put("usage", usage);

                    energyDataArray.add(data);
                }

                apiResultData.put("data", energyDataArray);

                System.out.println("notiGoalElectricPower - Start Check madeApiData");
                System.out.println("madeApiData: " + apiResultData);
                System.out.println("notiGoalElectricPower - End Check madeApiData");

                JsonArray apiResultDataArray = apiResultData.getJsonArray("data");
                JsonArray dbResultArray = dbResult.getJsonArray("data");

                String bldId;
                int gValue = 0;
                double gMax = 0;

                // 목표전력 계산법: 목표전력(=gvalue) X (임계치(=gmax) / 100) <= 실제전력값(=usage) -> 알람테이블에 적재

                Map<String, ArrayList> bldNotificationData = new HashMap<>();

                for (int j=0; j<dbResultArray.size(); j++) {
                    bldId = dbResultArray.getJsonObject(j).getString("bld_id");
                    gValue = dbResultArray.getJsonObject(j).getInteger("gvalue");
                    gMax = (double) dbResultArray.getJsonObject(j).getInteger("gmax");

                    for (int k=0; k<apiResultDataArray.size(); k++) {
                        if (bldId.equals(apiResultDataArray.getJsonObject(k).getString("bld_id"))) {
                            double usage = apiResultDataArray.getJsonObject(k).getDouble("usage");
                            double criticalPer = gMax / 100;
                            double mulResult = gValue * criticalPer;
                            int percentage = (int) ((usage / gValue) * 100);

                            logger.debug("notiGoalElectricPower - Start Check Calculation Result");
                            logger.debug("bldId: " + bldId);
                            logger.debug("gValue: " + String.valueOf(gValue) + ", gMax: " + String.valueOf(gMax));
                            logger.debug("criticalPer: " + String.valueOf(criticalPer) + ", mulResult: " + String.valueOf(mulResult));
                            logger.debug("usage: " + String.valueOf(usage) + ", percentage: " + String.valueOf(percentage));
                            logger.debug("notiGoalElectricPower - End Check Calculation Result");

                            if (mulResult <= usage) {
                                String sUsage = String.format("%.1f" , usage);

                                ArrayList<Object> vData = new ArrayList<>();
                                vData.add(sUsage); // 사용량(=value)
                                vData.add('4'); // 알림정도(=noti_level)

                                String message = "사용량이 목표사용량의 " + String.valueOf(percentage) + "%에 도달하였습니다. (목표 전력량: " + String.valueOf(gValue) + "kwh, 현재 전력량: " + sUsage + "kwh)";
                                vData.add(message);
                                vData.add("01");

                                logger.debug("notiGoalElectricPower - Rule Received Start => 목표전력 초과");
                                logger.debug("bld_id : " + bldId + ", 초과량 : " + String.valueOf(percentage) + "%, 목표전력량 : " + String.valueOf(gValue) + "kwh, 현재 전력량: " + sUsage + "kwh)");
                                logger.debug("notiGoalElectricPower - Rule Received End => 목표전력 초과");

                                bldNotificationData.put(bldId, vData);

                                System.out.println("notiGoalElectricPower - Start insertBldNotification");
                                boolean result = insertBldNotification(bldNotificationData, dbOption);
                                System.out.println("notiGoalElectricPower - End insertBldNotification");

                                logger.debug("notiGoalElectricPower - insertBldNotification Result: " + result);
                            }
                        }
                    }
                }
                logger.debug("notiGoalElectricPower - End Bodyhandler");

                client.close();
            });
        });
        logger.debug("notiGoalElectricPower - End Send REST API: IF_WEB_HUB_020");

        clientRequest.putHeader(HttpHeaders.AUTHORIZATION, authorization);
        clientRequest.putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        clientRequest.end();
    }

    public void notiPeakElectricPower(String url, String path, JsonObject dbResult, String dbOption) {
        JsonObject apiResultData = new JsonObject();

        HttpClient client = Vertx.vertx().createHttpClient(new HttpClientOptions().setSsl(true).setTrustAll(true).setVerifyHost(false));

        logger.debug("notiPeakElectricPower - Start Send REST API: IF_WEB_HUB_061");
        HttpClientRequest clientRequest = client.request(HttpMethod.GET, NISBCP_SERVICEHUB_PORT, url, path, r_result -> {

            r_result.bodyHandler(buffer -> {
                logger.debug("notiPeakElectricPower - Start Bodyhandler");

                JsonObject bufferResult = buffer.toJsonObject();
                JsonArray dataArray = bufferResult.getJsonArray("data");

                logger.debug("notiPeakElectricPower - Start Check dbResult, calledApi Size&Result");
                logger.debug("dbResult size: " + dbResult.size());
                logger.debug("dbResult size: " + dbResult.toString());
                logger.debug("calledApiSize: " + dataArray.size());
                logger.debug("calledApiResult: " + dataArray.toString());
                logger.debug("notiPeakElectricPower - End Check dbResult, calledApi Size&Result");

                JsonArray peakDataArray = new JsonArray();

                for (int i=0; i<dataArray.size(); i++) {
                    JsonObject data = new JsonObject();

                    String bldId = dataArray.getJsonObject(i).getString("bld_id");
                    double value = dataArray.getJsonObject(i).getDouble("value");

                    data.put("bld_id", bldId);
                    data.put("value", value);

                    peakDataArray.add(data);
                }

                apiResultData.put("data", peakDataArray);

                System.out.println("notiPeakElectricPower - Start Check madeApiData");
                System.out.println("madeApiData: " + apiResultData);
                System.out.println("notiPeakElectricPower - End Check madeApiData");

                JsonArray apiResultDataArray = apiResultData.getJsonArray("data");
                JsonArray dbResultArray = dbResult.getJsonArray("data");

                // 피크전력 계산법: (실제피크전력값(=value) / 계약전력(=elec_contract)) * 100 >= real_peak_cpvalue (and or or) ml_peak_cpvalue -> 알람테이블에 적재

                Map<String, ArrayList> bldNotificationData = new HashMap<>();

                String bldId = "";
                String andOrCondition = "";
                String optctlName = "";
                int realPeakCpvalue = 0;
                int mlPeakCpvalue = 0;
                int elecContract = 0;
                double predValue = 0;

                for (int j=0; j<dbResultArray.size(); j++) {
                    bldId = dbResultArray.getJsonObject(j).getString("bld_id");
                    andOrCondition = dbResultArray.getJsonObject(j).getString("and_or");
                    realPeakCpvalue = dbResultArray.getJsonObject(j).getInteger("real_peak_cpvalue");
                    optctlName = dbResultArray.getJsonObject(j).getString("optctl_name");
                    mlPeakCpvalue = dbResultArray.getJsonObject(j).getInteger("ml_peak_cpvalue");
                    elecContract = dbResultArray.getJsonObject(j).getInteger("elec_contract");
                    predValue = (double) dbResultArray.getJsonObject(j).getInteger("pred_value");

                    for (int k=0; k<apiResultDataArray.size(); k++) {
                        if (bldId.equals(apiResultDataArray.getJsonObject(k).getString("bld_id"))) {
                            double value = apiResultDataArray.getJsonObject(k).getDouble("value");
                            double usageDivElecContract = value / elecContract;
                            int mulResult = (int) (usageDivElecContract * 100);

                            boolean isExceed = false;

                            if (andOrCondition.equals("a")) {
                                if (mulResult >= realPeakCpvalue && mulResult >= mlPeakCpvalue) {
                                    isExceed = true;
                                }
                            } else if (andOrCondition.equals("o")) {
                                if (mulResult >= realPeakCpvalue || mulResult >= mlPeakCpvalue) {
                                    isExceed = true;
                                }
                            }

                            int percentage = (int) ((value / elecContract) * 100);

                            logger.debug("notiPeakElectricPower - Start Check Calculation Result");
                            logger.debug("bldId: " + bldId);
                            logger.debug("andOrCondition: " + andOrCondition + ", realPeakCpvalue: " + String.valueOf(realPeakCpvalue) + ", mlPeakCpvalue: " + String.valueOf(mlPeakCpvalue));
                            logger.debug("elecContract: " + String.valueOf(elecContract) + ", predValue: " + String.valueOf(predValue));
                            logger.debug("value: " + String.valueOf(value) + ", usageDivElecContract: " + String.valueOf(usageDivElecContract) + ", mulResult: " + String.valueOf(mulResult));
                            logger.debug("optctlName: " + optctlName + ", isExceed: " + isExceed + ", percentage: " + String.valueOf(percentage));
                            logger.debug("notiPeakElectricPower - End Check Calculation Result");

                            if (isExceed) {
                                String sValue = String.format("%.1f" , value);

                                if (mulResult > 24) {

                                    ArrayList<Object> vData = new ArrayList<>();
                                    vData.add(sValue); // 사용량(=value)

                                    String level = "1";

                                    if (mulResult > 50) {
                                        level = "2";
                                    } else if (mulResult > 85) {
                                        level = "3";
                                    } else if (mulResult > 100) {
                                        level = "4";
                                    }
                                    vData.add(level);

                                    String message = "피크전력이 계약전력의 " + String.valueOf(percentage) + "%에 도달하였습니다. (계약전력: " +  String.valueOf(elecContract)  + "kw, 현재피크전력: " + sValue + "kw)";
                                    vData.add(message);
                                    vData.add("00");

                                    logger.debug("notiPeakElectricPower - Rule Received Start => 전력피크 초과");
                                    logger.debug("bld_id : " + bldId + ", 초과량 : " + String.valueOf(percentage) + "%, 계약전력량 : " + String.valueOf(elecContract) + "kw, 현재피크전력: " + sValue + "kw)");
                                    logger.debug("notiPeakElectricPower - Rule Received End => 전력피크 초과");

                                    bldNotificationData.put(bldId, vData);

                                    System.out.println("notiPeakElectricPower - Start insertBldNotification");
                                    boolean result = insertBldNotification(bldNotificationData, dbOption);
                                    System.out.println("notiPeakElectricPower - End insertBldNotification");

                                    logger.debug("notiPeakElectricPower - insertBldNotification Result: " + result);
                                }

                                NipaQuery nipaQuery = new NipaQuery();

                                String[] params = new String[] {bldId};

                                String query = nipaQuery.getQuery("rule_2_1", params);

                                Connection conn = dbManager.getDBConnection("nisbcp", dbOption);
                                JsonObject jsonDBResult = dbManager.getDBResultForJsonObject(conn, query);

                                JsonArray jsonDataArray = jsonDBResult.getJsonArray("data");

                                int dataSize = jsonDataArray.size();

                                if (dataSize > 0) {
                                    for (int m=0; m<jsonDataArray.size(); m++) {
                                        String selectedBldId = jsonDataArray.getJsonObject(m).getString("bld_id");
                                        String pointId = jsonDataArray.getJsonObject(m).getString("point_id");
                                        String facilName = jsonDataArray.getJsonObject(m).getString("facil_name");
                                        String facilId = jsonDataArray.getJsonObject(m).getString("facil_id");
                                        String measureId = jsonDataArray.getJsonObject(m).getString("measure_id");

                                        logger.debug("notiPeakElectricPower - Start Check ControlPoint Result");
                                        logger.debug("selectedBldId: " + selectedBldId);
                                        logger.debug("pointId: " + pointId + ", measureId: " + measureId);
                                        logger.debug("facilName: " + facilName + ", facilId: " + facilId);
                                        logger.debug("notiPeakElectricPower - End Check ControlPoint Result");

                                        String finalOptctlName = optctlName;
                                        String finalPointId = pointId;
                                        String finalFacilID = facilId;
                                        String finalBldId = bldId;
                                        int finalElecContract = elecContract;
                                        int finalControlCount = m;

                                        System.out.println("finalOptctlName: " + finalOptctlName);
                                        System.out.println("finalPointId: " + finalPointId);
                                        System.out.println("finalFacilId: " + finalFacilID);
                                        System.out.println("finalBldId: " + bldId);
                                        System.out.println("finalElecContract: " + elecContract);
                                        System.out.println("finalControlCount: " + finalControlCount);

                                        JsonObject dataParams = new JsonObject();

                                        dataParams.put("id", pointId);
                                        dataParams.put("v", "0");

                                        HttpClient controlClient = Vertx.vertx().createHttpClient(new HttpClientOptions().setSsl(true).setTrustAll(true).setVerifyHost(false));
                                        String nodePath = "/blds/" + selectedBldId + "/control";

                                        logger.debug("notiPeakElectricPower - Start Send REST API: IF_WEB_HUB_017");
                                        HttpClientRequest controlClientRequest = controlClient.request(HttpMethod.PUT, NISBCP_SERVICEHUB_PORT, url, nodePath, c_result -> {
                                            String controlMessage = "";

                                            if (c_result.statusCode() == 200) {
                                                logger.debug("notiPeakElectricPower IF_WEB_HUB_017 Succeed");

                                                controlMessage = "전력피크제어 \"" + finalOptctlName + "\"이 실행되었습니다. (계약전력: " + finalElecContract + "kw, 현재피크전력: " + sValue + "kw)";
                                            } else {
                                                logger.debug("notiPeakElectricPower IF_WEB_HUB_017 Failed");
                                                logger.debug(c_result.statusMessage());

                                                controlMessage = "전력피크제어 \"" + finalOptctlName + "\"이 실패하였습니다. (계약전력: " + finalElecContract + "kw, 현재피크전력: " + sValue + "kw)";
                                            }

                                            if (finalControlCount == 0) {
                                                Map<String, ArrayList> cpBldNotificationData = new HashMap<>();

                                                ArrayList<Object> vtData = new ArrayList<>();
                                                vtData.add(sValue);
                                                vtData.add('4');

                                                vtData.add(controlMessage);
                                                vtData.add("03");

                                                logger.debug("notiPeakElectricPower - Rule Received Start => 전력피크제어");
                                                logger.debug("bld_id : " + finalBldId + ", 제어명칭 : " + finalOptctlName + ", 제어대상 : " + finalPointId + "계약전력 : " + String.valueOf(finalElecContract) + "kw, 현재피크전력: " + sValue + "kw)");
                                                logger.debug("notiPeakElectricPower - Rule Received End => 전력피크제어");

                                                cpBldNotificationData.put(finalBldId, vtData);

                                                System.out.println("notiPeakElectricPower - Start insertBldControlNotification");
                                                boolean cpResult = insertBldNotification(cpBldNotificationData, dbOption);
                                                System.out.println("notiPeakElectricPower - End insertBldControlNotification");

                                                logger.debug("notiPeakElectricPower - insertBldControlNotification Result: " + cpResult);
                                            }
                                        });
                                        logger.debug("notiPeakElectricPower - End Send REST API: IF_WEB_HUB_017");

                                        controlClientRequest.putHeader(HttpHeaders.AUTHORIZATION, authorization);
                                        controlClientRequest.putHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(dataParams.encode().getBytes().length));
                                        controlClientRequest.putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
                                        controlClientRequest.write(dataParams.encode()).end();
                                    }

                                }
                            }
                        }
                    }
                }

                logger.debug("notiPeakElectricPower - End Bodyhandler");

                client.close();
            });
        });
        logger.debug("notiPeakElectricPower - End Send REST API: IF_WEB_HUB_061");

        clientRequest.putHeader(HttpHeaders.AUTHORIZATION, authorization);
        clientRequest.putHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        clientRequest.end();
    }

    private boolean insertBldNotification(Map<String, ArrayList> notiData, String dbOption) {
        Connection conn = dbManager.getDBConnection("nisbcp", dbOption);

        int i = 0;
        String tailQuery = "";
        String valueTypes = "";

        for (Map.Entry<String, ArrayList> entry : notiData.entrySet()) {
            String key = entry.getKey();
            ArrayList value = entry.getValue();
            valueTypes = value.get(3).toString();

            if (i == notiData.size()-1) {
                tailQuery += "('" + key + "','" + value.get(0) + "','" + value.get(1) + "','" + value.get(2) + "','" + value.get(3) + "'); ";
            } else {
                tailQuery += "('" + key + "','" + value.get(0) + "','" + value.get(1) + "','" + value.get(2) + "','" + value.get(3) + "'), ";
            }

            i++;
        }

        String query = "INSERT INTO T_TEMP_BLD_NOTIFICATION(TARGET_BLD_ID, VALUE, NOTI_LEVEL, MESSAGE, VALUE_TYPE) VALUES " + tailQuery;

        logger.debug("insertBldNotification - Alarm Code(=Value Type): " + valueTypes);
        logger.debug("insertBldNotification - Nipa Query: " + query);

        boolean result = dbManager.insertDB(conn, query);

        logger.debug("insertBldNotification - insertDB Result: " + result);

        return result;
    }
}
