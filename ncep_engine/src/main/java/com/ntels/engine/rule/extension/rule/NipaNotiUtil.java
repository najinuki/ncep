package com.ntels.engine.rule.extension.rule;

import java.time.LocalDateTime;

public class NipaNotiUtil {

    public LocalDateTime getPredDateTime() {
        LocalDateTime peakDateTime = LocalDateTime.now();
        int currentMin = peakDateTime.getMinute();

        if (currentMin < 15) {
            peakDateTime = peakDateTime.withMinute(0).plusMinutes(15);
        } else if  (currentMin < 30) {
            peakDateTime = peakDateTime.withMinute(15).plusMinutes(15);
        } else if  (currentMin < 45) {
            peakDateTime = peakDateTime.withMinute(30).plusMinutes(15);
        } else {
            peakDateTime = peakDateTime.withMinute(45).plusMinutes(15);
        }

        return peakDateTime;
    }
}
