package com.ntels.engine.rule.extension.site.smartcampus;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.wso2.siddhi.annotation.Example;
import org.wso2.siddhi.annotation.Extension;
import org.wso2.siddhi.annotation.Parameter;
import org.wso2.siddhi.annotation.ReturnAttribute;
import org.wso2.siddhi.annotation.util.DataType;
import org.wso2.siddhi.core.config.SiddhiAppContext;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.function.FunctionExecutor;
import org.wso2.siddhi.core.util.config.ConfigReader;
import org.wso2.siddhi.query.api.definition.Attribute;
import org.wso2.siddhi.query.api.exception.SiddhiAppValidationException;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Extension(
        name = "timeComp",
        namespace = "ntels",
        description = "주어진 날짜 기준으로 요일 비교",
        parameters = {
                @Parameter(name = "start.time",
                        description = "비교시작시간(hhmmss)-> UTC시간 기준",
                        type = {DataType.STRING}),
                @Parameter(name = "end.time",
                        description = "비교종료시간(hhmmss)-> UTC시간 기준",
                        type = {DataType.STRING}),
                @Parameter(name = "zone.id",
                        description = "zoneId(Asia/Seoul or Asia/Jakarta)",
                        type = {DataType.STRING})

        },
        returnAttributes = @ReturnAttribute(
                description = "True or False",
                type = {DataType.STRING}),
        examples = {
                @Example(
                        syntax = "from fooStream\n" +
                                 "select ntels:timeComp('090000', '180000', 'Asia/Jakarta') as rslt\n" +
                                 "insert into output;",
                        description = "주어진 날짜 기준으로 시간 비교")
        }
)
public class TimeCompare extends FunctionExecutor {

    private CloseableHttpClient httpclient = null;
    private static Logger logger = Logger.getLogger(TimeCompare.class);

    @Override
    protected void init(ExpressionExecutor[] attributeExpressionExecutors, ConfigReader configReader,
                        SiddhiAppContext siddhiAppContext) {
        logger.debug("timeCompare init");

        httpclient = HttpClients.custom().build();

        if (attributeExpressionExecutors.length < 1) {
            throw new SiddhiAppValidationException("필수 파라메터 오류(stationName을 입력해 주세요.) : " + attributeExpressionExecutors.length);
        }
    }

    @Override
    protected Object execute(Object[] data) {
        logger.debug("timeCompare execute-1");

        String startTime = (String) data[0];
        String endTime = (String) data[1];
        String zoneId = (String) data[2];

        String result;

        LocalDateTime nowLtime = LocalDateTime.now();
        ZonedDateTime utcNowLTime = ZonedDateTime.of(nowLtime, ZoneId.of("UTC"));
        ZonedDateTime chgNowLTime = ZonedDateTime.ofInstant(utcNowLTime.toInstant(), ZoneId.of(zoneId));

        LocalDateTime ldt = chgNowLTime.toLocalDateTime();
        LocalDateTime sdt = LocalDateTime.of(ldt.toLocalDate(), LocalTime.parse(startTime, DateTimeFormatter.ofPattern("HHmmss")));
        LocalDateTime edt = LocalDateTime.of(ldt.toLocalDate(), LocalTime.parse(endTime, DateTimeFormatter.ofPattern("HHmmss")));

        logger.debug("---------- TimeCompare Info ----------");
        logger.debug("prev ldt: " + ldt);
        logger.debug("prev sdt: " + sdt);
        logger.debug("prev edt: " + edt);
        logger.debug("----------------------------------------");

        if (sdt.isAfter(edt)) {
            edt = edt.plusDays(1);
        }

        logger.debug("---------- TimeCompare Info ----------");
        logger.debug("ldt: " + ldt);
        logger.debug("sdt: " + sdt);
        logger.debug("edt: " + edt);
        logger.debug("----------------------------------------");

        if (ldt.isBefore(edt) && ldt.isAfter(sdt)) {
            result = "True";
        } else {
            result = "False";
        }

        return result;
    }

    @Override
    protected Object execute(Object data) {
        logger.debug("timeCompare execute-2");

        return "False";

    }

    @Override
    public Attribute.Type getReturnType() {
        return Attribute.Type.STRING;
    }

    @Override
    public Map<String, Object> currentState() {
        return null;    //No need to maintain a state.
    }

    @Override
    public void restoreState(Map<String, Object> state) {
        //Since there's no need to maintain a state, nothing needs to be done here.
    }
}
