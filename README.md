# ncep

N-CEP Engine


### 1. cep 란?
CEP란 Complex Event Processing의 약자로 초당 수십~수백만건의 이벤트(데이터)가 발생하는 시스템에서 실시간으로 사용자에게 유의미한 정보를 검출하는데 사용

<img src="./image/cep_flow.png" width="638" height="156">

#### Input(입력)
- 외부 시스템과 통신하기 위한 다양한 프로토콜을 지원하는 서버
- 외부 시스템에서 들어오는 패킷을 처리하는 Input 모듈(REST, Apache Kafka 지원)과
  외부 시스템으로 전송하는 Output 모듈(E-Mail, SMS, APP Push 지원)로 구성

#### Query(쿼리)
- 외부 시스템으로 부터 오는 데이터에 대한 적재 및 모니터링 기능 제공
- Rule 체크 및 외부 시스템 전송 여부 확인
- Rule에 대한 등록/처리/삭제 기능 제공

#### Output(출력)
- Event 실시간 조회 및 패턴 검색 기능 제공
- Rule/Event/Action에 대한 등록/수정/삭제 


### 2. cep 활용 예제
1) 네트워크 모니터링
- 작동 중이지 않는 서버가 존재하면 관리자에게 통보
2) 온라인 주문 실시간 통계
- 상품별 주문 Top 10 / 지역별 주문 비율 등
3) 보안 위협 감지
- 비밀번호 변경을 3번 연속 실패 / 로그인을 5번 연속 실패 등
4) 위치 추적 / 제한
- GPS 좌표 값을 적재해서 특정 위치에 도착했을 경우 통보
5) 유통/제조 재품 재고
- 제품 재고 부족 시 재고 부족 알림
6) 부정 사용
- 동일 장소 / 시간에 수차례 연속적인 카드 사용할 경우 통보


### 3. 적용 오픈소스 및 기술
1) CEP Engine
- Java 1.8
- [WSO2 Siddhi](https://siddhi.io/en/v4.x/docs/)
- [Vert.x](https://vertx.io/)
- [Apache Ignite](https://ignite.apache.org/)
- [Docker, Dokcer Compose](https://www.docker.com/)

2) CEP IO
- Java 1.8
- [Spring 5.0, Spring Boot, Spring WebFlux](https://spring.io)
- [Docker, Dokcer Compose](https://www.docker.com/)

3) CEP Web
- Java 1.8
- JQuery
- [Spring 5.0, Spring Boot, Spring MVC](https://spring.io)
- [Docker, Dokcer Compose](https://www.docker.com/)

### 4. 소프트웨어 아키텍처
<img src="./image/cep_architecture.png" width="794" height="516">

### 5. 활용 예제
- [SmartCampus](https://gitlab.com/najinuki/ncep/-/wiki_pages/Rule-List-For-Smartcampus)
- [SmartBuilding](https://gitlab.com/najinuki/ncep/-/wiki_pages/Rule-list-for-nisbcp-nipa)

