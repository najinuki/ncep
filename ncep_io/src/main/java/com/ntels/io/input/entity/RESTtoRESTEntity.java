package com.ntels.io.input.entity;

public class RESTtoRESTEntity {
    String event_name;
    String event_descp;
    String init_value;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_descp() {
        return event_descp;
    }

    public void setEvent_descp(String event_descp) {
        this.event_descp = event_descp;
    }

    public String getInit_value() {
        return init_value;
    }

    public void setInit_value(String init_value) {
        this.init_value = init_value;
    }
}
