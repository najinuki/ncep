package com.ntels.io.input.controller;

import com.ntels.io.common.exception.ErrorCode;
import com.ntels.io.common.exception.NCEPIOException;
import com.ntels.io.common.util.RedisUtil;
import com.ntels.io.common.util.RestUtil;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@RestController
public class RESTInController {
	private static final Logger logger = LoggerFactory.getLogger(RESTInController.class);

	@Autowired
	private RestUtil restUtil;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private ReactiveRedisTemplate redis;

	@Value("${ncep.engine.url}")
	private String ENGINE_URL;

	@Value("${ncep.engine.port}")
	private String ENGINE_PORT;

	/**
	 * R.4.1 Event 전송
	 *
	 * @param data
	 * @return
	 */
	@PostMapping("/send/event")
    public void sendEvent(@RequestBody JSONObject data) throws Exception {
		logger.debug("sendEvent started >>>" + data);

		String event_name =  (String) data.get("event_name");
		String event_type =  (String) data.get("event_type");
		Map<String, Object> event =  (HashMap) data.get("event");

    	if(event_name==null || event_name.equals("")) {
			throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "event_name" + " is Required.");
		}

		if(event.isEmpty()) {
			throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "event" + " is Required.");
		}

		Map<String, Object> cpEvent = event;
		if ("home".equals(event_type)) {
			String content_value = (String) cpEvent.get("content_value");

			if (content_value != null) {
				cpEvent.put("content_value", new String(Base64.getDecoder().decode(content_value)));
			}
		}

		// Redis
		Map<String, Object> redisObj = cpEvent;
		redisObj.put("event_name", event_name);
		redisObj.put("event_type", event_type);
		String key = "event";
		//Mono<Long> redisResult = redisUtil.putEventInfo(key, redisObj);

		// REST
		JSONObject obj = new JSONObject();
    	obj.put("event_name", event_name);
    	obj.put("event", cpEvent);
		String path = "/send/event";
    	String url = ENGINE_URL+":"+ENGINE_PORT;

/**/
		HashMap header = new HashMap();
		//Mono<String> restResult = restUtil.sendToServer("post", header, url, path, obj.toJSONString());
		restUtil.sendToEngine("post", header, url, path, obj.toJSONString());
		//Mono<Tuple2<Long, String>> tupleResult = Mono.zip(redisResult, restResult);

		//return restResult;
    }

}
