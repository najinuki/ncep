package com.ntels.io.input.controller;
import com.ntels.io.common.exception.ErrorCode;
import com.ntels.io.common.exception.NCEPIOException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;

/**
 *
 */

public class KafkaInController {
    private static final Logger logger = LoggerFactory.getLogger(KafkaInController.class);

    @PostMapping("/send/event")
    public Mono<Void> sendEvent(@RequestBody JSONObject data) throws Exception{
        logger.debug("sendEvent started >>>" + data);

        String event_name =  (String) data.get("event_name");
        String event = (String) data.get("event");

        if(event_name==null || event_name.equals("")) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "event_name" + " is Required.");
        }

        if(event==null || data.get("event").equals("")) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "event" + " is Required.");
        }

        return Mono.empty();
    }
}
