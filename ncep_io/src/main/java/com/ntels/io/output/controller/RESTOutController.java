package com.ntels.io.output.controller;

import com.ntels.io.common.exception.ErrorCode;
import com.ntels.io.common.exception.NCEPIOException;
import com.ntels.io.common.util.DBUtil;
import com.ntels.io.common.util.EmailUtil;
import com.ntels.io.common.util.KakaoUtil;
import com.ntels.io.common.util.RestUtil;
import com.ntels.io.output.dao.RuleAddDao;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class RESTOutController {
    private static final Logger logger = LoggerFactory.getLogger(RESTOutController.class);

    @Autowired
    private RestUtil restUtil;

    @Autowired
    private EmailUtil emailUtil;

    @Autowired
    private DBUtil dbUtil;

    @Autowired
    private KakaoUtil kakaoUtil;

    /**
     * R.4.2 Action 전송
     *
     * @param data
     * @return
     */
    @PostMapping("/send/action")
    public Mono<Void> sendAction (@RequestBody JSONObject data) throws Exception {
        logger.debug("sendAction started>>> " + data);

        String rule_id = (String) data.get("rule_id");
        String send_type = (String) data.get("send_type");
        Map<String, Object> send_info =  (HashMap) data.get("send_info");
        String send_content = (String) data.get("send_content");

        if(rule_id==null || rule_id.equals("")) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "rule_id" + " is Required.");
        }

        if(send_type==null || send_type.equals("")) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "send_type" + " is Required.");
        }

        if(send_info.isEmpty()) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "send_info" + " is Required.");
        }

        if(send_content==null || send_content.equals("")) {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "send_content" + " is Required.");
        }

        if(send_type.equals("E-Mail")) {
            String to = (String) send_info.get("to");
            String title = (String) send_info.get("title");
            emailUtil.sendEmail(to, title, send_content);

        } else if(send_type.equals("REST")) {
            String url = (String) send_info.get("url");
            String port = (String) send_info.get("port");
            String path = (String) send_info.get("path");
            String method = (String) send_info.get("method");
            HashMap<String, String> header = (HashMap) send_info.get("header");

            String externalURL = url + ":" + port;
            restUtil.sendToServer(method, header, externalURL, path, send_content);

        } else if(send_type.equals("DB")) {
            dbUtil.updateTable(send_info, send_content);

        } else if(send_type.equals("KAKAO")) {

            String title = (String) send_info.get("title");
            String auth_info = (String) send_info.get("auth_info");

            String description = send_content;

            kakaoUtil.sendToMessage(title, auth_info, description);

        } else {
            throw new NCEPIOException(ErrorCode.MANDATORY_PARAMETER_ERROR, "send_type" + " need to check (E-Mail, REST)" );
        }

        return Mono.empty();
    }
}
