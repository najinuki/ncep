package com.ntels.io.output.dao;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public class RuleAddDao {
    private static final Logger logger = LoggerFactory.getLogger(RuleAddDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insertRuleQuery(JSONObject param) {
        String rule_query = (String) param.get("send_content");
        jdbcTemplate.update(rule_query);

    }
}
