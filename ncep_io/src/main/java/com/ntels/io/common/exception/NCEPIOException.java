package com.ntels.io.common.exception;

public class NCEPIOException extends RuntimeException {

    private final int errorCode;

    public NCEPIOException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMsg() {
        String message = this.getMessage();
        return "(" + this.errorCode + ") " + (message != null ? message : "");
    }

}
