package com.ntels.io.common.exception;

public class ErrorCode {
    public static final int MANDATORY_PARAMETER_ERROR = 10;

    public static final String SERVER_ERROR_MSG = "A server error has occurred.";
    public static final String MANDATORY_PARAMETER_ERROR_MSG = "Mandatory parameter ";
}
