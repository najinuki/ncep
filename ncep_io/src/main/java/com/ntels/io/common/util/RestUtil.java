package com.ntels.io.common.util;

import com.ntels.io.common.exception.NCEPIOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Component
public class RestUtil {
    private Logger logger =LoggerFactory.getLogger(RestUtil.class);

    public Mono<String> sendToServer (String method, HashMap header, String url, String path, String param) throws Exception {
        Mono<String> result = null;

        String authorization = (String) header.get("Authorization");

        WebClient client = WebClient.create(url);
        if (authorization == null || "".equals(authorization)) {
            logger.debug("1 authorization : " + authorization + ", url : " + url);
            result = client.post().uri(path).contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromObject(param)).retrieve()
                    .bodyToMono(String.class);
        } else {
            logger.debug("2 authorization : " + authorization + ", url : " + url);
            result = client.post().uri(path).contentType(MediaType.APPLICATION_JSON).header("Authorization", authorization)
                    .body(BodyInserters.fromObject(param)).retrieve()
                    .bodyToMono(String.class);
        }

        System.out.println("wekekekekssss");

        result.subscribe(this::handleResponse);

        return result;
    }

    public void sendToEngine(String method, HashMap header, String url, String path, String param) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(param,headers);

        try {
            System.out.println("send : " + url + ", " + path);

            String map = restTemplate.postForObject(url + path, entity, String.class);
            System.out.println("send22!! : " + map);

        } catch (HttpClientErrorException e) {
            e.printStackTrace();
        }
    }

    private void handleResponse(String st) {
        logger.debug("----------------------- REST Response -----------------------");
        logger.debug("Response Body : " + st);
        logger.debug("------------------------------------------------------------");
    }

}
