package com.ntels.io.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import java.util.Map;

/**
 *
 */
@Component
public class RedisUtil {
    private static final Logger logger = LoggerFactory.getLogger(RedisUtil.class);
    public static final String REDIS_KEY_AUTH_TOKEN = "NCEP_REDIS_KEY";

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ReactiveRedisTemplate redis;

    public Mono<Long> getEventInfo(String key) {
        logger.debug("start RedisUtil getEventInfo : " + key);
        return redis.opsForList().size("event");
    }

    public Mono<Long> putEventInfo(String key, Map<String, Object> param) {
        logger.debug("start RedisUtil putEventInfo [{}], [{}] " , key, param);
        return redis.opsForList().leftPush(key, param);
    }

    public void deleteEventInfo(String key) {
        redisTemplate.opsForHash().delete(REDIS_KEY_AUTH_TOKEN, key);
    }

}
