package com.ntels.io.common.util;

import org.json.simple.JSONArray;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Map;

@Component
public class DBUtil {

    public boolean updateTable(Map connInfo, String query) {
        Connection conn = null;
        boolean result = false;

        try {
            String url = (String) connInfo.get("conn_url");
            String id = (String) connInfo.get("conn_id");
            String pw = (String) connInfo.get("conn_pw");
            String port = (String) connInfo.get("conn_port");
            String db = (String) connInfo.get("conn_db");
            db = db + "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC";

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + url + ":" + port + "/" + db , id, pw);
            PreparedStatement stmt = conn.prepareStatement(query);
            result = stmt.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception e) {}
        }

        return result;
    }

}
