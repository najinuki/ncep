package com.ntels.io.common.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 *  카카오 관련 인증 및 메시지 API Util
 *  https://developers.kakao.com/docs/restapi/user-management
 */

@Component
public class KakaoUtil {
    private Logger logger = LoggerFactory.getLogger(KakaoUtil.class);

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private RedisUtil redisUtil;

    @Value("${kakao.rest.appkey}")
    private String KAKAO_REST_APPKEY;

    @Value("${kakao.redirect.uri}")
    private String KAKAO_REDIRECT_URI;

    @Value("${kakao.auth.uri}")
    private String KAKAO_AUTH_URI;

    @Value("${kakao.api.uri}")
    private String KAKAO_API_URI;

    @Value("${kakao.auth.code.path}")
    private String KAKAO_AUTH_CODE_PATH;

    @Value("${kakao.auth.token.path}")
    private String KAKAO_AUTH_TOKEN_PATH;

    @Value("${kakao.api.token.validation.path}")
    private String KAKAO_API_TOKEN_VALIDATION_PATH;

    @Value("${kakao.api.message.path}")
    private String KAKAO_API_MESSAGE_PATH;

    public KakaoUtil () {
        // https://devtalk.kakao.com/t/api/12313/3 참고
        // Spring에서 제공하는 RestTemplate에서는 기본컨버터가 포함되어있지 않음.
        // Kakao API 사용시 아래와 같이 추가하지 않으면 API를 호출해도 NullPointerException 발생.
        // 한글깨짐방지를 위함.
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
    }

    /**
     * 로그인 코드받기 API
     *
     * @return ResponseEntity<String>
     */
    public ResponseEntity<String>  getAuthorizeCode() {

        String url = KAKAO_AUTH_URI + KAKAO_AUTH_CODE_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("client_id", KAKAO_REST_APPKEY)
                .queryParam("redirect_uri", KAKAO_REDIRECT_URI)
                .queryParam("response_type", "code");

        URI build_url = builder.build().encode().toUri();

        ResponseEntity<String> response = restTemplate.exchange(build_url, HttpMethod.GET, new HttpEntity(headers), String.class);

        return response;
    }

    /**
     * 사용자 토큰받기 API
     * @param authorize_code: 카카오 API쪽에 Token관련정보를 얻기위해 필요
     *                        코드받기 API를 이용해 return된 authorize_code를 사용
     *
     * @return ResponseEntity<String>
     */
    public ResponseEntity<String> getToken(String authorize_code) {

        String url = KAKAO_AUTH_URI + KAKAO_AUTH_TOKEN_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("grant_type", "authorization_code")
                .queryParam("client_id", KAKAO_REST_APPKEY)
                .queryParam("redirect_uri", KAKAO_REDIRECT_URI)
                .queryParam("code", authorize_code);

        URI build_url = builder.build().encode().toUri();

        ResponseEntity<String> response = restTemplate.exchange(build_url, HttpMethod.POST, new HttpEntity(headers), String.class);

        return response;
    }

    /**
     * 사용자 토큰갱신 API
     * @param refresh_token: 카카오 API쪽에 Token정보를 갱신하기 위해 필요
     *                       사용자 토큰 받기 API를 이용해 return된 refresh_token을 사용
     *
     * @description: access_token은 발급 받은 후 12시간-24시간(정책에 따라 변동 가능)동안 유효합니다.
     *               refresh token은 한달간 유효하며, refresh token 만료가 1주일 이내로 남은 시점에서
     *               사용자 토큰 갱신 요청을 하면 갱신된 access token과 갱신된 refresh token이 함께 반환됩니다.
     *
     * @return ResponseEntity<String>
     */
    public ResponseEntity<String> refreshToken(String refresh_token) {
        ResponseEntity<String> response = null;

        String url = KAKAO_AUTH_URI + KAKAO_AUTH_TOKEN_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("grant_type", "refresh_token")
                .queryParam("client_id", KAKAO_REST_APPKEY)
                .queryParam("refresh_token", refresh_token);

        URI build_url = builder.build().encode().toUri();

        logger.debug("---------- Refresh Token Info ----------");
        logger.debug("Final Url : " + build_url);
        logger.debug("----------------------------------------");

        try {
            response = restTemplate.exchange(build_url, HttpMethod.POST, new HttpEntity(headers), String.class);
        } catch (HttpClientErrorException exception) {
            System.out.println(exception.getStatusCode());
            System.out.println(exception.getMessage());
        }

        logger.debug("--------- Refresh Token REST Response -------------");
        logger.debug("Status Code : " + response.getStatusCode());
        logger.debug("Response Body : " + response.getBody());
        logger.debug("-------------------------------------------------------");

        return response;
    }

    /**
     * 사용자 토큰 유효성 검사 및 정보 얻기 API
     *
     * @return HttpStatus
     */
    public HttpStatus validateToken(String authorization) {

        String url = KAKAO_API_URI + KAKAO_API_TOKEN_VALIDATION_PATH;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("Authorization", authorization);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity(headers), String.class);

        return response.getStatusCode();
    }

    /**
     * 나에게 메시지 보내기 API
     *
     * @param title: 카카오톡 메시지 템플릿 내용에 들어갈 제목
     * @param description: 카카오톡 메시지 템플릿 내용에 들어갈 설명
     * @return restTemplate
     */
    public RestTemplate sendToMessage(String title, String auth_info, String description) {
        logger.debug("started sendToServer title = [{}], auth_info = [{}], description = [{}],",
                     title, auth_info, description);

        ResponseEntity<String> response = null;

        String url = KAKAO_API_URI + KAKAO_API_MESSAGE_PATH;

        JSONObject authInfoObj = getAuthInfo(auth_info);

        String authorization = authInfoObj.get("token_type") + " " + authInfoObj.get("access_token");

        String template_id = "11907"; // 고정

        // description 내용에 들어있는 html 태그 제거
        String split_description = description.replaceAll("\\<.*?>","");

        JSONObject template_args = new JSONObject();
        template_args.put("title", title);
        template_args.put("description", split_description);

        logger.debug("---------- sendToMessage REST Request ----------");
        logger.debug("URL : " + url);
        logger.debug("Authorization : " + authorization);
        logger.debug("-------------------------------------------------");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", authorization);

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("template_id", template_id)
                .queryParam("template_args", template_args.toJSONString());

        URI build_url = builder.build().encode().toUri();

        logger.debug("------------- sendToMessage Info -------------");
        logger.debug("Final Url : " + build_url);
        logger.debug("Headers : " + headers.toString());
        logger.debug("----------------------------------------------");
        try {

            response = restTemplate.exchange(build_url, HttpMethod.POST, new HttpEntity(headers), String.class);
        } catch (HttpClientErrorException exception) {

            // 토큰관련 에러
            // 400: 잘못된 토큰
            // 401: 토큰유효기간 만료
            if (exception.getStatusCode() == HttpStatus.UNAUTHORIZED ||
                exception.getStatusCode() == HttpStatus.BAD_REQUEST) {

                String refresh_token = (String) authInfoObj.get("refresh_token");

                System.out.println("========== previous refresh_token ==========");
                System.out.println(authInfoObj.get("refresh_token"));
                System.out.println("============================================");

                response = refreshToken(refresh_token);

                // 요청 결과로 갱신된 새로운 access_token과 해당 토큰의 만료 시간, 또한 Refresh Token 자체가 갱신되었으면 갱신된 refresh_token도 함께 받게 됩니다.
                // refresh_token은 응답에 포함될 수도 있고 포함되지 않을 수도 있기 때문에 포함된 경우에 대해서만 갱신하여 저장
                String new_auth_info = response.getBody();

                System.out.println("========== new refresh_token ==========");
                System.out.println("new access_token: " + getAuthInfo(response.getBody()).get("access_token"));
                System.out.println("new refresh_token: " + getAuthInfo(response.getBody()).get("refresh_token"));
                System.out.println("=======================================");

                // resend for demo.
                sendToMessage(title, new_auth_info, description);
            }
        }

        logger.debug("------------- sendToMessage REST Response -------------");
        logger.debug("Status Code : " + response.getStatusCode());
        logger.debug("Response Body : " + response.getBody());
        logger.debug("-------------------------------------------------------");

        return restTemplate;
    }

    /**
     * From string to JSONObject for Kakao Authorization.
     *
     * @param auth_info: 카카오 사용자계정 연결정보
     * @return JSONObject
     */
    public JSONObject getAuthInfo(String auth_info) {
        JSONParser parser = new JSONParser();

        Object convertObj = null;
        try {
            convertObj = parser.parse(auth_info);
        } catch (ParseException e) {

            logger.debug("------------------- Json String Parse ------------------");
            logger.debug(e.getMessage());
            logger.debug("--------------------------------------------------------");
        }
        JSONObject authInfoObj = (JSONObject) convertObj;

        System.out.println("========== Start authInfoObj ==========");
        System.out.println("access_token : " + authInfoObj.get("access_token"));
        System.out.println("token_type : " + authInfoObj.get("token_type"));
        System.out.println("refresh_token : " + authInfoObj.get("refresh_token"));
        System.out.println("expires_in : " + authInfoObj.get("expires_in"));
        System.out.println("scope : " + authInfoObj.get("scope"));
        System.out.println("========== End authInfoObj ============");

        return authInfoObj;
    }
}