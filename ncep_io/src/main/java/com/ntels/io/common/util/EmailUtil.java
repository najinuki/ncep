package com.ntels.io.common.util;

import com.ntels.io.output.controller.RESTOutController;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.mail.internet.MimeMessage;

/**
 *
 */
@Component
public class EmailUtil {
    private static final Logger logger = LoggerFactory.getLogger(RESTOutController.class);

    @Autowired
    public JavaMailSender emailSender;

    public Mono<JSONObject> sendEmail (String to, String subject, String text) throws Exception {
        logger.debug("started sendEmail");

        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "UTF-8");

        mimeMessage.setContent(text, "text/html; charset=UTF-8");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setFrom("ncepEngine@gmail.com");
        emailSender.send(mimeMessage);

        return Mono.empty();

    }
}
