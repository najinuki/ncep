package com.ntels.io.configuration;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import reactor.core.publisher.Flux;

/**
 *
 */
@Configuration

public class RedisConfig {
    @Value("${ncep.redis.host}")
    private String redisHostName;

    @Value("${ncep.redis.port}")
    private int redisHostPort;

    @Value("${ncep.redis.auth}")
    private String redisHostAuth;

    @Bean
    public ReactiveRedisConnectionFactory lettuceConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisHostName);
        redisStandaloneConfiguration.setPort(redisHostPort);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisHostAuth));

        return new LettuceConnectionFactory(redisStandaloneConfiguration);
    }

    @Bean
    ReactiveRedisTemplate<String, Object> reactiveRedisTemplate() {
        RedisSerializer<String> serializer = new StringRedisSerializer();
        RedisSerializer<Object> hashSerializer = new GenericJackson2JsonRedisSerializer();
        RedisSerializationContext<String , Object> serializationContext = RedisSerializationContext
                .<String, Object>newSerializationContext()
                .key(serializer)
                .value(hashSerializer)
                .hashKey(serializer)
                .hashValue(hashSerializer)
                .build();
        return new ReactiveRedisTemplate<>(lettuceConnectionFactory(), serializationContext);
    }


/*    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisHostName);
        redisStandaloneConfiguration.setPort(redisHostPort);
        redisStandaloneConfiguration.setPassword(RedisPassword.of(redisHostAuth));
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }

    @Bean(name="redisTemplate")
    public RedisTemplate redisTemplateConfig() {
        RedisTemplate redisTemplate = new RedisTemplate();

        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
        redisTemplate.setConnectionFactory(jedisConnectionFactory());

        return redisTemplate;
    }*/
}
