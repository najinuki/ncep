package com.ntels.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
public class NCEPIOApplication {

	public static void main(String[] args) {
		SpringApplication.run(NCEPIOApplication.class, args);
	}
}
