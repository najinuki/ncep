﻿/*
Created: 2018-02-20
Modified: 2018-08-03
Model: MySQL 5.7
Database: MySQL 5.7
*/


-- Create tables section -------------------------------------------------

-- Table ncep_service.input_event

CREATE TABLE `ncep_service`.`input_event`
(
  `event_id` Bigint NOT NULL,
  `event_name` Varchar(200) NOT NULL,
  `event_name_order` Varchar(1000) NOT NULL,
  `event_descp` Varchar(1000),
  `event_type` Varchar(2) NOT NULL
 COMMENT '10 : 일반 Stream
11 : 초기 데이터가 존재하는 Stream
12 : 외부 Table',
  `init_value_yn` Char(1) NOT NULL
 COMMENT 'Y or N',
  `init_value` Varchar(10000),
  `event_query` Varchar(1000) NOT NULL,
  `insert_date` Datetime NOT NULL,
  `update_date` Datetime NOT NULL
)
;

ALTER TABLE `ncep_service`.`input_event` ADD PRIMARY KEY (`event_id`)
;

-- Table ncep_service.rule

CREATE TABLE `ncep_service`.`rule`
(
  `rule_id` Bigint NOT NULL,
  `rule_name` Varchar(200) NOT NULL,
  `rule_descp` Varchar(1000),
  `rule_query` Varchar(3000) NOT NULL,
  `callback_name` Varchar(50) NOT NULL,
  `send_type` Varchar(10) NOT NULL,
  `send_info` Varchar(1000) NOT NULL,
  `send_content` Varchar(4000) NOT NULL,
  `send_method` Varchar(2) NOT NULL
 COMMENT '11: 매번, 12: 최초 1회, 13: 시간당 1회, 14: 하루에 1회',
  `last_send_time` Datetime,
  `insert_date` Datetime NOT NULL,
  `update_date` Datetime NOT NULL
)
;

ALTER TABLE `ncep_service`.`rule` ADD PRIMARY KEY (`rule_id`)
;

-- Table ncep_service.rule_input_event

CREATE TABLE `ncep_service`.`rule_input_event`
(
  `rule_input_event_id` Bigint NOT NULL,
  `rule_id` Bigint NOT NULL,
  `event_id` Bigint NOT NULL
)
;

ALTER TABLE `ncep_service`.`rule_input_event` ADD PRIMARY KEY (`rule_id`,`event_id`,`rule_input_event_id`)
;

-- Create foreign keys (relationships) section -------------------------------------------------


ALTER TABLE `ncep_service`.`rule_input_event` ADD CONSTRAINT `Relationship2` FOREIGN KEY (`event_id`) REFERENCES `ncep_service`.`input_event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;


ALTER TABLE `ncep_service`.`rule_input_event` ADD CONSTRAINT `Relationship1` FOREIGN KEY (`rule_id`) REFERENCES `ncep_service`.`rule` (`rule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
;


CREATE TABLE `ncep_service`.`rule_history`
(
  `hist_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rule_id` bigint(20) NOT NULL,
  `rule_name` varchar(200) DEFAULT NULL,
  `rule_descp` varchar(1000) DEFAULT NULL,
  `send_content` varchar(4000) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`hist_id`)
)
;

CREATE TABLE `ncep_service`.`event_history` (
  `hist_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(200) DEFAULT NULL,
  `event_type` varchar(50) DEFAULT NULL,
  `event` varchar(8000) NOT NULL,
  `insert_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`hist_id`)
)
;