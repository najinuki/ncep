package com.ntels.web.controller;

import com.ntels.web.common.util.ElasticUtil;
import com.ntels.web.common.util.RestTempletUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Input Event Controller Class
 */
@Controller
public class InputEventController {

    private static Logger logger = LoggerFactory.getLogger(InputEventController.class);

    @Autowired
    private RestTempletUtil restTempletUtil;

    @Autowired
    private ElasticUtil elasticUtil;

    @RequestMapping(value = "/eventRegister")
    public String eventRegister() {
        return "eventRegister";
    }

    /**
     * R.1.1 Input Event 등록
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/input/event")
    @ResponseBody
    public JSONObject inputEvent(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.1.1 Input Event 등록 Start >>> " + param);

        JSONObject obj = new JSONObject();
        obj.put("event_name", param.get("event_name"));
        obj.put("event_descp", param.get("event_descp"));
        obj.put("event_type", param.get("event_type"));
        obj.put("init_value_yn", param.get("init_value_yn"));
        obj.put("event_query", param.get("event_query"));

        String event_type = (String) param.get("event_type");
        if ("12".equals(event_type)) {
            String crontab = (String) param.get("crontab");

            boolean result = CronExpression.isValidExpression(crontab);
            System.out.println(">>> cron result : " + result);
            if (result == false) {
                JSONObject resultObj = new JSONObject();
                resultObj.put("result", false);
                resultObj.put("msg", "cron expression is invalid.");

                return resultObj;
            }
        }


        String init_format = (String) param.get("init_value_format");
        if(param.get("init_value_yn").equals("Y") && (init_format.equals("json") || init_format.equals("db"))) { // JSON
            String init_value_str = (String) param.get("init_value");
            JSONParser jsonParser = new JSONParser();
            JSONArray array = (JSONArray) jsonParser.parse(init_value_str);

            ArrayList attrArray = (ArrayList) param.get("attr_name");
            ArrayList typeArray = (ArrayList) param.get("data_type");

            LinkedHashMap attrType = new LinkedHashMap();
            for (int i = 0; i < attrArray.size(); i++) {
                String key = (String) attrArray.get(i);
                String value = (String) typeArray.get(i);

                attrType.put(key, value);
            }

            JSONArray newArr = new JSONArray();
            for (int i = 0; i < array.size(); i++) {
                JSONObject eachObj = (JSONObject) array.get(i);

                if(eachObj.size() != attrArray.size()) {
                    logger.debug("Error(Size check) >> init length : " + eachObj.size() + "attr length : " + attrArray.size());
                    throw new Exception("length");

                } else {
                    Iterator<String> keys = eachObj.keySet().iterator();

                    JSONObject newObj = new JSONObject();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        String dataType = (String) attrType.get(key);
                        if (dataType == null) {
                            logger.debug("Error(Key name check) >> init key : " + key + " attrName : " + attrType.get(key));
                            throw new Exception("attr name");
                        } else {
                            String eachType = String.valueOf(eachObj.get(key));

                            if ("double".equals(dataType)) {
                                newObj.put(key, Double.parseDouble(eachType));
                            } else if ("float".equals(dataType)) {
                                newObj.put(key, Float.parseFloat(eachType));
                            } else if ("int".equals(dataType)) {
                                newObj.put(key, Integer.parseInt(eachType));
                            } else if ("long".equals(dataType)) {
                                newObj.put(key, Long.parseLong(eachType));
                            } else {
                                newObj.put(key, eachType);
                            }
                        }
                    }

                    logger.debug(">>>> newObj : " + newObj.toJSONString());
                    newArr.add(newObj);
                }
            }
            logger.debug("new array : " + newArr);
            obj.put("init_value", newArr);
        }

        List attrNameList = (List) param.get("attr_name");
        org.json.JSONArray eventNameOrder = new org.json.JSONArray(attrNameList);
        obj.put("event_name_order", eventNameOrder);

        String path = "/input/event";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, obj, null);

        return resultObj;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/input/event/excel")
    @ResponseBody
    public JSONObject excelUpload(MultipartHttpServletRequest multiPart) throws Exception {
        Iterator<String> files = multiPart.getFileNames();
        String event_name = multiPart.getParameter("event_name");
        String event_descp = multiPart.getParameter("event_descp");
        String event_type = multiPart.getParameter("event_type");
        String init_value_yn = multiPart.getParameter("init_value_yn");
        String event_query = multiPart.getParameter("event_query");
        String attr_array = multiPart.getParameter("attr_name");
        String[] attr_name = attr_array.split(",");
        String data_array = multiPart.getParameter("data_type");
        logger.debug(data_array);
        String[] data_type = data_array.split(",");

        JSONArray attrArr = new JSONArray();
        JSONArray array = new JSONArray();
        for(String name:attr_name) {
            array.add(name);
        }
        JSONArray typeArray = new JSONArray();
        for(String type:data_type) {
            typeArray.add(type);
        }

        JSONObject obj = new JSONObject();
        obj.put("event_name", event_name);
        obj.put("event_descp", event_descp);
        obj.put("event_type", event_type);
        obj.put("init_value_yn", init_value_yn);
        obj.put("event_query", event_query);
        logger.debug("param : " + obj);

        while (files.hasNext()) {
            String uploadFile = files.next();
            MultipartFile mFile = multiPart.getFile(uploadFile);
            String fileName = mFile.getOriginalFilename();

            File convFile = File.createTempFile(mFile.getOriginalFilename(), "");
            mFile.transferTo(convFile);

            Workbook wb;

            if (fileName.endsWith(".xls")) {
                wb = new HSSFWorkbook(new FileInputStream(convFile));
            } else if (fileName.endsWith(".xlsx")) {
                wb = new XSSFWorkbook(new FileInputStream(convFile));
            } else {
                throw new IllegalAccessError("check .xls or .xlsx");
            }
            FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

            Sheet sheet = wb.getSheetAt(0);
            if(array.size() != sheet.getRow(0).getLastCellNum()) {
                logger.debug("check Event Attr Size >>> " + array.size() + " sheet cell size >>> " + sheet.getRow(0).getLastCellNum());
                throw new Exception();
            }

            for(int i=0; i<sheet.getRow(0).getLastCellNum(); i++) {
                String pageName = (String) array.get(i);
                String sheetName = sheet.getRow(0).getCell(i).toString();

                if(!pageName.equals(sheetName)) {
                    logger.debug("check Event Attr Name >>> " + array.get(i) + " sheet attrName>>> " + sheet.getRow(0).getCell(i));
                    throw new Exception();
                }
            }

            for (int i=1; i<=sheet.getLastRowNum(); i++) {
                JSONObject eachObj = new JSONObject();
                for(int j=0; j<sheet.getRow(0).getLastCellNum(); j++) {
                    Cell cell = sheet.getRow(i).getCell(j);
                    logger.debug(">>> cell : " + typeArray.get(j) + ", " + cell);

                    if ("double".equals(typeArray.get(j))) {
                        eachObj.put(array.get(j), cell.getNumericCellValue());
                    } else if ("float".equals(typeArray.get(j))) {
                        eachObj.put(array.get(j), (float) cell.getNumericCellValue());
                    } else if ("int".equals(typeArray.get(j))) {
                        eachObj.put(array.get(j), (int) cell.getNumericCellValue());
                    } else if ("long".equals(typeArray.get(j))) {
                        eachObj.put(array.get(j), (long) cell.getNumericCellValue());
                    } else {
                        try {
                            eachObj.put(array.get(j), cell.getStringCellValue());
                        } catch (Exception e) {
                            if (DateUtil.isCellDateFormatted(cell)) {
                                System.out.println("is Cell date formatted");
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                eachObj.put(array.get(j), dateFormat.format(cell.getDateCellValue()));
                            } else {
                                e.printStackTrace();
                                System.out.println("is Cell date formatted false");
                                eachObj.put(array.get(j), (long) cell.getNumericCellValue());
                            }
                        }
                    }
                }
                attrArr.add(eachObj);
            }
        }

        obj.put("init_value", attrArr);
        logger.debug("request Obj >>> " + obj);

        logger.debug("array : " + array.toJSONString());
        obj.put("event_name_order", array);

        String path = "/input/event";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, obj, null);

        return resultObj;
    }

    /**
     * R.1.2 Input Event 삭제
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.DELETE, value="/input/event")
    @ResponseBody
    public JSONObject removeEvent(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.1.2 Input Event 삭제 Start >>> " + param);

        String event_id = (String) param.get("event_id");
        String path = "/input/event/"+event_id;

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.DELETE, null, null);

        return resultObj;
    }

    /**
     * R.1.4 Input Event 목록 조회
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value="/event")
    public String getEvent(Model model) throws Exception {
        logger.debug("R.1.4 Input Event 목록 조회 Start");
        String path = "/input/event";

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);
        model.addAttribute("list", resultObj.get("body"));

        return "eventList";
    }

    /**
     * R.1.5 Rule 기준 Input Event 목록 조회
     *
     * @param rule_id
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value="/rule/{rule_id}/input/event")
    @ResponseBody
    public JSONObject getEventListOfRule(@PathVariable String rule_id) throws Exception {
        logger.debug("R.1.5 Rule 기준 Input Event 목록 조회 Start");

        String path = "/rule/"+rule_id+"/input/event";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);

        return resultObj;
    }

    /**
     * R.1.6 Input Event 상세 조회
     *
     * @param model
     * @param event_id
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value="/eventDetail")
    public String getDetailEvent(Model model, @RequestParam String event_id) throws Exception {
        logger.debug("R.1.6 Input Event 상세 조회 start");
        String eventPath = "/input/event/" + event_id;

        JSONObject eventObj = restTempletUtil.sendToServer(eventPath, HttpMethod.GET, null, JSONObject.class);
        model.addAttribute("eventList", eventObj.get("body"));

        String rulePath = "/input/event/" + event_id + "/rule";
        JSONObject ruleObj = restTempletUtil.sendToServer(rulePath, HttpMethod.GET, null, JSONArray.class);
        model.addAttribute("ruleList", ruleObj.get("body"));

        return "eventDetail";
    }

    /**
     * R.1.7 Input Event 중복 체크
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/input/event/check")
    @ResponseBody
    public Map<String, Object> getRuleEvent(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.1.7 Input Event 중복 체크 Start >>> " + param);

        String path= "/input/event/check";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, param, null);

        return resultObj;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/send/event/history")
    @ResponseBody
    public JSONObject sendEventHistory(@RequestBody JSONObject param) throws Exception {
        logger.debug("Event 내역조회 start >>> " + param);
        JSONObject resultObj = new JSONObject();

        JSONObject result = elasticUtil.callElasticApi(param);
        logger.debug(">> result : " + result.toJSONString());

        JSONObject hists1 = (JSONObject) result.get("hits");
        JSONArray hists2 = (JSONArray) hists1.get("hits");

        JSONArray dataArr = new JSONArray();
        for (int i = 0; i < hists2.size(); i++) {
            JSONObject tmp = (JSONObject) hists2.get(i);
            JSONObject source = (JSONObject) tmp.get("_source");
            String time = (String) source.get("@timestamp");

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime ldt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
            ZonedDateTime utcDT = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
            ZonedDateTime kstDateTime = ZonedDateTime.ofInstant(utcDT.toInstant(), ZoneId.of("Asia/Seoul"));

            JSONObject dataObj = new JSONObject();
            dataObj.put("time", kstDateTime.format(format));
            dataObj.put("event", source.toJSONString());

            dataArr.add(dataObj);
        }

        resultObj.put("data", dataArr);

        JSONObject aggregations = (JSONObject) result.get("aggregations");
        JSONObject histogram = (JSONObject) aggregations.get("date_histogram#count");
        JSONArray buckets = (JSONArray) histogram.get("buckets");

        JSONArray chartArr = new JSONArray();
        for (int i = 0; i < buckets.size(); i++) {
            JSONObject tmp = (JSONObject) buckets.get(i);

            JSONArray chartObj = new JSONArray();
            chartObj.add((long) tmp.get("key"));
            chartObj.add((long) tmp.get("doc_count"));
            chartArr.add(chartObj);
        }

        resultObj.put("chart", chartArr);

        return resultObj;
    }

    @RequestMapping(value="/event/database/select")
    @ResponseBody
    public Map<String, Object> selectDB(@RequestBody JSONObject param) {
        logger.debug("Select DB 체크 Start >>> " + param);

        String url = (String) param.get("conn_url");
        String id = (String) param.get("conn_id");
        String pw = (String) param.get("conn_pw");
        int port = (int) param.get("conn_port");
        String db = (String) param.get("conn_db");
        String sql_query = (String) param.get("sql_query");
        String db_url = "jdbc:mysql://" + url + ":" + port + "/" + db + "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC";

        JSONObject resultObj = new JSONObject();
        if (!sql_query.trim().toLowerCase().startsWith("select")) {
            resultObj.put("result", "Error");
            resultObj.put("error_msg", "select 문을 사용해주세요.");

            return resultObj;
        }

        Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(db_url, id, pw);
            PreparedStatement stmt = conn.prepareStatement(sql_query);
            ResultSet rs = stmt.executeQuery();

            JSONArray label = new JSONArray();
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 1; i < rsmd.getColumnCount()+1; i++) {
                label.add(rsmd.getColumnLabel(i));
            }

            JSONArray value = new JSONArray();
            while (rs.next()) {
                JSONArray row = new JSONArray();
                for (int i = 1; i < rsmd.getColumnCount()+1; i++) {
                    row.add(rs.getObject(i));
                }

                value.add(row);
            }

            resultObj.put("result", "Success");
            resultObj.put("label", label);
            resultObj.put("value", value);

        } catch (Exception e) {
            e.printStackTrace();
            resultObj.put("result", "Error");
            resultObj.put("error_msg", e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {}
        }

        return resultObj;
    }

}


