package com.ntels.web.controller;

import com.ntels.web.common.util.RestTempletUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;

/**
 * Rule Controller Class
 */
@Controller
public class RuleController {

    private static Logger logger = LoggerFactory.getLogger(RuleController.class);

    @Autowired
    private RestTempletUtil restTempletUtil;

    @RequestMapping(value = "/")
    public String index() {
        return "home";
    }

    @RequestMapping(value = "/home")
    public String home() {
        return "home";
    }

    @RequestMapping(value = "/ruleRegister")
    public String ruleRegister(Model model) throws Exception {
        logger.debug("R.2.4 룰 목록 조회 start");
        String path = "/input/event";

        JSONObject obj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);

        model.addAttribute("list", obj.get("body"));

        // ruleRegister view에서 미사용중으로 주석처리
        //String tablePath = "/input/event?event_type=11";
        //JSONObject obj2 = restTempletUtil.sendToServer(tablePath, HttpMethod.GET, null, JSONArray.class);

        //model.addAttribute("table_list", obj2.get("body"));

        return "ruleRegister";
    }


    /**
     * R.2.1 룰 등록
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/rule")
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public JSONObject addRule(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.2.1 룰 등록 Start >>>" + param);

        String path = "/rule";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, param, null);

        return resultObj;
    }

    /**
     * R.2.2 룰 삭제
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/rule")
    @ResponseBody
    public JSONObject removeRule(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.2.2 룰 삭제 start >>> " + param);

        String rule_id = (String) param.get("rule_id");
        String path = "/rule/" + rule_id;

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.DELETE, null, null);

        return resultObj;
    }

    /**
     * R.2.4 룰 목록 조회
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/rule")
    public String getRule(Model model) throws Exception {
        logger.debug("R.2.4 룰 목록 조회 start");

        String path = "/rule";

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);
        model.addAttribute("list", resultObj.get("body"));

        return "ruleList";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ruleHistory")
    public String getRuleHistory(Model model) throws Exception {
        logger.debug("R.2.4 룰 히스토리 목록 조회 start");

        String path = "/rule/hist";

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);
        model.addAttribute("list", resultObj.get("body"));

        return "ruleHistList";
    }

    /**
     * R.2.6 룰 상세 조회
     *
     * @param model
     * @param rule_id
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/ruleDetail")
    public String ruleDetail(Model model, @RequestParam String rule_id) throws Exception {
        logger.debug("R.2.6 룰 상세 조회 started");

        String path = "/rule/" + rule_id;

        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONObject.class);
        model.addAttribute("list", resultObj.get("body"));

        return "ruleDetail";
    }


    /**
     * R.2.6 Rule Validation 체크
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/rule/validate")
    @ResponseBody
    public JSONObject valiRule(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.2.6 Rule Validation 체크 start >>> " + param);

        String path = "/rule/validate";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, param, null);

        return resultObj;
    }

    @RequestMapping(value="/rule/database/conn")
    @ResponseBody
    public Map<String, String> connDB(@RequestBody JSONObject param) throws Exception {
        logger.debug("Connection DB 체크 Start >>> " + param);

        String url = (String) param.get("conn_url");
        String id = (String) param.get("conn_id");
        String pw = (String) param.get("conn_pw");
        int port = (int) param.get("conn_port");
        String db = (String) param.get("conn_db");
        String db_url = "jdbc:mysql://" + url + ":" + port + "/" + db + "?connectTimeout=5000&socketTimeout=5000&characterEncoding=UTF-8&serverTimezone=UTC";
        JSONObject resultObj = new JSONObject();

        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection(db_url, id, pw);
        conn.createStatement();
        conn.close();

        return resultObj;
    }

}
