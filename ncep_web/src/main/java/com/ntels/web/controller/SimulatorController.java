package com.ntels.web.controller;

import com.ntels.web.common.util.RestTempletUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Simulator Controller Class
 */
@Controller
public class SimulatorController {

    private static Logger logger = LoggerFactory.getLogger(SimulatorController.class);

    @Autowired
    private RestTempletUtil restTempletUtil;

    @Value("${websocket.url}")
    private String websocketUrl;

    /**
     * Simulator Page 호출
     *
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/sendSimulator")
    public String sendSimulator(Model model) throws Exception {
        logger.debug("sendSimulator called : " + websocketUrl);

        String path = "/rule";
        JSONObject ruleObj = restTempletUtil.sendToServer(path, HttpMethod.GET, null, JSONArray.class);
        model.addAttribute("ruleList", ruleObj.get("body"));
        model.addAttribute("websocketUrl", websocketUrl);

        return "sendSimulator";
    }

    /**
     * R.4.1 Event 전송
     *
     * @param param
     * @return
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/send/event")
    @ResponseBody
    public JSONObject sendEvent(@RequestBody JSONObject param) throws Exception {
        logger.debug("R.4.1 sendEvent Started");

        String event_name = (String) param.get("event_name");
        Map<String, String> event =  (HashMap) param.get("event");
        Map<String, String> eventType =  (HashMap) param.get("eventType");
        Map<String, Object> newEvent =  new HashMap<>();

        Iterator<String> keys = event.keySet().iterator();
        while(keys.hasNext()) {
            String key = keys.next();

            String dataType = eventType.get(key);
            if ("double".equals(dataType)) {
                newEvent.put(key, Double.parseDouble(event.get(key)));
            } else if ("float".equals(dataType)) {
                newEvent.put(key, Float.parseFloat(event.get(key)));
            } else if ("int".equals(dataType)) {
                newEvent.put(key, Integer.parseInt(event.get(key)));
            } else if ("long".equals(dataType)) {
                newEvent.put(key, Long.parseLong(event.get(key)));
            } else {
                newEvent.put(key, event.get(key));
            }
        }

        JSONObject obj = new JSONObject();
        obj.put("event_name", event_name);
        obj.put("event", newEvent);
        obj.put("isSimulator", "true");
        String path = "/send/event";
        JSONObject resultObj = restTempletUtil.sendToServer(path, HttpMethod.POST, obj, null);

        return resultObj;
    }
}
