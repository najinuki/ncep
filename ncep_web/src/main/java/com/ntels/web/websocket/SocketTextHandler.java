package com.ntels.web.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.ArrayList;

@Component
public class SocketTextHandler extends TextWebSocketHandler {

    private static Logger logger = LoggerFactory.getLogger(SocketTextHandler.class);
    private static ArrayList<WebSocketSession> wsSession = new ArrayList<>();

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.debug("WebSocket Connected : " + session.getUri());

        if ("/simulator".equals(session.getUri().toString())) {
            wsSession.add(session);
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        logger.debug("WebSocket Closed : " + session.getUri());

        if ("/simulator".equals(session.getUri().toString())) {
            wsSession.remove(session);
        }

    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {
        logger.debug("WebSocket Message Received : " + session.getUri());

        if (wsSession.size() > 0) {
            for (WebSocketSession ws : wsSession) {
                if (ws.isOpen()) {
                    ws.sendMessage(textMessage);
                }
            }
        }

    }

}