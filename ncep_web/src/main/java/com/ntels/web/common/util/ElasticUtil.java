package com.ntels.web.common.util;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ElasticUtil {

    private static Logger logger = LoggerFactory.getLogger(ElasticUtil.class);

    @Value("${elastic.url}")
    private String host;

    @Value("${elastic.port}")
    private int port;

    public JSONObject callElasticApi(JSONObject param) {
        JSONObject result = new JSONObject();
        RestHighLevelClient client = null;

        try {
            String from = (String) param.get("from");
            String to = (String) param.get("to");
            String searchField = (String) param.get("searchField");
            String searchText = (String) param.get("searchText");

            // Default 는 최근 1시간
            if (from == null && to == null) {
                from = LocalDateTime.now().minusHours(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                to = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            }

            System.out.println("from : " + from + ", to : " + to);

            client = new RestHighLevelClient(RestClient.builder(new HttpHost(host, port)));

            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            DateHistogramAggregationBuilder aggregation = AggregationBuilders.dateHistogram("count").field("@timestamp");
            //aggregation.interval(1800000); // 30분
            aggregation.interval(600000); // 10분

            searchSourceBuilder.aggregation(aggregation);

            //QueryBuilder qb = QueryBuilders.matchQuery("gw_id", "D36503098373");
            RangeQueryBuilder rqb = QueryBuilders.rangeQuery("@timestamp")
                    .gte(from)
                    .lte(to)
                    .format("yyyy-MM-dd HH:mm:ss||yyyy-MM-dd HH:mm:ss")
                    .timeZone("+09:00");

            QueryBuilder rsltQB = null;
            if (searchField != null && searchText != null) {
                QueryBuilder qb = QueryBuilders.matchQuery(searchField, searchText);
                rsltQB = QueryBuilders.boolQuery().must(qb).filter(rqb);
            } else {
                rsltQB = QueryBuilders.boolQuery().filter(rqb);
            }

            searchSourceBuilder.query(rsltQB);
            searchSourceBuilder.sort(new FieldSortBuilder("@timestamp").order(SortOrder.DESC));
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.source(searchSourceBuilder);
            searchRequest.indices("ami", "fac", "ieq", "power");
            //searchRequest.indices();

            SearchResponse searchResponse = client.search(searchRequest);

            JSONParser parser = new JSONParser();
            result = (JSONObject) parser.parse(searchResponse.toString());

            logger.debug(">>>> result : " + result.toJSONString());

        } catch (Exception e) {
            logger.error("Error : ", e);
        } finally {
            try { client.close(); } catch (Exception e) {}
        }

        return result;
    }

}
