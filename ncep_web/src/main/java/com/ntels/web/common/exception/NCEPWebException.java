package com.ntels.web.common.exception;

public class NCEPWebException extends RuntimeException {

    private final int errorCode;

    public NCEPWebException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMsg() {
        String message = this.getMessage();
        return "(" + this.errorCode + ") " + (message != null ? message : "");
    }

}
