package com.ntels.web.common.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;


/**
 * Created by yongjoo on 2018-03-06.
 */
@Component
public class RestTempletUtil {
    private Logger logger = LoggerFactory.getLogger(RestTempletUtil.class);

    @Value("${server.default.url}")
    private String SERVER_URL;

    public JSONObject sendToServer(String path, HttpMethod method, Map<String, Object> body, Class dataType) {

        String url = SERVER_URL + path;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity responseEntity = null;
        HttpEntity<Object> request = null;
        String requestType = null;

        if(dataType != null) {
            requestType = dataType.getSimpleName();
        }

        JSONObject obj = new JSONObject();
        request = new HttpEntity<Object>(body);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

        logger.debug("----------------------- REST Request -----------------------");
        logger.debug("URL : " + builder.toUriString());
        logger.debug("Method : " + method);
        logger.debug("Param : " + body);
        logger.debug("------------------------------------------------------------");

        try {
            if(HttpMethod.GET == method && requestType.equals("JSONArray")) {
                responseEntity = restTemplate.exchange(builder.toUriString(), method, request, JSONArray.class);
            }

            if(HttpMethod.GET == method && requestType.equals("JSONObject")) {
                responseEntity = restTemplate.exchange(builder.toUriString(), method, request, JSONObject.class);
            }

            if(HttpMethod.POST == method) {
                responseEntity = restTemplate.exchange(builder.toUriString(), method, request, Map.class);
            }

            if(HttpMethod.DELETE == method) {
                responseEntity = restTemplate.exchange(builder.toUriString(), method, request, Map.class);
            }

            logger.debug("----------------------- REST Response -----------------------");
            logger.debug("Status Code : " + responseEntity.getStatusCode());
            logger.debug("Response Body : " + responseEntity.getBody());
            logger.debug("-------------------------------------------------------------");

            obj.put("body", responseEntity.getBody());
            obj.put("status_code", responseEntity.getStatusCode());
            return obj;

        } catch (HttpClientErrorException e) {
            logger.debug("----------------------- REST Response -----------------------");
            logger.debug("Status Code : " + e.getStatusCode());
            logger.debug("Response Body : " + e.getResponseBodyAsString());
            logger.debug("-------------------------------------------------------------");
            throw new HttpClientErrorException(e.getStatusCode());
        }
    }


}
