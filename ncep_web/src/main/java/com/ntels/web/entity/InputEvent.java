package com.ntels.web.entity;

/**
 * Created by yongjoo on 2018-02-01.
 */
public class InputEvent {
    String event_id;
    String event_name;
    String event_descp;
    String event_type;
    String init_value_yn;
    String init_value;
    String event_query;

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_descp() {
        return event_descp;
    }

    public void setEvent_descp(String event_descp) {
        this.event_descp = event_descp;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getInit_value_yn() {
        return init_value_yn;
    }

    public void setInit_value_yn(String init_value_yn) {
        this.init_value_yn = init_value_yn;
    }

    public String getInit_value() {
        return init_value;
    }

    public void setInit_value(String init_value) {
        this.init_value = init_value;
    }

    public String getEvent_query() {
        return event_query;
    }

    public void setEvent_query(String event_query) {
        this.event_query = event_query;
    }
}
