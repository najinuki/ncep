package com.ntels.web.entity;

import java.util.List;

/**
 * Created by yongjoo on 2018-02-01.
 */
public class Rule {
    String rule_id;
    String rule_name;
    String rule_descp;
    String rule_query;
    String callback_name;
    String action_name;
    String send_type;
    Object send_info;
    String send_content;

    List event_info;
    String event_id;
    String init_value_yn;
    String event_query;

    public String getEvent_query() {
        return event_query;
    }

    public void setEvent_query(String event_query) {
        this.event_query = event_query;
    }

    public Object getSend_info() {
        return send_info;
    }

    public void setSend_info(Object send_info) {
        this.send_info = send_info;
    }

    public List getEvent_info() {
        return event_info;
    }

    public void setEvent_info(List event_info) {
        this.event_info = event_info;
    }

    public String getRule_id() {
        return rule_id;
    }

    public void setRule_id(String rule_id) {
        this.rule_id = rule_id;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getRule_descp() {
        return rule_descp;
    }

    public void setRule_descp(String rule_descp) {
        this.rule_descp = rule_descp;
    }

    public String getRule_query() {
        return rule_query;
    }

    public void setRule_query(String rule_query) {
        this.rule_query = rule_query;
    }

    public String getCallback_name() {
        return callback_name;
    }

    public void setCallback_name(String callback_name) {
        this.callback_name = callback_name;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getInit_value_yn() {
        return init_value_yn;
    }

    public void setInit_value_yn(String init_value_yn) {
        this.init_value_yn = init_value_yn;
    }

    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public String getSend_type() {
        return send_type;
    }

    public void setSend_type(String send_type) {
        this.send_type = send_type;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }
}
