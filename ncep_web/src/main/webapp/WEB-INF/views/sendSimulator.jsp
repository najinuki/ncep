<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- CodeMirror -->
    <link rel="stylesheet" type="text/css" media="screen" href="resources/css/codemirror.css"/>

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">


  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-calculator"></i> <b>Send Simulator</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="x_panel" style="min-height:650px;">
                  <div class="x_title">
                    Rule 정보
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left">
                      <label for="fullname">Rule</label>
                      <select id="ruleSelect" class="form-control">
                        <option value="">Choose Rule ...</option>
                        <c:forEach var="ruleList" items="${ruleList}">
                            <option value="${ruleList.rule_id}" value2="${ruleList.rule_query}">${ruleList.rule_name}</option>
                        </c:forEach>
                      </select>
                      <br/>

                      <label id="eveltList" for="email">Event</label>
                      <select id="eventSelect" class="form-control">
                        <option value="">Choose Event ...</option>

                      </select>
                      <br/><br/>

                      <label id="event_plus" class="event_plus" for="email">Event Attribute</label>

                      <br/><br/>
                      <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
                      <button type="button" id="sendButton" class="btn btn-success btn-lg"><i class="fa fa-play"></i>  Send</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    Rule Query
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <textarea id="code" required="required" name="code" class="form-control col-md-7 col-xs-12"></textarea>
                  </div>
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    Console
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <textarea id="simTextArea" required="required" name="simTextArea" class="form-control col-md-7 col-xs-12" style="height:200px;"></textarea>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>

    <script src="resources/js/codemirror.js"></script>
    <script src="resources/js/sql.js"></script>

    <script src="resources/build/js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            <!-- CodeMirror  -->
            var query = CodeMirror.fromTextArea(document.getElementById("code"), {
              mode: "text/x-sql"
            });

            $(".CodeMirror.cm-s-default").css("border","solid 1px #73879C");
            $(".CodeMirror.cm-s-default").css("cursor","text");
            $(".CodeMirror.cm-s-default").css("font-family","Helvetica Neue");
            $(".CodeMirror.cm-s-default").css("font-size","14px");

            var rule_id = "";
            var rule_query = "";
            $('#ruleSelect').on('change', function() {
                var resultObj = new Object();
                rule_id = $('#ruleSelect').val();
                rule_query = $('#ruleSelect > option:selected').attr('value2');

                resultObj.rule_id = rule_id;

                if(rule_id == "") {
                    $('.optList').remove();
                    $('.attrList').remove();
                    query.setValue("");
                    return false;
                }

                query.setValue(rule_query);
                $('.attrList').remove();

                jsonData = JSON.stringify(resultObj);
                $.ajax({
                    method: "get",
                    url : "/rule/"+rule_id+"/input/event",
                    contentType: "application/json",
                    success : function (data) {
                        $('.optList').remove();
                        for(var i=0; i<data.body.length; i++) {
                            var test = data.body[i].event_query;
                            $('#eventSelect').append('<option id="eventList" class="optList" value=' + '"'
                                                    + data.body[i].event_query + '"'
                                                    + 'value2 = ' + '"'
                                                    + data.body[i].event_name + '"'
                                                    + '>'
                                                    + data.body[i].event_name + '</option>')
                        }
                    },

                     error: function(request, status, error) {
                        alert("Error Code : " + request.status + "\n" + "Error Cause: " + error);
                    }
                });
            });

            var event_name = "";
            var event_query = "";
            var attrType = new Array();
            $('#eventSelect').on('change', function() {
                attrType = [];
                event_name = $('#eventSelect > option:selected').attr('value2');
                event_query = $('#eventSelect').val();
                var start = event_query.indexOf("(");
                var end = event_query.indexOf(")", start+1);
                var attr = event_query.substring(start+1, end);
                var attrArr = attr.split(",");
                var length = attrArr.length;

                if(attrArr=="") {
                    $('.attrList').remove();
                    return false;
                }

                $('.attrList').remove();
                for(var i=0; i<length; i++) {
                    var attr_name = attrArr[i].trim().split(" ");
                    attrType.push(attr_name[1]);
                    $('#event_plus').append('<div id="attrList" class="attrList form-group">'
                                            +   '<label id="attrName" class="control-label col-md-4 col-sm-4 col-xs-12" style="text-align:left;">'
                                            +       attr_name[0]
                                            +   '</label>'
                                            +   '<div class="col-md-8 col-sm-8 col-xs-12">'
                                            +       '<input type="text" id="attrVal" class="form-control">'
                                            +   '</div>'
                                            + '</div>'
                                            + '');
                    }
            });

            $('#sendButton').click(function() {
                var totalObj = new Object();
                totalObj.event_name = event_name;
                var obj = new Object();
                var objType = new Object();

                if(rule_id=="") {
                    alert("룰을 선택해주세요");
                    return false;
                }

                if(event_query=="") {
                    alert("이벤트를 선택해주세요");
                    return false;
                }

                var check = true;
                $("label[id='attrName']").each(function (i) {
                    var attr_type = attrType[i];
                    var attr_name = $("label[id='attrName']").eq(i).text();
                    var attr_val= $("input[id='attrVal']").eq(i).val();

                    if(attr_val=="") {
                        alert("속성 값 " + attr_name + "을 입력해 주세요");
                        check = false;
                        return false;
                    }

                    if(attr_type=="string") {
                        attr_val = $("input[id='attrVal']").eq(i).val();
                    } else {
                        if (isNaN(attr_val)) {
                            alert("자료형을 잘못 입력했습니다.");
                            check = false;
                            return false;
                        }
                    }

                    obj[attr_name] = attr_val;
                    objType[attr_name] = attr_type;

                });

                if(check == false) {
                    return false;
                }

                totalObj.event = obj;
                totalObj.eventType = objType;
                jsonData = JSON.stringify(totalObj);

                $.ajax({
                    method: "post",
                    url: "/send/event",
                    data: jsonData,
                    contentType: "application/json",
                    success:function(data) {

                    },

                     error: function(request, status, error) {
                        alert("속성 자료형이 일치하지 않습니다." + "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                    }
                });

            });

            var webSocket = new WebSocket('${websocketUrl}/simulator');
            webSocket.onopen = onOpen;
            webSocket.onclose = onClose;
            webSocket.onmessage = onMessage;

            function onOpen(event) {
                $('#simTextArea').text("********************** Rule Simulator **********************\n");
            }

            function onClose(event) {

            }

            function onMessage(event) {
                console.log("event : ", event);
                var wsData = JSON.parse(event.data);

                if (rule_id == wsData.rule_id || wsData.isSimulator == 'true') {
                    var text = $('#simTextArea').text();
                    text += "------------------------------------------------------------------\n";
                    text += wsData.msg + "\n";
                    text += "------------------------------------------------------------------\n";

                    $('#simTextArea').text(text);
                }
            }
        });

    </script>

  </body>
</html>
