<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="resources/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- SmartWizard -->
    <link href="resources/vendors/jQuery-Smart-Wizard/styles/smart_wizard.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="resources/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <!-- CodeMirror -->
    <link rel="stylesheet" type="text/css" media="screen" href="resources/css/codemirror.css"/>

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">
    <style type="text/css">
    <!--
        .cm-s-default {
            border: solid 1px #73879C;
            cursor: text;
            font-family: Helvetica Neue;
            font-size: 14px;
        }
        .CodeMirror pre {
            padding: 0 40px; /* Horizontal padding of content */
        }
    //-->
    </style>
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-edit"></i> <b>Rule 등록</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- Smart Wizard -->
                    <div id="smartWizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                              Input Event 선택 <br />
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                              Rule 등록 <br />
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                              Output Action 등록 <br />
                            </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-1">
                        <div class="x_panel">
                          <div class="x_title">
                          <b>Event 목록</b>
                          </div>
                          <div class="x_content">
                            <table id="eventTable" class="table table-striped table-bordered" style="cursor:pointer">
                              <thead>
                                <tr>
                                  <th style="overflow:hidden;white-space:nowrap;">선택</th>
                                  <th id="event_name" style="overflow:hidden;white-space:nowrap;">Event 명</th>
                                  <th style="overflow:hidden;white-space:nowrap;">Event 타입</th>
                                  <th style="overflow:hidden;white-space:nowrap;">초기값 유무</th>
                                  <th style="overflow:hidden;white-space:nowrap;">Event 내용</th>
                                </tr>
                              </thead>

                              <tbody>
                               <c:forEach var="eventList" items="${list}">
                                  <tr>
                                     <td>
                                       <input class="flat" type="checkbox" id="checkEvent" name="checkEvent" value=${eventList.event_id} />
                                     </td>
                                     <td id="event_name">${eventList.event_name}</td>
                                      <c:choose>
                                        <c:when test="${eventList.event_type == 10}">
                                          <td id="event_type_${eventList.event_id}">Stream</td>
                                        </c:when>
                                        <c:when test="${eventList.event_type == 11}">
                                          <td id="event_type_${eventList.event_id}">Table</td>
                                        </c:when>
                                        <c:when test="${eventList.event_type == 12}">
                                          <td id="event_type_${eventList.event_id}">Trigger</td>
                                        </c:when>
                                      </c:choose>
                                     <td id="event_init_${eventList.event_id}">${eventList.init_value_yn}</td>
                                     <td id="event_query_${eventList.event_id}">${eventList.event_query}</td>
                                  </tr>
                               </c:forEach>
                               </tbody>

                            </table>
                          </div>
                        </div>
                      </div>

                      <form class="form-horizontal form-label-left" novalidate>
                          <div id="step-2">
                            <div class="x_panel">
                              <div class="x_title">
                              <b>Rule 등록</b>
                              </div>
                              <div class="x_content">
                                    <div class="item form-group">
                                      <label for="rule_name">Rule 명 <span class="required">*</span>
                                      </label>
                                      <input id="rule_name" class="form-control col-md-7 col-xs-12" name="name" required="required" type="text">
                                    </div>
                                    <div class="item form-group">
                                      <label for="rule_descp">Rule 설명 <span class="required">*</span>
                                      </label>
                                      <input id="rule_descp" class="form-control col-md-7 col-xs-12" name="rule_descp" required="required" type="text">
                                    </div>
                                    <div>
                                      <div class="form-group">
                                        <label for="query">Event Query</label>
                                        <textarea id="event_query" class="form-control col-md-7 col-xs-12">
                                        </textarea>
                                      </div>
                                    </div>

                                    <div>
                                      <div class="form-group">
                                        <label for="query">Rule Query <span class="required">*</span>
                                        </label>
                                        <textarea id="code" required="required" name="code"></textarea>
                                      </div>
                                    </div>
                              </div>
                            </div>
                          </div>


                          <div id="step-3">
                            <div class="x_panel">
                              <div class="x_title">
                              <b>Action 등록</b>
                              </div>
                              <div class="x_content">

                                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                                  <div class="form-group">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">시간별 전송방법</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select id="time_send_method" name="time_send_method" class="form-control" required>
                                        <option value="">선택하세요</option>
                                        <option value="11">매번</option>
                                        <option value="12">최초 1회</option>
                                        <option value="13">시간당 1회</option>
                                        <option value="14">하루에 1회</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group" id="div_send_type">
                                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">전송 타입</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select id="heard" name="heard" class="form-control" required>
                                        <option value="">선택하세요</option>
                                        <option value="E-Mail">E-Mail</option>
                                        <option value="REST">REST</option>
                                        <option value="KAKAO">카카오톡</option>
                                        <option value="DB">DB Table</option>
                                      </select>
                                    </div>
                                  </div>

                                  <!-- 동적변화 -->
                                  <div id="divEmail" style="display:none;">
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">메일 주소</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="email_address" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">메일 제목</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="email_subject" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">전송 내용</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="email_content" class="editor-wrapper"></div>
                                        <textarea name="descr1" id="descr1" style="display:none;"></textarea>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="divREST" style="display:none;">
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">URL</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="send_url" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Port</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="send_port" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Path</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="send_path" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Method</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="send_method" class="form-control" required>
                                          <option value="">선택하세요</option>
                                          <option value="get">GET</option>
                                          <option value="post">POST</option>
                                          <option value="put">PUT</option>
                                          <option value="delete">DELETE</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Header</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-md-4 col-sm-4 col-xs-12 form-group" style="padding-left:0px;">
                                          <input type="text" placeholder="Authorization" class="form-control" readonly>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                                          <input type="text" id="auth_val" placeholder="Value" class="form-control">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">전송내용 타입</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="rest_content_type" class="form-control" required>
                                          <option value="html" select>HTML</option>
                                          <option value="text">TEXT</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">전송 내용</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="rest_content" class="editor-wrapper"></div>
                                        <textarea name="descr2" id="descr2" style="display:none;"></textarea>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="divKakao" style="display:none;">
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">메시지 제목</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="send_kakao_subject" required="required" class="form-control col-md-7 col-xs-12">
                                       </div>
                                      <a class="col-md-3 col-sm-3 col-xs-6" id="kakao-login-btn"></a>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">전송 내용</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="send_kakao_message" class="editor-wrapper"></div>
                                        <textarea name="descr1" id="descr1" style="display:none;"></textarea>
                                      </div>
                                    </div>
                                  </div>

                                  <div id="divDB" style="display:none;">
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">데이터베이스 주소</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group" style="margin-bottom: 0px;">
                                          <input type="text" id="conn_url" required="required" class="form-control col-md-7 col-xs-12">
                                          <span class="input-group-btn"><button id="connButton" type="button" class="btn btn-primary">접속테스트</button></span>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">ID</label>
                                      <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" id="conn_id" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                      <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">Password</label>
                                      <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="password" id="conn_pw" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">포트번호</label>
                                      <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" id="conn_port" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                      <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">데이터베이스</label>
                                      <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" id="conn_db" required="required" class="form-control col-md-7 col-xs-12">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">SQL 쿼리</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div id="conn_sql" class="editor-wrapper"></div>
                                        <textarea name="descr3" id="descr3" style="display:none;"></textarea>
                                      </div>
                                    </div>
                                  </div>

                                </form>

                              </div>
                            </div>
                          </div>
                      </form>
                    </div>
                    <!-- End SmartWizard Content -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- validator -->
    <script src="resources/vendors/validator/validator.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="resources/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- iCheck -->
    <script src="resources/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="resources/vendors/jszip/dist/jszip.min.js"></script>
    <script src="resources/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="resources/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- bootstrap-wysiwyg -->
    <script src="resources/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="resources/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="resources/vendors/google-code-prettify/src/prettify.js"></script>

    <script src="resources/js/codemirror.js"></script>
    <script src="resources/js/sql.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="resources/build/js/custom.js"></script>

    <!-- Kakao Scripts -->
    <script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>

    <script type="text/javascript">
      var authInfo = "";

      $(document).ready(function() {
        // init시 생성된 Kakao 애플리케이션의 JavaScript 키를 넣어준다.
        Kakao.init('60fe9ea06c92a1d9c320941a88ec4ce8');

        // 카카오 로그인 버튼을 생성합니다.
        Kakao.Auth.createLoginButton({
            container: '#kakao-login-btn',
            success: function(authObj) {

              authInfo = JSON.stringify(authObj);
            },
            fail: function(err) {
              alert(JSON.stringify(err));
            }
        });

        $('#eventTable').DataTable({
            "columnDefs": [{
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            }],
            "oLanguage": {
                "sEmptyTable": "데이터가 없습니다."
            }
        });

        <!-- Parsley Setting -->
        var totalObj = new Object();
        var event_id;
        var event_query;
        var jsonData;

        <!-- Smart Wizard Setting & Validation Check -->
        $('#smartWizard').smartWizard({
            onLeaveStep:leaveAStepCallback,
            onFinish:onFinishCallback,
            onShowStep:onShowCallback,
            keyNavigation: false,
            buttonOrder: ['prev', 'next', 'finish'],
            labelPrevious: '이전',
            labelNext: '다음',
            labelFinish: '저장'
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
        $('.actionBar').append('<a href="#" id="checkButton" class="buttonValidate btn btn-danger">문법검사</a>');
        $('.buttonValidate').css('display', 'none');

        function leaveAStepCallback(obj, context){
            return validateSteps(context.fromStep, context.toStep);
        }

        function onShowCallback(objs, context){
            if (context.toStep == 1) {
                $('.buttonPrevious').css('display', 'none');
                $('.buttonNext').css('display', 'inline-block');
                $('.buttonFinish').css('display', 'none');
                $('.buttonValidate').css('display', 'none');
            } else if (context.toStep == 2) {
                $('.buttonPrevious').css('display', 'inline-block');
                $('.buttonNext').css('display', 'inline-block');
                $('.buttonFinish').css('display', 'none');
                $('.buttonValidate').css('display', 'inline-block');
            } else {
                $('.buttonPrevious').css('display', 'inline-block');
                $('.buttonNext').css('display', 'none');
                $('.buttonFinish').css('display', 'inline-block');
                $('.buttonValidate').css('display', 'none');
            }
        }

        function onFinishCallback(objs, context){
            if(validateFinishStep()){
                var isStepValid = true;

                jsonData = JSON.stringify(totalObj);

                $.ajax({
                    method: "post",
                    url : "/rule",
                    data: jsonData,
                    contentType: "application/json",
                    success : function (data) {
                        alert("룰이 등록 되었습니다");
                        location.reload();
                    },
                    error: function(request, status, error) {
                        alert("룰 등록 실패"+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                    }
                });
            }
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function validateFinishStep() {
            var isStepValid = true;
            var time_send_method = $("select[name=time_send_method]").val();

            if( time_send_method == "" ) {
                alert("시간별 전송방법을 선택해 주세요");
                isStepValid = false;
                return isStepValid;
            }

            totalObj.send_method = time_send_method;

            var send_type = $("select[name=heard]").val();
            if( send_type == "" ) {
                alert("전송 타입을 입력해 주세요");
                isStepValid = false;
                return isStepValid;
            }
            totalObj.send_type = send_type;

            if(send_type == "REST") {
                var infoObj = new Object();

                if( $("#send_url").val() == "" ) {
                    alert("URL을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.url = $("#send_url").val();

                if( $("#send_port").val() == "" ) {
                    alert("PORT를 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.port = $("#send_port").val();

                if( $("#send_path").val() == "" ) {
                    alert("Path를 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.path = $("#send_path").val();

                if( $("#send_method").val() == "" ) {
                    alert("Method를 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.method =$("#send_method").val();

                if( $("#rest_content").html() == "" ) {
                    alert("전송내용을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }

                var header = new Object();
                header.Authorization = $("#auth_val").val();
                infoObj.header = header;

                totalObj.send_info = infoObj;
                totalObj.send_type = send_type;

                if ($('#rest_content_type').val() == "html") {
                    totalObj.send_content = $('#rest_content').html();
                } else {
                    totalObj.send_content = $('#rest_content').text().replace(/\s+/g,' ' ).replace(/^\s/,'').replace(/\s$/,'');
                }
            }

            if(send_type == "E-Mail") {
                var infoObj = new Object();
                var email_address = $("#email_address").val();

                if( email_address == "" ) {
                    alert("이메일 주소를 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.to = email_address;

                if( $("#email_subject").val() == "" ) {
                    alert("이메일 제목을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.title = $("#email_subject").val();

                var email_address_validation = validateEmail(email_address);

                if ( email_address_validation == false ) {
                    alert("잘못된 메일주소입니다. 다시 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }

                if( $("#email_content").html() == "" ) {
                    alert("전송내용을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }

                totalObj.send_info = infoObj;
                totalObj.send_content = $('#email_content').html();
            }

            if(send_type == "KAKAO") {
                var infoObj = new Object();

                if ( authInfo == "") {
                    alert("카카오계정 연결정보를 확인해주세요.");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.auth_info = authInfo;

                if( $("#send_kakao_subject").val() == "" ) {
                    alert("메시지 제목을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }
                infoObj.title = $("#send_kakao_subject").val();

                if( $("#send_kakao_message").html() == "" ) {
                    alert("전송내용을 입력해 주세요");
                    isStepValid = false;
                    return isStepValid;
                }

                totalObj.send_info = infoObj;
                totalObj.send_type = send_type;
                totalObj.send_content = $('#send_kakao_message').html();
            }

            if(send_type == "DB") {
                var infoObj = new Object();
                infoObj.conn_url = $("#conn_url").val();
                infoObj.conn_id = $("#conn_id").val();
                infoObj.conn_pw = $("#conn_pw").val();
                infoObj.conn_port = $("#conn_port").val();
                infoObj.conn_db = $("#conn_db").val();
                totalObj.send_info = infoObj;
                totalObj.send_content = $('#conn_sql').text().trim();
            }

            return isStepValid;
        }

        var checkVal = false;
        function validateSteps(stepnumber, tonumber){
            var isStepValid = true;
            var tdArr = new Array();
            if(stepnumber == 1){
                 $("#event_query").val("");
                 $("input[name=checkEvent]:checked").each(function() {
                       event_id = $(this).val();
                       event_type = $("#event_type_" + event_id).html();
                       event_query = $("#event_query_" + event_id).html();
                       var init_value_yn = $("#event_init_" + event_id).html();
                       console.log("init_value_yn : ", init_value_yn);
                       var obj = new Object();
                       obj.event_id = event_id;
                       obj.event_type = event_type;
                       obj.event_query = event_query;
                       obj.init_value_yn = init_value_yn;
                       tdArr.push(obj);

                       /*$(this).attr('checked', false);*/

                       var tmpVal = $("#event_query").val().trim();
                       if (tmpVal == "") {
                           $("#event_query").val($("#event_query_" + event_id).html());
                       } else {
                           $("#event_query").val(tmpVal + "\r\n" + $("#event_query_" + event_id).html());
                       }
                 });

                 totalObj.event_info = tdArr;

                 var tableTypeCount = 0;
                 var totalObjLength = totalObj.event_info.length;

                 for (var i in totalObj.event_info) {
                    if (totalObj.event_info[i].event_type == "Table") {
                        tableTypeCount += 1;
                    }
                 }

                 if(totalObjLength == tableTypeCount) {
                     alert("Event를 Table만 선택할 수는 없습니다.");
                     isStepValid = false;
                     return isStepValid;
                 }

                 if(totalObjLength == 0) {
                     alert("등록할 이벤트를 선택해 주세요");
                     isStepValid = false;
                     return isStepValid;
                 }
            }

            if(stepnumber == 2 && tonumber > stepnumber){
                if($('#rule_name').val()=="" ) {
                    alert("룰 이름을 입력하지 않았습니다.");
                    isStepValid = false;
                    return isStepValid;
                }

                if( query.getValue()=="" ) {
                    alert("룰 쿼리를 입력하지 않았습니다.");
                    isStepValid = false;
                    return isStepValid;
                }

                if(checkVal == false) {
                    alert("룰 쿼리검사를 하지 않았습니다.");
                    isStepValid = false;
                    return isStepValid;
                }

                totalObj.rule_query = query.getValue();
                totalObj.rule_name = $('#rule_name').val();
                totalObj.rule_descp = $('#rule_descp').val();
            }
            return isStepValid;
        }

        $(".buttonValidate").click(function() {
            checkVal = true;
            var event_query = $("#event_query").val();

            var queryObj = new Object();
            queryObj.rule_query = query.getValue();
            queryObj.event_query = event_query;
            queryData = JSON.stringify(queryObj);

            $.ajax({
                method: "post",
                url: "/rule/validate",
                data: queryData,
                contentType: "application/json",
                success:function(data) {
                    alert("올바른 쿼리 문법입니다.");
                },

                error: function(request, status, error) {
                    alert("쿼리를 다시 확인해 주세요" + "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                }
            });
        });

        $("#heard").on("change", function(){
            if( $("select[name=heard]").val() == "E-Mail" ) {
                delete totalObj.send_info;
                delete totalObj.send_content;

                $("#send_url").val('');
                $("#send_port").val('');
                $("#send_path").val('');
                $("#send_method").val('');
                $('#rest_content').html('');

                $("#conn_url").val('');
                $("#conn_id").val('');
                $("#conn_pw").val('');
                $("#conn_port").val('');
                $("#conn_db").val('');

                $('#send_kakao_subject').val('');
                $('#send_kakao_message').html('');

                $("#divEmail").show();
                $("#divREST").hide();
                $("#divDB").hide();
                $("#divKakao").hide();
            }

            if( $("select[name=heard]").val() == "REST" ) {
                delete totalObj.send_info;
                delete totalObj.send_content;

                $("#email_address").val('');
                $("#email_subject").val('');
                $("#email_content").html('');

                $("#conn_url").val('');
                $("#conn_id").val('');
                $("#conn_pw").val('');
                $("#conn_port").val('');
                $("#conn_db").val('');

                $('#send_kakao_subject').val('');
                $('#send_kakao_message').html('');

                $("#divEmail").hide();
                $("#divREST").show();
                $("#divDB").hide();
                $("#divKakao").hide();
            }

            if( $("select[name=heard]").val() == "KAKAO" ) {
                delete totalObj.send_info;
                delete totalObj.send_content;

                $("#send_url").val('');
                $("#send_port").val('');
                $("#send_path").val('');
                $("#send_method").val('');
                $('#rest_content').html('');

                $("#email_address").val('');
                $("#email_subject").val('');
                $("#email_content").html('');

                $("#conn_url").val('');
                $("#conn_id").val('');
                $("#conn_pw").val('');
                $("#conn_port").val('');
                $("#conn_db").val('');

                $("#divEmail").hide();
                $("#divREST").hide();
                $("#divDB").hide();
                $("#divKakao").show();
            }

            if( $("select[name=heard]").val() == "DB" ) {
                delete totalObj.send_info;
                delete totalObj.send_content;

                $("#send_url").val('');
                $("#send_port").val('');
                $("#send_path").val('');
                $("#send_method").val('');
                $('#rest_content').html('');

                $("#email_address").val('');
                $("#email_subject").val('');
                $("#email_content").html('');

                $('#send_kakao_subject').val('');
                $('#send_kakao_message').html('');

                $("#divEmail").hide();
                $("#divREST").hide();
                $("#divDB").show();
                $("#divKakao").hide();
            }

            if( $("select[name=heard]").val() == "" ) {
                delete totalObj.send_info;
                delete totalObj.send_content;

                $("#email_address").val('');
                $("#email_subject").val('');
                $("#email_content").html('');

                $("#send_url").val('');
                $("#send_port").val('');
                $("#send_path").val('');
                $("#send_method").val('');
                $('#rest_content').html('');

                $("#conn_url").val('');
                $("#conn_id").val('');
                $("#conn_pw").val('');
                $("#conn_port").val('');
                $("#conn_db").val('');

                $('#send_kakao_subject').val('');
                $('#send_kakao_message').html('');

                $("#divEmail").hide();
                $("#divREST").hide();
                $("#divDB").hide();
                $("#divKakao").hide();
            }
        });

        $('#connButton').click(function() {
            var resultObj = new Object();
            var conn_url = $('#conn_url').val();
            var conn_id = $('#conn_id').val();
            var conn_pw = $('#conn_pw').val();
            var conn_port = $('#conn_port').val();
            var conn_db = $('#conn_db').val();

            if(conn_url=="") {
                alert("주소를 입력해주세요");
                return false;
            }

            if(conn_id=="") {
                alert("ID를 입력해주세요");
                return false;
            }

            if(conn_pw=="") {
                alert("Password를 입력해주세요");
                return false;
            }

            if(conn_port=="") {
                alert("포트번호를 입력해주세요");
                return false;
            }

            if(conn_db=="") {
                alert("데이터베이스를 입력해주세요");
                return false;
            }

            resultObj.conn_url = conn_url;
            resultObj.conn_id = conn_id;
            resultObj.conn_pw = conn_pw;
            resultObj.conn_port = Number(conn_port);
            resultObj.conn_db = conn_db;
            jsonData = JSON.stringify(resultObj);

            $.ajax({
                method:'post',
                url:'/rule/database/conn',
                data: jsonData,
                contentType: "application/json",
                success:function(data) {
                    alert("연결 가능합니다.");
                },

                error: function(request, status, error) {
                    alert("연결할 수 없습니다."+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                }
            });
        });

        var actionTable = $('#datatable-action').DataTable();

        <!-- CodeMirror  -->
        var query = CodeMirror.fromTextArea(document.getElementById("code"), {
          lineNumbers: true,
          mode: "text/x-sql"
        });

        $(".CodeMirror-gutter.CodeMirror-linenumbers").css("width","29px");

        query.on("change",function () {
            checkVal = false;
        });


      });
    </script>

  </body>
</html>
