<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-history"></i> <b>Rule 상세</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <button class="btn btn-primary" id="listButton" type="button">목록</button>
                      <button class="btn btn-danger" id="deleteButton" type="button">삭제</button>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <th class="w_20">Rule 명</th>
                          <td>${list.rule_name}</td>
                        </tr>
                        <tr>
                          <th>Rule 설명</th>
                          <td>${list.rule_descp}</td>
                        </tr>
                        <tr>
                          <th>Rule 쿼리</th>
                          <td>${list.rule_query}</td>
                        </tr>
                        <tr>
                          <th>전송 타입</th>
                          <td>${list.send_type}</td>
                        </tr>
                        <tr>
                          <c:set var="info" value="${list.send_info}"/>
                          <c:set var="splitInfo" value="${fn:split ( fn:substring( info, 1, fn:length(info)-1 ), ',' ) }"/>
                          <th>전송 정보</th>
                          <td>
                                <c:forEach var="value" items="${splitInfo}">
                                    ${ value } <br>
                                </c:forEach>
                          </td>
                        </tr>
                        <tr>
                          <th>전송 방식</th>
                          <c:choose>
                            <c:when test="${list.send_method == 11}">
                              <td>매번</td>
                            </c:when>
                            <c:when test="${list.send_method == 12}">
                              <td>최초 1회</td>
                            </c:when>
                            <c:when test="${list.send_method == 13}">
                              <td>시간당 1회</td>
                            </c:when>
                            <c:when test="${list.send_method == 14}">
                              <td>하루에 1회</td>
                            </c:when>
                          </c:choose>
                        </tr>
                        <tr>
                          <th>최종 전송 시간</th>
                          <td>${list.last_send_time}</td>
                        </tr>
                        <tr>
                          <th>전송 내용</th>
                          <td>${list.send_content}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade bs-example-modal-sm" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2">룰 "${list.rule_name}" 삭제</h4>
                </div>
                <div class="modal-body">
                  <p>룰 "${list.rule_name}" 를 삭제하시겠습니까?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                  <button type="button" class="btn btn-primary" id="deleteButton">삭제</button>
                </div>

              </div>
            </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="resources/build/js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listButton').click(function() {
                location.href="rule";
            });

            $('#deleteButton').click(function() {
                var rule_id = ${list.rule_id};
                if (confirm("Rule ID : " + rule_id + "을(를) 삭제하시겠습니까?") == true) {
                    var ruleObj = new Object();
                    ruleObj.rule_id = String(rule_id);
                    jsonData = JSON.stringify(ruleObj);

                    $.ajax({
                        method : "delete",
                        url : "/rule",
                        contentType : "application/json",
                        data : jsonData,
                        success : function (data) {
                            alert("룰이 삭제 되었습니다");
                            location.href="rule";
                        },

                        error: function(request, status, error) {
                            alert("룰 삭제 실패"+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                        }
                    });
                }
            });


        });

    </script>

  </body>
</html>
