<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="resources/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="resources/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">
	<style type="text/css">
	</style>

  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-dashboard"></i> <b>Dashboard</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                  <input type="text" class="form-control" id="searchText">
                  <span class="input-group-btn">
                    <button class="btn btn-primary pull-right" id="btnSearch" style="margin-right: 5px;"><i class="fa fa-search"></i> Search</button>
                  </span>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
                    <div class="col-md-6">
                      <h3>Event 현황</h3>
                    </div>
                    <div class="col-md-6">
                      <div id="chart_range" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2018 - January 28, 2018</span> <b class="caret"></b>
                      </div>
                    </div>
                  </div>
                  <div class="x_content">
                    <div class="demo-container" style="height:200px">
                      <div id="chart" style="height:200px;"></div>
                    </div>
                    <br/>

                    <table class="table table-striped" style="table-layout:fixed;word-break:break-all;">
                      <thead>
                        <tr>
                          <th style="width:15%;">Time</th>
                          <th style="width:85%;">Event</th>
                        </tr>
                      </thead>
                      <tbody id="eventCondition">
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

        <div id="placeholder" style="width: 825px; height: 150px;"></div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Sparklines -->
    <script src="resources/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- gauge.js -->
    <script src="resources/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="resources/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- Skycons -->
    <script src="resources/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="resources/vendors/Flot/jquery.flot.js"></script>
    <script src="resources/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="resources/vendors/Flot/jquery.flot.time.js"></script>
    <script src="resources/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="resources/vendors/Flot/jquery.flot.resize.js"></script>
    <script src="resources/vendors/Flot/jquery.flot.tooltip.js"></script>
    <!-- Flot plugins -->
    <script src="resources/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="resources/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="resources/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="resources/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="resources/vendors/moment/min/moment.min.js"></script>
    <script src="resources/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="resources/build/js/custom.min.js"></script>
    <script type="text/javascript">
        var plot;

        $(document).ready(function() {
            var chartStartDay;
            var chartEndDay;

            init_data_range();

    		var chart_settings = {
                tooltip: {
                    show: true,
                    content: '<div style="background-color: rgba(1,25,20,0.75); padding: .50em"><font color="#ffffff">Event Count : %y</font></div>',
                    shifts: {
                        x: 10,
                        y: 20
                    },
                    defaultTheme: false
                },
                xaxis: {
                    mode : "time",
                    timeformat : "%H:%M",
                    minTickSize : [10, "minute"],
                    timezone: "browser",
                    tickLength: 0
                },
    			series: {
                    bars: {
                        show: true,
                        barWidth: 5 * 7200,
                        align: "center",
                        fill: 1,

                    }
    			},
    			colors: ["#26B99A"],
    			grid: {
    				borderWidth: {
    					top: 0,
    					right: 0,
    					bottom: 1,
    					left: 1
    				},
    				borderColor: {
    					bottom: "#7F8790",
    					left: "#7F8790"
    				},
    				hoverable: true
    			}
    		};

            plot = $.plot($("#chart"), [{
            }], chart_settings);

            getEventData();

            $('#btnSearch').click(function() {
                getEventData();
            });

        });

        function init_data_range() {
            console.log("111 : ", moment().subtract(1, 'hour') + ", " + moment());
            chartStartDay = moment().subtract(1, 'hour');
            chartEndDay = moment();

            function cb(start, end) {
                chartStartDay = start.format('YYYY-MM-DD HH:mm:ss');
                chartEndDay = end.format('YYYY-MM-DD HH:mm:ss');
                $('#chart_range span').html(chartStartDay + ' - ' + chartEndDay);
            }

            $('#chart_range').daterangepicker({
                startDate: chartStartDay,
                endDate: chartEndDay,
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                },
                timePicker: true,
                timePicker24Hour: true,
                timePickerSeconds: true
            }, cb);

            cb(chartStartDay, chartEndDay);
        }

        function getEventData() {
            var eventObj = new Object();
            eventObj.from = chartStartDay;
            eventObj.to = chartEndDay;

            if ($('#searchText').val() != '' && $('#searchText').val().indexOf(':') == -1) {
                alert("검색어를 찾으려하는 필드명:값 형태로 넣어주십시요.");
                return;
            }

            var searchText = $('#searchText').val().split(":");
            eventObj.searchField = searchText[0];
            eventObj.searchText = searchText[1];

            jsonData = JSON.stringify(eventObj);

            $.ajax({
                method : "post",
                url : "/send/event/history",
                contentType : "application/json",
                data : jsonData,
                success : function (result) {
                    console.log(">> result : ", result);
                    var data = result.data;
                    var chart = result.chart;

                    $('#eventCondition').empty();
                    var appendHtml = '';
                    for (var i = 0; i < data.length; i++) {
                        appendHtml += '<tr><td>' + data[i].time + '</td>'
                                    + '<td>' + data[i].event + '</td></tr>'
                    }
                    $('#eventCondition').append(appendHtml);

                    plot.setData([{data: chart, label: "Event Count"}]);

                    //var axes = plot.getAxes();
                    //axes.xaxis.options.minTickSize = [10, "minute"];

                    plot.setupGrid();
                    plot.draw();

                },

                error: function(request, status, error) {
                    alert("Error Code : " + request.status + "\n" + "Error Cause: " + error);
                }
            });
        }

    </script>
  </body>
</html>
