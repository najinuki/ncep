<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="resources/vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="resources/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-sign-in"></i> <b>Input Event 등록</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <br />
                    <form id="uploadForm" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Event 명
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group">
                            <input type="text" id="event_name" required="required" class="form-control col-md-7 col-xs-12">
                            <span class="input-group-btn">
                              <button id="checkButton" type="button" class="btn btn-primary">중복확인</button>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Event 타입</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="event_type" class="form-control" required style="padding-left:6px;">
                            <option value="">Choose..</option>
                            <option value="stream">Stream</option>
                            <option value="table">Table</option>
                            <option value="trigger">Trigger</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group" id="form_event_attr">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Event 속성</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="btn-group navbar-right">
                            <button class="btn btn-sm btn-default" type="button" id="plus" data-placement="top" data-toggle="tooltip" data-original-title="행추가"><i class="glyphicon glyphicon-plus"></i></button>
                            <button class="btn btn-sm btn-default" type="button" id="minus" data-placement="top" data-toggle="tooltip" data-original-title="행삭제"><i class="glyphicon glyphicon-minus"></i></button>
                          </div>
                          <br /><br />
                          <table id="attr_table" class="table table-bordered">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Attribute Name</th>
                                <th>Data Type</th>
                              </tr>
                            </thead>
                            <tbody id="event_attr">
                              <tr>
                                <th scope="row" style="vertical-align:middle;">1</td>
                                <td>
                                  <input type="text" id="event_attrName" required="required" class="form-control col-md-7 col-xs-12">
                                </td>
                                <td>
                                  <select id="event_dataType" class="form-control" required>
                                    <option value="">Choose..</option>
                                    <option value="string">string</option>
                                    <option value="int">int</option>
                                    <option value="long">long</option>
                                    <option value="double">double</option>
                                    <option value="float">float</option>
                                    <option value="bool">bool</option>
                                    <option value="object">object</option>
                                  </select>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="form-group" id="form_crontab">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Crontab 표현식
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="crontab" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group" id="init_value_yn">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">초기값 유무
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <label class="control-label">
                            <input type="radio" class="flat" name="initValueYn" id="initYes" value="Y" required /> 예
                            <input type="radio" class="flat" name="initValueYn" id="initNo" value="N" checked /> 아니요
                          </label>
                        </div>
                      </div>
                      <div class="form-group" id="init_value_format">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">초기값 포멧
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <label class="control-label">
                            <input type="radio" class="flat" name="initValueFormat" id="initJson" value="json" checked required /> JSON
                            <input type="radio" class="flat" name="initValueFormat" id="initExcel" value="excel" /> Excel
                            <input type="radio" class="flat" name="initValueFormat" id="initDB" value="db" /> DB
                          </label>
                        </div>
                      </div>

                      <div class="form-group" id="init_value_json">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">초기값(JSON)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="jsonInitValue" style="width:100%; height:200px;"></textarea>
                        </div>
                      </div>

                      <div class="form-group" id="init_value_excel">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">초기값(Excel)</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <!--<form action="/file-upload" id="upload" method="POST" class="dropzone" enctype="multipart/form-data">-->
                              <div class="fallback">
                                <input id="excelFile" name="excelFile" type="file" multiple />
                              </div>
                            <!--</form>-->
                        </div>
                      </div>

                      <div class="form-group" id="init_value_db">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">초기값(DB)
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">DB 주소</label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                              <input type="text" id="conn_url" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">ID</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="text" id="conn_id" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">Password</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="password" id="conn_pw" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">포트</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="text" id="conn_port" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">DB명</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <input type="text" id="conn_db" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="middle-name" class="control-label col-md-2 col-sm-2 col-xs-12">SQL 쿼리</label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                              <textarea id="sql_query" style="width:100%; height:60px;"></textarea>
                              <br/>
                              <div style="text-align:right;">
                                <button id="runSQL" type="button" class="btn btn-primary" style="margin-top:5px;">SQL 실행</button>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target=".modal_list" style="margin-top:5px; margin-right:0px;">SQL 결과</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-5">
                          <button class="btn btn-primary" type="button">취소</button>
                          <button type="button" id="submit" class="btn btn-success">등록</button>
                        </div>
                      </div>
                    </form>

                    <div class="modal fade modal_list" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">SQL 실행결과</h4>
                          </div>
                          <div class="modal-body">
                            <table id="table_result" class="table table-bordered" style="overflow:auto;display:block;white-space:nowrap;height:400px;">
                            </table>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="resources/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Dropzone.js -->
    <script src="resources/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- iCheck -->
    <script src="resources/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="resources/vendors/jszip/dist/jszip.min.js"></script>
    <script src="resources/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="resources/vendors/pdfmake/build/vfs_fonts.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="resources/build/js/custom.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var sql_result_label;
        var sql_result_value;

        $('#plus').click(function() {
            var length = $("#attr_table tbody tr").length + 1;
            $('#event_attr').append('<tr>'
                                    + '<th scope="row" style="vertical-align:middle;">'+length+'</td>'
                                    + '<td>'
                                    +   '<input type="text" id="event_attrName" required="required" class="form-control col-md-7 col-xs-12">'
                                    + '</td>'
                                    + '<td>'
                                    +   '<select id="event_dataType" class="form-control" required>'
                                    +     '<option value="">Choose..</option>'
                                    +     '<option value="string">string</option>'
                                    +     '<option value="int">int</option>'
                                    +     '<option value="long">long</option>'
                                    +     '<option value="double">double</option>'
                                    +     '<option value="float">float</option>'
                                    +     '<option value="bool">bool</option>'
                                    +     '<option value="object">object</option>'
                                    +   '</select>'
                                    + '</td>'
                                   +'</tr>' );
        });

        $('#minus').click(function() {
            $('#attr_table > tbody:last > tr:last').remove();
        });

        var idCheck =  false;
        $('#checkButton').click(function() {
            idCheck = true;
            var resultObj = new Object();
            var event_name = $('#event_name').val();

            if(event_name=="") {
                alert("이벤트 명을 입력해주세요");
                $('#checkButton').text("중복확인");
                $('#checkButton').attr("class", "btn btn-primary");
                return false;
            }
            resultObj.event_name = event_name;
            jsonData = JSON.stringify(resultObj);

            $.ajax({
                method:'post',
                url:'/input/event/check',
                data: jsonData,
                contentType: "application/json",
                success:function(data) {
                    alert("사용할 수 있는 이름입니다.");
                    $('#checkButton').text("사용가능");
                    $('#checkButton').attr("class", "btn btn-success");
                },

                error: function(request, status, error) {
                    idCheck = false;
                    alert("이벤트 이름 중복입니다."+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                }
            });
        });

        $('#event_name').on('change', function() {
            idCheck = false;
            $('#checkButton').text("중복확인");
            $('#checkButton').attr("class", "btn btn-primary");
        });

        $('#form_crontab').hide();
        $('#init_value_yn').hide();
        $('#init_value_format').hide();
        $('#init_value_json').hide();
        $('#init_value_excel').hide();
        $('#init_value_db').hide();

        $('#event_type').on('change', function() {
            if( $('#event_type').val() == 'table' ) {
                $('#form_event_attr').show();
                $('#form_crontab').hide();

                $('#init_value_yn').show();
            } else if ( $('#event_type').val() == 'trigger' ) {
                $('#form_event_attr').hide();
                $('#form_crontab').show();

                $('#init_value_yn').hide();
                $('#init_value_format').hide();
                $('#init_value_json').hide();
                $('#init_value_excel').hide();
                $('#init_value_db').hide();
            } else {
                $('#form_event_attr').show();
                $('#form_crontab').hide();

                $('#init_value_yn').hide();
                $('#init_value_format').hide();
                $('#init_value_json').hide();
                $('#init_value_excel').hide();
                $('#init_value_db').hide();
            }
        });

        $('#init_value_yn').on('ifChecked', function () {
            if ( $("input[name=initValueYn]:checked").val() == 'Y' ) {
                $('#init_value_format').show();
                $('#init_value_json').show();
            } else {
                $('#init_value_format').hide();
                $('#init_value_json').hide();
                $('#init_value_excel').hide();
                $('#init_value_db').hide();
            }
        });

        $('#init_value_format').on('ifChecked', function () {
            if ($("input[name=initValueFormat]:checked").val() == 'json') {
                $('#init_value_excel').hide();
                $('#init_value_json').show();
                $('#init_value_db').hide();
            } else if ($("input[name=initValueFormat]:checked").val() == 'excel') {
                $('#init_value_excel').show();
                $('#init_value_json').hide();
                $('#init_value_db').hide();
            } else {
                $('#init_value_excel').hide();
                $('#init_value_json').hide();
                $('#init_value_db').show();
            }
        });

        $('#runSQL').click(function() {
            var resultObj = new Object();
            var conn_url = $('#conn_url').val();
            var conn_id = $('#conn_id').val();
            var conn_pw = $('#conn_pw').val();
            var conn_port = $('#conn_port').val();
            var conn_db = $('#conn_db').val();
            var sql_query = $('#sql_query').val();

            if(conn_url=="") {
                alert("주소를 입력해주세요");
                return false;
            }

            if(conn_id=="") {
                alert("ID를 입력해주세요");
                return false;
            }

            if(conn_pw=="") {
                alert("Password를 입력해주세요");
                return false;
            }

            if(conn_port=="") {
                alert("포트번호를 입력해주세요");
                return false;
            }

            if(conn_db=="") {
                alert("데이터베이스를 입력해주세요");
                return false;
            }

            if(sql_query=="") {
                alert("쿼리문을 입력해주세요");
                return false;
            }

            resultObj.conn_url = conn_url;
            resultObj.conn_id = conn_id;
            resultObj.conn_pw = conn_pw;
            resultObj.conn_port = Number(conn_port);
            resultObj.conn_db = conn_db;
            resultObj.sql_query = sql_query;
            jsonData = JSON.stringify(resultObj);

            $.ajax({
                method:'post',
                url:'/event/database/select',
                data: jsonData,
                contentType: "application/json",
                success:function(data) {
                    if (data.result == 'Error') {
                        alert("에러가 발생했습니다.\n" + data.error_msg);
                        return;
                    } else {
                        alert("SQL 실행을 성공했습니다.\nSQL 결과를 확인해주세요.");
                    }

                    $('#table_result').html('');
                    var resultText = '<thead style="background:rgba(52,73,94,.94); color:white;"><tr>';
                    for (var i = 0; i < data.label.length; i++) {
                        resultText += '<th>' + data.label[i] + '</th>';
                    }
                    resultText += '</tr></thead><tbody>';

                    for (var i = 0; i < data.value.length; i++) {
                        resultText += '<tr>';
                        for (var j = 0; j < data.value[i].length; j++) {
                            resultText += '<td>' + data.value[i][j] + '</td>';
                        }
                        resultText += '</tr>';
                    }
                    resultText += '</tbody>';

                    $('#table_result').html(resultText);

                    sql_result_label = data.label;
                    sql_result_value = data.value;
                },

                error: function(request, status, error) {
                    alert("연결할 수 없습니다."+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                }
            });
        });

        $('#submit').click(function() {
            if (!confirm("등록하시겠습니까?")) {
                return;
            }

            var resultObj = new Object();
            var event_name = $('#event_name').val();
            var event_descp = $('#event_descp').val();
            var event_type = $('#event_type').val();
            var row_length = $("#attr_table tbody tr").length;

            var init_value_yn = $("input[name=initValueYn]:checked").val();
            var init_value_format = $("input[name=initValueFormat]:checked").val();
            var init_value_json = $('#jsonInitValue').val();

            var init_value_excel = $('#excelFile').val();
            var excel_format = init_value_excel.split(".");
            var crontab = $('#crontab').val();

            if (event_name == "") {
                alert("이벤트 명을 입력해주세요");
                return false;
            }
            if (idCheck == false) {
                alert("이벤트 중복확인 해주세요");
                return false;
            }
            if (event_descp == "") {
                alert("이벤트 설명을 입력해주세요");
                return false;
            }
            if (event_type == "") {
                alert("이벤트 타입을 입력해주세요");
                return false;
            }
            if (init_value_yn == 'Y' && init_value_format == 'json' && init_value_json == "") {
                alert("초기값(JSON)을 입력해 주세요");
                return false;
            }
            if (init_value_yn == 'Y' && init_value_format == 'excel' && init_value_excel == "") {
                alert("파일을 선택해 주세요");
                return false;
            }
            if (init_value_yn == 'Y' && init_value_format == 'excel' && init_value_excel != "") {
                if (excel_format[1] != "xls" && excel_format[1] != "xlsx" ) {
                    alert("Excel 파일을 선택해 주세요");
                    return false;
                }
            }
            if (event_type == 'trigger' && crontab == "") {
                alert("Crontab 표현식을 입력해주세요");
                return false;
            }

            if (event_type == 'trigger') {
                var event_query = "define trigger "
                                + event_name
                                + " at '" + crontab + "';";
                resultObj.event_name = event_name;
                resultObj.event_descp = event_descp;
                resultObj.event_query = event_query;
                resultObj.crontab = crontab;

            } else {
                var nameArr = new Array();
                var typeArr = new Array();

                var attStr = "";
                var isValidate = true;
                $("input:text[id='event_attrName']").each(function(index,element){
                    if ($(this).val().trim() == '') {
                        isValidate = false;
                    }

                    nameArr.push( $(this).val() );
                });

                $("select[id='event_dataType']").each(function(index,element){
                    if ($(this).val().trim() == '') {
                        isValidate = false;
                    }
                    typeArr.push( $(this).val() );
                });

                if (isValidate == false) {
                    alert("Event 속성을 제대로 입력해주세요");
                    return false;
                }

                for(var i=0; i<row_length; i++) {
                    attStr+=nameArr[i] + " " + typeArr[i];
                    if(i<row_length-1) {
                        attStr+=", ";
                    }
                }

                var event_query = "define "
                                + event_type
                                + " " + event_name
                                + "(" + attStr + ");";

                resultObj.event_name = event_name;
                resultObj.event_descp = event_descp;
                resultObj.event_query = event_query;

                resultObj.attr_name = nameArr;
                resultObj.data_type = typeArr;
            }

            if(event_type=="stream") {
                resultObj.event_type = "10";
            }

            if(event_type=="table") {
                resultObj.event_type = "11";
            }

            if(event_type=="trigger") {
                resultObj.event_type = "12";
            }

            if( init_value_yn == 'N' ) {
                resultObj.init_value_yn = init_value_yn;
            }

            console.log('init_value_format : ', init_value_format);
            if ( init_value_yn == 'Y' && init_value_format == 'json' ) {
                resultObj.init_value_yn = init_value_yn;
                resultObj.init_value = init_value_json;
                resultObj.init_value_format = init_value_format;
            }

            if ( init_value_yn == 'Y' && init_value_format == 'db' ) {
                resultObj.init_value_yn = init_value_yn;

                var obj_values = new Array();

                for (var i = 0; i < sql_result_value.length; i++) {
                    var obj_value = new Object();
                    for (var j = 0; j < sql_result_value[i].length; j++) {
                        var result_value = sql_result_value[i][j];
                        obj_value[sql_result_label[j]] = result_value;
                    }
                    obj_values.push(obj_value);
                }

                resultObj.init_value = JSON.stringify(obj_values);
                resultObj.init_value_format = init_value_format;
            }

            if ( init_value_yn == 'Y' && init_value_format == 'excel' ) {
                var form = document.getElementById('uploadForm');
                var uploadForm = $('#uploadForm')[0];
                var formData = new FormData(uploadForm);
                formData.append("event_name", event_name);
                formData.append("event_descp", event_descp);
                formData.append("event_type", "11");

                formData.append("init_value_yn", init_value_yn);
                formData.append("event_query", event_query);
                formData.append("attr_name", nameArr);
                formData.append("data_type", typeArr);

                $.ajax({
                    method: "post",
                    enctype: 'multipart/form-data',
                    url : "/input/event/excel",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function (data) {
                        alert("이벤트가 등록 되었습니다");
                        location.reload();
                    },

                    error: function(request, status, error) {
                        alert("이벤트 등록 실패"+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                    }
                });
            } else {
                jsonData = JSON.stringify(resultObj);
                $.ajax({
                    method: "post",
                    url : "/input/event",
                    data: jsonData,
                    contentType: "application/json",
                    success : function (data) {
                        if (data.result == false) {

                            return;
                        } else {
                            alert("이벤트가 등록 되었습니다");
                            location.reload();
                        }
                    },

                    error: function(request, status, error) {
                        alert("이벤트 등록 실패"+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                    }
                });
            }
        });

      });

    </script>

  </body>
</html>
