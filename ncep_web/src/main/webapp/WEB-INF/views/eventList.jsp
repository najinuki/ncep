<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="resources/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">


  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-history"></i> <b>Event 목록</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="eventTable" class="table table-hover table-striped table-bordered bulk_action" data-toggle="table" style="cursor:pointer">
                       <thead>
                         <tr>
                           <th id="event_id">ID</th>
                           <th style="overflow:hidden;white-space:nowrap;">Event 명</th>
                           <th style="overflow:hidden;white-space:nowrap;">Event 타입</th>
                           <th style="overflow:hidden;white-space:nowrap;">Event 쿼리</th>
                           <th style="overflow:hidden;white-space:nowrap;">생성일자</th>
                           <th style="overflow:hidden;white-space:nowrap;">삭제</th>
                         </tr>
                       </thead>

                       <tbody>
                       <c:forEach var="eventList" items="${list}">
                          <tr>
                             <td id="eventID">${eventList.event_id}</td>
                             <td style="overflow:hidden;white-space:nowrap;">${eventList.event_name}</td>
                             <c:choose>
                                <c:when test="${eventList.event_type == 10}">
                                    <td>Stream</td>
                                </c:when>
                                <c:when test="${eventList.event_type == 11}">
                                    <td>Table</td>
                                </c:when>
                                <c:when test="${eventList.event_type == 12}">
                                    <td>Trigger</td>
                                </c:when>
                             </c:choose>
                             <c:choose>
                                <c:when test="${fn:length(eventList.event_query) > 50}">
                                    <td style="overflow:hidden;white-space:nowrap; width:120;">${fn:substring(eventList.event_query,0,49)}...</td>
                                </c:when>
                                <c:otherwise>
                                    <td style="overflow:hidden;white-space:nowrap; width:120;">${eventList.event_query}</td>
                                </c:otherwise>
                             </c:choose>
                             <td>${eventList.insert_date}</td>
                             <td style="text-align:center; vertical-align:middle;"><button type="button" id="deleteButton" class="btn btn-dark btn-xs" style="margin: 0;">삭제</button></td>
                          </tr>
                       </c:forEach>
                       </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="resources/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>

    <!-- iCheck -->
    <script src="resources/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="resources/vendors/jszip/dist/jszip.min.js"></script>
    <script src="resources/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="resources/vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="resources/build/js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#eventTable').DataTable({
                "oLanguage": {
                    "sEmptyTable": "데이터가 없습니다."
                }
            });
            $('#eventTable tbody').on('click', 'button', function() {
                var data = table.row( $(this).parents('tr') ).data();
                var event_id = data[0];
                if (confirm("Event ID : " + event_id + "을(를) 삭제하시겠습니까?") == true) {
                    var eventObj = new Object();
                    eventObj.event_id = String(event_id);
                    jsonData = JSON.stringify(eventObj);

                    $.ajax({
                        method : "delete",
                        url : "/input/event",
                        contentType : "application/json",
                        data : jsonData,
                        success : function (data) {
                            alert("이벤트가 삭제 되었습니다");
                            location.reload();
                        },

                        error: function(request, status, error) {
                            alert("이벤트에 등록된 Rule이 사용중입니다. Rule을 먼저 삭제해주십시오.");
                        }
                    });
                }
                return false;
            });

            $('#eventTable tbody').on('click', 'tr', function() {
                var data = table.row(this).data();
                location.href="eventDetail?event_id="+data[0];
            });

        });

    </script>

  </body>
</html>
