<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> CEP Web </title>

    <!-- Bootstrap -->
    <link href="resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="resources/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="resources/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="resources/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="resources/build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <div class="site_title"> &nbsp;<i class="fa fa-th"></i> <span>NCEP Admin</span></div>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <jsp:include page="../include/side.jsp" flush="false"/>
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><i class="fa fa-history"></i> <b>Event 상세</b></h2>
              </div>

              <div class="title_right">
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                      <button class="btn btn-primary" id="listButton" type="button">목록</button>
                      <button class="btn btn-danger" id="deleteButton" type="button">삭제</button>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-bordered">
                      <tbody>
                      <c var="eventList" items="${eventList}">
                        <tr>
                          <th class="w_20">Event 명</th>
                          <td>${eventList.event_name}</td>
                        </tr>
                        <tr>
                          <th>Event 설명</th>
                          <td>${eventList.event_descp}</td>
                        </tr>
                        <tr>
                          <th>Event 타입</th>
                             <c:choose>
                                <c:when test="${eventList.event_type == 10}">
                                    <td>Stream</td>
                                </c:when>
                                <c:when test="${eventList.event_type == 11}">
                                    <td>Table</td>
                                </c:when>
                             </c:choose>
                        </tr>
                        <tr>
                          <th>Event 쿼리</th>
                          <td>${eventList.event_query}</td>
                        </tr>
                        <tr>
                          <th>초기값 여부</th>
                          <td>${eventList.init_value_yn}</td>
                        </tr>
                        <tr>
                          <th>초기값</th>
                          <td>
                          </td>
                        </tr>
                        </c>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <b>Event 적용 Rule 목록</b>
                  </div>
                  <div class="x_content">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Event 명</th>
                          <th>Rule 명</th>
                          <th>Rule Query</th>
                          <th>전송 타입</th>
                        </tr>
                      </thead>
                      <tbody>
                      <c:forEach var="ruleList" items="${ruleList}">
                        <c var="eventList" items="${eventList}">
                        <tr>
                          <td>${eventList.event_name}</td>
                          <td>${ruleList.rule_name}</td>
                          <td>${ruleList.rule_query}</td>
                          <td>${ruleList.send_type}</td>
                        </tr>
                        </c>
                      </c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade bs-example-modal-sm" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2">룰 "${list.rule_name}" 삭제</h4>
                </div>
                <div class="modal-body">
                  <p>룰 "${list.rule_name}" 를 삭제하시겠습니까?</p>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                  <button type="button" class="btn btn-primary" id="deleteButton">삭제</button>
                </div>

              </div>
            </div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="resources/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="resources/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="resources/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="resources/vendors/nprogress/nprogress.js"></script>
    <!-- Datatables -->
    <script src="resources/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="resources/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="resources/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="resources/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="resources/vendors/jszip/dist/jszip.min.js"></script>
    <script src="resources/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="resources/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="resources/build/js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#listButton').click(function() {
                location.href="event";
            });

            $('#deleteButton').click(function() {
                var event_id = ${eventList.event_id};
                if (confirm("Event ID : " + event_id + "을(를) 삭제하시겠습니까?") == true) {
                    var eventObj = new Object();
                    eventObj.event_id = String(event_id);
                    jsonData = JSON.stringify(eventObj);

                    $.ajax({
                        method : "delete",
                        url : "/input/event",
                        contentType : "application/json",
                        data : jsonData,
                        success : function (data) {
                            alert("이벤트가 삭제 되었습니다");
                            location.href="event";
                        },

                        error: function(request, status, error) {
                            alert("이벤트 삭제 실패"+ "\n" + "Error Code : " + request.status + "\n" + "Error Cause: " + error);
                        }
                    });
                }
            });

        });

    </script>

  </body>
</html>
