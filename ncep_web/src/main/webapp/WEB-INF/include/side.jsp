<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <ul class="nav side-menu">
      <li><a href="home"><i class="fa fa-dashboard"></i> Dashboard</a>
      </li>
      <li><a href="eventRegister"><i class="fa fa-edit"></i> Event Register</a>
      </li>
      <li><a href="ruleRegister"><i class="fa fa-edit"></i> Rule Register</a>
      </li>
      <li><a href="event"><i class="fa fa-history"></i> Event List</a>
      </li>
      <li><a href="rule"><i class="fa fa-history"></i> Rule List</a>
      </li>
      <li><a href="ruleHistory"><i class="fa fa-history"></i> RuleHistory List</a>
      </li>
      <li><a href="sendSimulator"><i class="fa fa-calculator"></i> Send Simulator</a>
      </li>
    </ul>
  </div>

</div>
<!-- /sidebar menu -->